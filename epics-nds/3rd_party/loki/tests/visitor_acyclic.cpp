/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * =============================================================
 * Acyclic visitor pattern.
 * This is essentially the example page 253 of the book.
 */

#include <iostream>

#include "Visitor.h"

// [1] Declare the root oh the hierarchy as visitable, by inheriting
//     from BaseVisitable< ReturnType >
//     The return type of Visit is void by default
//
class Base : public Loki::BaseVisitable< >
{
	public:
};

// ======================================================
// [2] Declare each member of the hierarchy that is visitable as such
//     Use the macro DEFINE_VISITABLE
//

class Derived1 : public Base
{
	public:
		DEFINE_VISITABLE();
};

class Derived2 : public Derived1
{
	public:
		DEFINE_VISITABLE();
};

// ======================================================

class MyVisitor :
	public Loki::BaseVisitor,
	public Loki::Visitor< Derived1 >,
	public Loki::Visitor< Derived2 >
{
	public:
		void Visit(Derived1 &) { std::cout << "visiting Derived1 \n"; }
		void Visit(Derived2 &) { std::cout << "visiting Derived2 \n"; }
};

// ======================================================

int main()
{
	MyVisitor v1;

	Derived1 d1;
	Derived2 d2;

	d1.Accept( v1 );
	d2.Accept( v1 );
}
