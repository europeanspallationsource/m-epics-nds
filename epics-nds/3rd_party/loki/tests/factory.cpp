/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ================================================================
 * Sample program for the object Factory (chapter 8).
 *
 */
#include <iostream>
#include <string>

#include "Factory.h"

// -----------------------------------------------
// include custom class hierarchy
//
#include "base_2der.h"

// -----------------------------------------------
// functions usedto create objects of the class hierarchy
//
Base * Create0() { return new Base; }
Base * Create1() { return new Derived1; }
Base * Create2() { return new Derived2; }

// -----------------------------------------------
//
int main()
{
	Loki::Factory< Base, std::string > factory;

	std::cout << factory.Register( "base", Create0 ) << "\n";
	std::cout << factory.Register( "der1", Create1 ) << "\n";
	std::cout << factory.Register( "der2", Create2 ) << "\n";

	// registering again "base" fails, even if the function is the same
	std::cout << factory.Register( "base", Create0 ) << "\n";

	Base * b0 = factory.CreateObject( "base" );
	Base * b1 = factory.CreateObject( "der1" );
	Base * b2 = factory.CreateObject( "der2" );

	b0->print();
	b1->print();
	b2->print();

	delete b0;
	delete b1;
	delete b2;
}
