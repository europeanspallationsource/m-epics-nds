/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ===============================================================
 * Sample program for GenScatterHierarchy (see pg. 64)
 *
 * For the GenScatterHierarchy the same type cannot appear twice
 * in the typelist otherwise there is an ambiguity.
 *
 */
#include <iostream>
#include <stdio.h>  // lazyness

#include "TypeManip.h"
#include "HierarchyGenerators.h"


// -------------------------------------------------------
// custom class hierarchy
//                        Base
//                         |
//                 +-------+-------+
//                 |               |
//                 C0              C1
//                 |
//                 C2
//
class Base
{
	public:
		virtual ~Base() {};
		virtual void print() = 0;
};

class C0 : public Base
{
	public:
		C0() { printf("C0 cstr. %p\n", (void*)this); }
		void print() { printf("print C0 %p\n", (void*)this); }
};

class C1 : public Base
{
	public:
		C1() { printf("C1 cstr. %p\n", (void*)this); }
		void print() { printf("print C1 %p\n", (void*)this); }
};

class C2 : public C0
{
	public:
		C2() { printf("C2 cstr. %p\n", (void*)this); }
		void print() { printf("print C2 %p\n", (void*)this); }
};


// the constructor of the Model invokes the constructors of the 
// tamplate classes, when the hierarchy is built, by constructing
// the member attribute "value_"
//
template< typename T >
class Model
{
	private:
		T value_;
	public:
		T * Get()  { return & value_; }
			
};

int main()
{
  typedef Loki::GenScatterHierarchy< TYPELIST_3(C0,C1,C2), Model > GSH;
  GSH gsh;

  // this is the same as Loki::Field<C0, GSH>( gsh ).Get();
  Base * pc0 = Loki::Field<C0>( gsh ).Get();
  std::cout << "Field<C0>: ";
  pc0->print();

  Base * pc1 = Loki::Field<0>( gsh ).Get();
  std::cout << "Field at <0>: ";
  pc1->print();

  Base * pc2 = Loki::Field<C2>( gsh ).Get();
  std::cout << "Field<C2>: ";
  pc2->print();


#if 0
  // this gives a compiler warninig
  // base class Loki::GenScatterHierarchy<int, Model> inacessible 
  // due to ambiguity
  //
  typedef Loki::GenScatterHierarchy< TYPELIST_2(int,int), Model > GSI;
  GSI gsi;

  // then this is an error
  // int * i = Loki::Field<int>( gsi ).Get();
  int * i = Loki::Field<0>(gsi).Get();
  printf("int %p\n", (void*)i);
#endif

}
