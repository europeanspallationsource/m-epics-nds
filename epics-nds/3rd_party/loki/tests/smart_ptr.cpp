/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ==============================================================
 * Sample program that uses the smart pointer (Chapter 7 [?]).
 */
#include <iostream>

#include "SmartPtr.h"

// This is the storage allocation for smart pointer based on malloc
//
#include "MallocSPStorage.h"

struct MyClass
{
  int i_;
  
  MyClass( int i ) : i_(i) { std::cout << "MyClass cstr. " << i_ << "\n"; }
  ~MyClass() { std::cout << "MyClass dstr. " << i_ << "\n"; }

  void print() { std::cout << i_ << "\n"; }
};


typedef Loki::SmartPtr< 
  MyClass,
  Loki::RefCounted,
  Loki::AllowConversion,
  Loki::AssertCheck,          // Loki::RejectNull,
  Loki::DefaultSPStorage      // Loki::MallocSPStorage
> MyClassPtr;

int main()
{
  try {
    MyClass mc1( 2 );
   
    MyClass * mc2 = new MyClass( 4 );
    MyClassPtr sp2 = mc2;
    sp2->print();
  
    MyClassPtr sp1( new MyClass(3) );
    sp1->print();
  
    sp1 = sp2;
    sp1->print();
    sp2->print();
  
    // with RejectNUll checking policy this causes an "aborted"
    // MyClassPtr sp3( NULL );
    
    mc2 = sp2;
    mc2->print();
  
    // This is illegal, because the smat pointer takes ownership
    // MyClassPtr sp2( & mc1 );
    // sp2->print();
   
    // delete mc2; // this is an error
  } catch ( std::exception & e ) {
	  std::cout << e.what() << "\n";
  }
}
  
