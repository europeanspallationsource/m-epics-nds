/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ===============================================================
 * Sample Visitor pattern
 * 
 */
#include <iostream>
#include <typeinfo>

// [1] Forward declare Visitor
//
class Visitor;

// [2] Forward declare the visitable classes of the hierarchy
// 
class Derived1;
class Derived2;

// [3] The hierarchy Base class declares Accept() with Visitor parameter
//
class Base
{
	public:
		virtual void Accept( Visitor * ) = 0;
};

// [4] Visitor declares an overloaded Visit() for each visitable class
//
class Visitor
{
	public:
		virtual void Visit( Derived1 * ) = 0;
		virtual void Visit( Derived2 * ) = 0;
};

// ======================================================
// [5] Visitable classes implement Accept()

class Derived1 : public Base
{
	public:
		void Accept( Visitor * v ) 
		{ 
			std::cout << "Derived1 accepts visitor " 
				  << typeid( *v ).name() << "\n";
			v->Visit( this ); 
		}
};

class Derived2 : public Derived1
{
	public:
		void Accept( Visitor * v ) 
		{ 
			std::cout << "Derived2 accepts visitor " 
				  << typeid( *v ).name() << "\n";
			v->Visit( this ); 
		}
};

// ======================================================
// [6] Concrete Visitor's implement Visit() for all the visitable classes

class Visitor1: public Visitor
{
	public:
		void Visit( Derived1 * d1 )
		{
			std::cout << "Visitor1 visits Derived1 \n";
		}
		void Visit( Derived2 * d1 )
		{
			std::cout << "Visitor1 visits Derived2 \n";
		}
};

class Visitor2: public Visitor
{
	public:
		void Visit( Derived1 * d1 )
		{
			std::cout << "Visitor2 visits Derived1 \n";
		}
		void Visit( Derived2 * d1 )
		{
			std::cout << "Visitor2 visits Derived2 \n";
		}
};

// ==========================================================

int main()
{
	Visitor1 v1;
	Visitor2 v2;

	Derived1 d1;
	Derived2 d2;

	d1.Accept( &v1 );
	d1.Accept( &v2 );
	d2.Accept( &v1 );
}
