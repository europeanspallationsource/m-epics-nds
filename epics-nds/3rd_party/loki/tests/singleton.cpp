/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ================================================================
 * This is an example of the usage of Singleton (Chapter 6 [?])
 *
 */
#include <iostream>

#include "Singleton.h"

// custom class hierarchy
//
#include "base_derived.h"

// overloaded Longevity function
//
int GetLongevity( Base * )
{
	std::cout << "GetLongevity(Base)\n";
	return 2;
}

int GetLongevity( Derived * )
{
	std::cout << "GetLongevity(Derived)\n";
	return 1;
}


// Declaration of a singleton typename, in which the policies are all specified.
// Tested with:
//      CreateUsingNew    DefaultLifetime           SingleThreaded
//      CreateUsingNew    PhoenixSingleton          SingleThreaded
//      CreateUsingNew    SingletonWithLongevity    SingleThreaded
//
typedef class Loki::SingletonHolder< 
		Base, 
		Loki::CreateUsingNew, 
		Loki::SingletonWithLongevity,
		Loki::SingleThreaded
	>  SingleBase;

typedef class Loki::SingletonHolder< 
                Derived,
	        Loki::CreateUsingNew,
	        Loki::SingletonWithLongevity
        > SingleDerived;


int main()
{
	for (int k=0; k<3; k++) {
		SingleBase::Instance().DoSomething();
	        SingleDerived::Instance().DoSomething();
	}
}
