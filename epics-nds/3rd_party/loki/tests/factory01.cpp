/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * =============================================================
 * Sample example of the Factory pattern (see chapter 8)
 *
 */
#include <iostream>

// ------------------------------------------------
// Base class of the custom hierarchy
//
class Base
{
  public:
    virtual ~Base() {}

    virtual void print() { std::cout << "Base\n"; }

};

// creator function
//
Base * Create0() { return new Base; }

// ----------------------------------------------------
// Object factory
//   * has an array of pointer to functions, used to create objects
//   * implements a dispatch ( Create() ) based on the index in the array
//   * has a method to register the creator functions
//   * has a method to unregister creator functions
//

template< class T >
class Factory
{
  public:
    typedef T* (*CreateCB)();

    Factory( int n ) : nr_(n)
    {
       map_ = new CreateCB[ nr_ ];
       for (int i=0; i<nr_; i++) 
         map_[i]=0;
    }

    ~Factory()
    {
       delete[] map_;
    }

    T* Create( unsigned int index )
    { 
      std::cout << "Factory create " << index << "\n";
      if ( index >= nr_ || map_[index] == 0 )
        return 0;
      return (map_[index])();
    }

    void Register( unsigned int index, CreateCB cb )
    { 
      if (index < nr_ )
        map_[index] = cb;
    }

    void Unregister( int index )
    {
      if (index < nr_ )
        map_[index] = 0;
    }

  private:
    int nr_;
    CreateCB * map_;
};
  
template< class T >
class Registrar
{
  public:
    typedef typename Factory<T>::CreateCB CreateCB;

    Registrar( Factory<T>& fact, int i, CreateCB cb ) 
      : fact_(fact), index_(i)
    {
      std::cout << "register " << index_ << "\n";
      fact_.Register( i, cb );
    }
  
    ~Registrar()
    {
      std::cout << "unregister " << index_ << "\n";
      fact_.Unregister(index_);
    }
  private:
    Factory<T> & fact_;
    int index_;
};


// ================================================
// instantiate a factoty with 4 callbacks
// and its registrar

Factory<Base> fact(4);
Registrar<Base> r0( fact, 0, Create0);

// ==================================================
// Derived functions of the custom hierarchy,
// their creator functions,
// and their registrars

class Derived1 : public Base
{
  public:
    ~Derived1() {}

    void print() { std::cout << "Derived1\n"; }
};

Base * Create1() { return new Derived1; }
Registrar<Base> r1( fact, 1, Create1);

class Derived2 : public Derived1
{
  public:
    ~Derived2() {}

    void print() { std::cout << "Derived2\n"; }
};

Base * Create2() { return new Derived2; }
Registrar<Base> r2( fact, 2, Create2);

// ====================================================
int main()
{
  std::cout << "main begins\n";
  Base * b0 = fact.Create( 0 );
  Base * b1 = fact.Create( 1 );
  Base * b2 = fact.Create( 2 );
  std::cout << "Now do some printing \n";
  b0->print();
  b1->print();
  b2->print();
  std::cout << "Time to quit.\n";
  return 0;
}

