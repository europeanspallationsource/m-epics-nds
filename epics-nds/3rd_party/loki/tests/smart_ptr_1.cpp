/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ===============================================================
 * Sample program that uses smart pointer (see pg. ...)
 *
 */
// #include <sys/types.h> // getpid kill
// #include <unistd.h>    // getpid
// #include <signal.h>    // kill

#include "SmartPtr.h"

#include "base_derived.h"


int main()
{
	Derived * pd = new Derived(2, 3);
	Base * pb = pd;

	{
		Loki::SmartPtr<Base> spd( pd );
		spd->DoSomething();

		Loki::SmartPtr<Base> spb( pb );
		spb->DoSomething();
	}
	std::cout << "out of the block \n";
	// kill( SIGKILL, getpid() );
	// std::cout << "killed \n";

 	// when it goes out of scope the smart pointer deallocates
	// its pointed object. The following code would give unpredictable
	// results
	//
	Loki::SmartPtr<Base> spb( pb );
	spb->DoSomething();

}

	
