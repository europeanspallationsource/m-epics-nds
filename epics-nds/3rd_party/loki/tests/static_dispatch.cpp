/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ===========================================================
 * This sample program show the usage of StaticDispatcher
 * pg. 275
 *
 */

#include <iostream>

#include "MultiMethods.h"

// =====================================================
// Class hierarchy:

struct Left0
{
	virtual void help() { std::cout << "Left-0 \n"; }
};

struct Left1 : public Left0
{
	virtual void help() { std::cout << "Left-1 \n"; }
};

struct Left2 : public Left1
{
	virtual void help() { std::cout << "Left-2 \n"; }
};

// ========================================================
// Custom Executor. The Executor must implement two methods:
//        ResultType Fire( BaseLhs &, BaseRhs & )
//        ResultType OnError( BaseLhs &, BaseRhs & )
// By default BaseRhs is the same as BaseLhs.
//

class MyExec
{
	public:
		void Fire( Left0 & l1, Left0 & l2 )
		{
			std::cout << "MyExec::Fire() \n";
			l1.help();
			l2.help();
		}

		void OnError( Left0 & l1, Left0 & l2 )
		{
			std::cout << "MyExec::OnError() \n";
		}
};

// ========================================================
// The dispatcher. By default the dispatch is symmetric, the BaseRhs
// coincides with the BaseRhs, the type list TypesRhs is the same
// as for the lhs, and the return value is void.
//
// N.B. even though Fire() and OnError() take Left0 as parameters,
// the dispatcher dispatches only Left1 and Left2, therefore if it
// used with Left0, OnError is invoked (see main() below).

typedef Loki::StaticDispatcher
<
	MyExec,
	Left0,
	TYPELIST_2( Left1, Left2 )
> MyDispatcher;

int main()
{
	Left0 l0;
	Left1 l1;
	Left2 l2;

	MyDispatcher dsp;
	MyExec exec;
	

	dsp.Go( l0, l1, exec );
	dsp.Go( l2, l1, exec );
}
