
#include <iostream>


class Base
{
	public:
		Base( int i = 1 ) : mBaseInt(i) { }

		void DoSomething()
		{
			std::cout << "Base is here " << mBaseInt << "\n";
		}
	protected:
		int mBaseInt;
};

class Derived : public Base
{
	public: 
		Derived( int i=0, int j = 2 ) : Base(i), mDerivedInt(j) { }

		void DoSomething()
	        {
			std::cout << "Derived " << mDerivedInt << "\n";
		}
	protected:
		int mDerivedInt;
};

