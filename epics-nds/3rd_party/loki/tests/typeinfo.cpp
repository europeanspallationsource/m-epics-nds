/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * =============================================================
 * Sample program that uses TypeInfo (see pg. ...)
 *
 */
#include <iostream>

#include "TypeInfo.h"

#include "base_derived.h"


int main()
{
	const char * name[] = { "Base", "bobj", "Base*", "pbobj", "Derived" };
	Base * base = new Base(1);
	Base bobj;
	Derived * derived;
	Loki::TypeInfo t[5];
	t[0] = Loki::TypeInfo( typeid( Base ) );
	t[1] = Loki::TypeInfo( typeid( bobj ) );
	t[2] = Loki::TypeInfo( typeid( Base * ) );
	t[3] = Loki::TypeInfo( typeid( base ) );
	t[4] = typeid( Derived );

	std::cout << "Base id:   " << t[0].name() << "\n";
	std::cout << "bobj id:   " << t[1].name() << "\n";
	std::cout << "Base * id: " << t[2].name() << "\n";
	std::cout << "pbase id:  " << t[3].name() << "\n";
	std::cout << "Derived id: " << t[4].name() << "\n";

	for (int i=0; i<5; i++) for (int j=i; j<5; j++) {
	  std::cout << name[i] << " vs " << name[j] << ":  < "
	            << (t[i] < t[j])  << "   == "
	  	    << (t[i] == t[j]) << "   > "
		    << (t[i] > t[j])  << "\n";
	}
	
}

