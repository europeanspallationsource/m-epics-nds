#include <iostream>

class Base
{
  public:
    virtual ~Base() {}

    virtual void print() { std::cout << "Base\n"; }

};


// ==================================================

class Derived1 : public Base
{
  public:
    ~Derived1() {}

    void print() { std::cout << "Derived1\n"; }
};


class Derived2 : public Base
{
  public:
    ~Derived2() {}

    void print() { std::cout << "Derived2\n"; }
};

