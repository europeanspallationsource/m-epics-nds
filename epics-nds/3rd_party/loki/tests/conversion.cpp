#include <iostream>

#if 0
#include "TypeManip.h"

using Loki::Conversion;

#else

template< typename T, typename U >
struct Conversion
{
  typedef char Small;
  struct Big { char dummy[2]; };

  static Big Test( ... );
  static Small Test( U );

  static T MakeT();

  static const unsigned size_U = sizeof( Small );
  static const unsigned size_T = sizeof(( Test( MakeT() ) ));

  // enum { exists = sizeof(Small) == sizeof(( Test( MakeT() ) )) };
  enum { exists = size_U == size_T };
  enum { sameType = false };
};

template< typename T >
struct Conversion<T, T>
{
  enum { exists = 1, sameType = 1 };
};

template< typename T >
struct Conversion< void, T >
{
  enum { exists = 0, sameType = 0 };
};

template< typename T >
struct Conversion< T, void >
{
  enum { exists = 0, sameType = 0 };
};

template< >
struct Conversion< void, void >
{
  enum { exists = 1, sameType = 1 };
};

#endif

template < typename T >
struct Base
{
  T value_;
};

template< typename T >
struct Derived : public Base<T>
{
  T* pointer_;
};

int main()
{
  Base<int> b;
  Derived<int> d;

  std::cout << "Conversions \n";
  std::cout << "int -> double " << Conversion<int,double>::exists << "\n";
  std::cout << "double -> int " << Conversion<double,int>::exists << "\n";
  std::cout << "char* -> int* " << Conversion<char*,int*>::exists << "\n";
  std::cout << "char* -> int  " << Conversion<char*,int >::exists << "\n";
  std::cout << "int -> void   " << Conversion<int,void>::exists << "\n";
  std::cout << "void -> void  " << Conversion<void,void>::exists << "\n";
  std::cout << "Derived<int> -> Base<int> " 
            << Conversion<Derived<int>, Base<int> >::exists 
            << "\n";
  std::cout << "Base<int> -> Derived<int> " 
            << Conversion<Base<int>,Derived<int> >::exists 
            << "\n";
  std::cout << "Derived<int> -> Base<char> " 
            << Conversion<Derived<int>, Base<char> >::exists 
            << "\n";
  std::cout << "Base<int> -> void " 
            << Conversion<Base<int>,void>::exists
            << "\n";
  std::cout << "void -> Base<int> "
            << Conversion<void,Base<int> >::exists
            << "\n";
}


