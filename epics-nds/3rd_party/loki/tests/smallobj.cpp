/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ==============================================================
 * Sample program that uses the small object allocator
 * (see pg. ...) and the fixed allocator (pg. ...)
 */
#include <iostream>

#include "SmallObj.h"

struct Obj
{
	int i;
	float f;
};

int main()
{
	Loki::FixedAllocator fa( sizeof( Obj ) );
        Loki::SmallObjAllocator sa( 3 * sizeof( Obj ), sizeof( Obj ) );

        Obj * p1[4];	
        Obj * p2[4];	
	for (int k=0; k<4; k++) {
		p1[k] = reinterpret_cast<Obj*>(fa.Allocate());
		p2[k] = (Obj*)sa.Allocate( sizeof( Obj ) );
		std::cout << p1[k] << " " << p2[k] << "\n";
	}
	for (int k=0; k<4; k++) {
		fa.Deallocate( p1[k] );
		sa.Deallocate( p2[k], sizeof( Obj ) );
	}

	typedef Loki::SmallObject< Loki::SingleThreaded, 
		        3 * sizeof( Obj ), 
			sizeof( Obj ) > sobj;
	for (int k=0; k<4; k++) {
		p1[k] = (Obj*)sobj::operator new( sizeof( Obj ) );
		p2[k] = (Obj*)sobj::operator new( sizeof( Obj ) );
		std::cout << p1[k] << " " << p2[k] << "\n";
	}
	for (int k=0; k<4; k++) {
		sobj::operator delete( p1[k], sizeof( Obj ) );
	}
	for (int k=0; k<4; k++) {
		std::cout << p2[k] << "\n";
	}
	for (int k=0; k<4; k++) {
		sobj::operator delete( p2[k], sizeof( Obj ) );
	}
}
