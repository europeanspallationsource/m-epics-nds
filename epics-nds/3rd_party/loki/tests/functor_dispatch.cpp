/** sample test for the Loki library 
 *
 * @author marco corvi <marcounderscorecorviatgeocitiesdotcom>
 * @date dec 2004
 *
 * This code is part of a set of sample test programs for the Loki library.
 *
 * The Loki library is copyright(c) 2001 by Andrei Alexandrescu.
 * It is described in the book:
 *     Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and
 *     Design Patterns Applied". Copyright (c) 2001. Addison-Wesley.
 * and is available from 
 *     http://sourceforge.net/projects/loki-lib/
 *
 * Permission to use, copy, modify, distribute and sell this software 
 *     (ie, the test programs) for any purpose is hereby granted without fee.
 * Permission to use, copy, modify, distribute and sell the software
 *     changes made to the Loki library is hereby granted without fee
 *     for any purposes provided the original Loki library copyright notice
 *     appears in all copies and that both that notice and this 
 *     permission notice appear in supporting documentation.
 * The author makes no representations about the suitability of this 
 *     software for any purpose. It is provided "as is" without express 
 *     or implied warranty.
 *
 * ===========================================================
 * This sample program show the usage of the functor
 * dispatcher (see pg. 285)
 *
 */

#include <iostream>
#include <stdexcept>

#include "MultiMethods.h"

// =====================================================
// Class hierarchy:
// 

struct Left0
{
	virtual void help() { std::cout << "Left-0 "; }
};

struct Left1 : public Left0
{
	virtual void help() { std::cout << "Left-1 "; }
};

struct Left2 : public Left1
{
	virtual void help() { std::cout << "Left-2 "; }
};



// ========================================================
// Functors

template< class C1, class C2 >
struct MyFun
{
	void operator()( C1 & c1, C2 & c2 )
	{
		std::cout << "MyFun-" << typeid(C1).name() << "-"
			  << typeid(C2).name() << ":  ";
		c1.help();
		c2.help();
		std::cout << "\n";
	}
};

// ========================================================
// The dispatcher. By default the BaseRhs
// coincides with the BaseLhs and the return value is void.
// The Callback has signature
//      ReturnType (*)( BaseLhs &, BaseRhs &)
//

typedef Loki::FunctorDispatcher< Left0 > MyDispatcher;

int main()
{
	Left0 l0;
	Left1 l1;
	Left2 l2;

	MyFun<Left0,Left0> fun00;
	MyFun<Left0,Left1> fun01;
	MyFun<Left1,Left0> fun10;
	MyFun<Left0,Left2> fun02;

	MyDispatcher dsp;

        try {
		dsp.Add< Left0, Left0, MyFun<Left0,Left0> >( fun00 ); // (1)
		dsp.Add< Left0, Left1, MyFun<Left0,Left1> >( fun01 ); // (2)
		dsp.Add< Left2, Left1, MyFun<Left0,Left0> >( fun00 ); // (3)

		dsp.Go( l0, l0 ); // use (1)
		dsp.Go( l0, l1 ); // use (2)
		dsp.Go( l2, l1 ); // use (3)
	        dsp.Go( l2, l0 ); // this throws a runtime_error

	} catch ( std::runtime_error & e ) {
		std::cout << "Error: " << e.what() << "\n";
	}
}
