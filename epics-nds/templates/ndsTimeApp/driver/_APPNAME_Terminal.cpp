/*
 * exChanel.cpp
 *
 *  Created on: 18. jul. 2012
 *      Author: Slava Isaev, Klemen Zagar
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <errlog.h>
#include <iocsh.h>
#include <string.h>

#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/stat.h>
#include <sys/timerfd.h>
#include <epicsThread.h>
#include <epicsMutex.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <time.h>

#include "ndsChannelStates.h"
#include "ndsDriverCommand.h"

#include "_APPNAME_Terminal.h"

Terminal::Terminal():
		eventCounter(0),
		timestamp()
{
	/*
	 * Registering state change handlers.
	 * State machine supports 3 levels of handlers.
	 */

	/*
	 * 1st level onRequest handlers
	 * These handlers are executed before state change and should succeed
	 *  to allow transfer to new state.
	 */
//	registerOnRequestStateHandler(CHANNEL_STATES_OFF, CHANNEL_STATES_ON, );

	/*
	 * 2nd level onLeave handlers for the current state
	 * These handlers are executed when state machine leaving current state.
	 */
//	registerOnLeaveStateHandler();

	/*
	 *  3rd level onEnter handler for the new state
	 *  These Hnadlers are executed right after the state was changed.
	 */
//	registerOnEnterStateHandler();

}

ndsStatus Terminal::registerHandlers(nds::PVContainers* pvContainers)
{
	nds::Terminal::registerHandlers(pvContainers);

	NDS_PV_REGISTER_INT32("Event", &nds::Base::setInt32, &Terminal::getInt32, &interruptEvent);
	NDS_PV_REGISTER_INT32ARRAY("TimeStamp", &nds::Base::setInt32Array, &Terminal::getInt32Array, &interruptTimeStamp);

    NDS_PV_REGISTER_INT32("Reason", &Terminal::setReason, &Base::getInt32, &interruptReason);


    char timerName[80];
    snprintf(timerName, 80, "TerminalTimer_%d", _portAddr);

    eventTimer = nds::Timer::create(timerName, boost::bind(&Terminal::generateEvent, this, _1, _2) );

    eventTimer->start(5.0);

	return ndsSuccess;
}

/*
 * Destroy a channel by freeing all its resources.
 */
Terminal::~Terminal()
{

}

void Terminal::destroy()
{
}

epicsTimerNotify::expireStatus Terminal::generateEvent(nds::TaskService &service, const epicsTime & currentTime)
{
	epicsInt32 value[2];

	timestamp += 600;

	epicsTimeStamp stamp = (epicsTimeStamp)timestamp;

	value[0] = stamp.secPastEpoch;
	value[1] = stamp.nsec;

	// Updating timestamp
	doCallbacksInt32Array(value, 2, interruptTimeStamp, _portAddr, stamp);

	// Generating Event
	++eventCounter;
	doCallbacksInt32(eventCounter, interruptEvent, _portAddr);

    epicsTimerNotify::expireStatus res(epicsTimerNotify::restart, 5.0);
    return res;
}

ndsStatus Terminal::setReason(asynUser* pasynUser, epicsInt32 value)
{
    try
    {
        // Reason's arguments is accessible through DriverCommand class
        nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
        std::cout << "Arg[0]: "<< command->Args[0] << std::endl;
        return ndsSuccess;
    }catch(...)
    {
        NDS_ERR("Casting can't be done!" );
        return ndsError;
    }
}

ndsStatus Terminal::setStreamingURL(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered)
{
    ndsStatus result = Channel::setStreamingURL(pasynUser, data, numchars, nbytesTransfered);

    NDS_WRN("Streaming URL is set: %s", _StreamingURL.c_str() );

    return result;
}


ndsStatus Terminal::doTest(asynUser *pasynUser, const nds::Message& value)
{
    NDS_WRN("Test function is called!" );

    nds::Message response;
    response.messageType = "TEST";
    response.insert("TEXT", "Success.");
    response.insert("CODE", "0");
    doCallbacksMessage( response );
    return ndsSuccess;
}

ndsStatus Terminal::onSwitchOn()
{
    NDS_WRN("Creating thread. " );

//    if(epicsThreadCreate(
//        "Thread",
//        epicsThreadPriorityMedium,
//        epicsThreadGetStackSize(epicsThreadStackSmall),
//        readerThread,
//        (void*)this) == 0)
//    {
//        cantProceed(
//            "Can't create thread. Error: '%s'.\n",
//            strerror(errno));
//    }

    return ndsSuccess;
}

void Terminal::process()
{
    while(!_stopThread)
    {
        timestamp += 600;

        for (int i=0; i<10; ++i)
        {
            timestamp += 1;
            epicsTimeStamp stamp = (epicsTimeStamp)timestamp;

            epicsInt32* value;
            value = (epicsInt32*) malloc(sizeof(epicsInt32)*2);

            value[0] = stamp.secPastEpoch;
            value[1] = stamp.nsec+i;

            // Updating timestamp
            doCallbacksInt32Array(value, 2, interruptTimeStamp, stamp);

            memset(value, 0, sizeof(epicsInt32)*2);
            free(value);

            // Generating Event
            ++eventCounter;
            doCallbacksInt32(eventCounter, interruptEvent);

            epicsThreadSleep(0.000000001);
        }
        epicsThreadSleep(0.000000001);
    }
}
