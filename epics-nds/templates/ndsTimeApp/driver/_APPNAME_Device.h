/*
 * ExampleDevice.h
 *
 *  Created on: 26.07.2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef EXAMPLEDEVICE_H_
#define EXAMPLEDEVICE_H_

#include "ndsDevice.h"

class _APPNAME_Device: public nds::Device
{
private:
	int interruptTime;

	// It is required for firmware update testing
	int _counter;
	std::string _firmwares[4];

	int _timeRequestCounter;
public:
	/// Constructor of the Device class
	_APPNAME_Device(const std::string& name);

	/// Destructor of the Device class
	virtual ~_APPNAME_Device();

	/**
	 *  NDS Manager will call this function when it gets request to create instance of the device.
	 *	@return Function should return success in case all structures was created properly.
	 *
	 */
    virtual ndsStatus createStructure(const char* portName, const char* params);

    /**
     *  This function will be called before the device will be destroyed.
     *  This is deterministic destroy function.
     *  (see documentation for the destroy order)
     */
    virtual void destroy();

    /// Test will register additional device PV handlers
    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

	/// Example of device initialization
	virtual ndsStatus onEnterInit(nds::DeviceStates, nds::DeviceStates);

    /// Examples of state handlers
	/// Handler for the request to switch On device
    virtual ndsStatus onSwitchOn(nds::DeviceStates, nds::DeviceStates);

    /// Handler for the case when device leve state on for any other state
    virtual ndsStatus onLeaveOn(nds::DeviceStates, nds::DeviceStates);

    /// Handler for the case when device entering to error state
    virtual ndsStatus onError(nds::DeviceStates, nds::DeviceStates);

    virtual ndsStatus onEnterOn(nds::DeviceStates, nds::DeviceStates);

    virtual ndsStatus onEnterOff(nds::DeviceStates, nds::DeviceStates);

    virtual ndsStatus onLeaveOff(nds::DeviceStates, nds::DeviceStates);

    virtual ndsStatus getTime(asynUser *pasynUser, epicsInt32 *value, size_t nelements, size_t *nIn);


    // Example of single value handlers
    //virtual ndsStatus setInt32(asynUser* pasynUser, epicsInt32 value);
    //virtual ndsStatus getFloat64(asynUser* pasynUser, epicsFloat64 *value);

    // Example of array value handlers
    //virtual ndsStatus setInt32Array(asynUser* pasynUser, epicsInt32Array *inArray,  size_t nelem);
    //virtual ndsStatus getint32Array(asynUser* pasynUser, epicsint32Array *outArray, size_t nelem,  size_t *nIn);

    // Example of string value handlers
    //virtual ndsStatus setOctet(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered);
    //virtual ndsStatus getOctet(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason);

    ndsStatus onReset(nds::DeviceStates prevState, nds::DeviceStates currState);

};

#endif /* EXAMPLEDEVICE_H_ */
