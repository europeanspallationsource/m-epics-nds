/*
 * _APPNAME_Pulse.h
 *
 *  Created on: 22. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef APPNAME_PULSE_H_
#define APPNAME_PULSE_H_

#include <ndsPulse.h>

class Pulse: public nds::Pulse
{
public:
    Pulse();
    virtual ~Pulse();

    ndsStatus setOriginTime(asynUser* pasynUser, epicsFloat64 value);
    ndsStatus getOriginTime(asynUser* pasynUser, epicsFloat64* value);

    ndsStatus setDelay(asynUser* pasynUser, epicsFloat64 value);
    ndsStatus getDelay(asynUser* pasynUser, epicsFloat64* value);

    ndsStatus setInverted(asynUser* pasynUser, epicsInt32 value);
    ndsStatus getInverted(asynUser* pasynUser, epicsInt32* value);

    ndsStatus setEnabled(asynUser* pasynUser, epicsInt32 value);
    ndsStatus getEnabled(asynUser* pasynUser, epicsInt32* value);

    ndsStatus setWidth(asynUser* pasynUser, epicsFloat64 value);
    ndsStatus getWidth(asynUser* pasynUser, epicsFloat64* value);

};

#endif /* APPNAME_PULSE_H_ */
