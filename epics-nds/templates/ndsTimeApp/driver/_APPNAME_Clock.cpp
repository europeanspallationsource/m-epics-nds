/*
 * _APPNAME_Clock.cpp
 *
 *  Created on: 22. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <ndsDriverCommand.h>

#include "_APPNAME_Clock.h"

Clock::Clock()
{
    // TODO Auto-generated constructor stub

}

Clock::~Clock()
{
    // TODO Auto-generated destructor stub
}

ndsStatus Clock::setDutyCycle(asynUser* pasynUser, epicsFloat64 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Clock: setDutyCycle EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Clock::setDutyCycle(pasynUser, value);
}

ndsStatus Clock::getDutyCycle(asynUser* pasynUser, epicsFloat64* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Clock: getDutyCycle EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Clock::getDutyCycle(pasynUser, value);
}

ndsStatus Clock::setOriginTime(asynUser* pasynUser, epicsFloat64 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Clock: setOriginTime EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Clock::setOriginTime(pasynUser, value);
}

ndsStatus Clock::getOriginTime(asynUser* pasynUser, epicsFloat64* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Clock: getOriginTime EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Clock::getOriginTime(pasynUser, value);
}

ndsStatus Clock::setDelay(asynUser* pasynUser, epicsFloat64 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Clock: setDelay EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Clock::setDelay(pasynUser, value);
}

ndsStatus Clock::getDelay(asynUser* pasynUser, epicsFloat64* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Clock: getDelay EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Clock::getDelay(pasynUser, value);
}

ndsStatus Clock::setEnabled(asynUser* pasynUser, epicsInt32 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Clock: setEnabled EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Clock::setEnabled(pasynUser, value);
}

ndsStatus Clock::getEnabled(asynUser* pasynUser, epicsInt32* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Clock: getEnabled EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Clock::getEnabled(pasynUser, value);
}

ndsStatus Clock::setWidth(asynUser* pasynUser, epicsFloat64 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Clock: setWidth EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Clock::setWidth(pasynUser, value);
}

ndsStatus Clock::getWidth(asynUser* pasynUser, epicsFloat64* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Clock: getWidth EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Clock::getWidth(pasynUser, value);
}

ndsStatus Clock::setTimeEnd(asynUser* pasynUser, epicsFloat64 value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Clock: setTimeEnd EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Clock::setTimeEnd(pasynUser, value);
}

ndsStatus Clock::getTimeEnd(asynUser* pasynUser, epicsFloat64* value)
{
    nds::DriverCommand *command = (nds::DriverCommand*)pasynUser->userData;
    std::cout << "Clock: getTimeEnd EventID: "<< command->Args[0] << " TerminalID: "<<command->Args[1]  << std::endl;

    return nds::Clock::getTimeEnd(pasynUser, value);
}
