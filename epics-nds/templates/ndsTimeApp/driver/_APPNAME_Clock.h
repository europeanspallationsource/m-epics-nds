/*
 * _APPNAME_Clock.h
 *
 *  Created on: 22. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef APPNAME_CLOCK_H_
#define APPNAME_CLOCK_H_

#include <ndsClock.h>

class Clock: public nds::Clock
{
public:
    Clock();
    virtual ~Clock();


    ndsStatus setOriginTime(asynUser* pasynUser, epicsFloat64 value);
    ndsStatus getOriginTime(asynUser* pasynUser, epicsFloat64* value);

    ndsStatus setDelay(asynUser* pasynUser, epicsFloat64 value);
    ndsStatus getDelay(asynUser* pasynUser, epicsFloat64* value);

    ndsStatus setInverted(asynUser* pasynUser, epicsInt32 value);
    ndsStatus getInverted(asynUser* pasynUser, epicsInt32* value);

    ndsStatus setEnabled(asynUser* pasynUser, epicsInt32 value);
    ndsStatus getEnabled(asynUser* pasynUser, epicsInt32* value);

    ndsStatus setWidth(asynUser* pasynUser, epicsFloat64 value);
    ndsStatus getWidth(asynUser* pasynUser, epicsFloat64* value);

    ndsStatus setDutyCycle(asynUser* pasynUser, epicsFloat64 value);
    ndsStatus getDutyCycle(asynUser* pasynUser, epicsFloat64* value);

    ndsStatus setTimeEnd(asynUser* pasynUser, epicsFloat64 value);
    ndsStatus getTimeEnd(asynUser* pasynUser, epicsFloat64* value);

};

#endif /* APPNAME_CLOCK_H_ */
