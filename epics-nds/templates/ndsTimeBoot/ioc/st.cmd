#!../../bin/_ARCH_/_APPNAME_

## You may have to change _APPNAME_ to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

epicsEnvSet("DIV", "-")
epicsEnvSet("PREFIX", "_APPNAME_")
epicsEnvSet("PORT",   "PIPE")

## Register all support components
dbLoadDatabase "dbd/_APPNAME_.dbd"
_APPNAME__registerRecordDeviceDriver pdbbase

## ndsCreateDevice - initializes NDS device of selected type
## and populates it with specified parameters
## _APPNAME_  - device driver name
##			    List of supported drivers could be get by calling ndsListDrivers
## $(PORT) 	  - Device name in IOC for references, also asyn port name.
## Parameters - List of parameters represented by Key=Value pair and separated by   
##              comma   		
ndsCreateDevice "_APPNAME_", "$(PORT)", "TRG=2"

## Load record instances
dbLoadRecords "db/_APPNAME_.db",      "PREFIX=_APPNAME_, ASYN_PORT=$(PORT), TIMEOUT=1"

## Loading FTE's events
#  EVENT_NAME  - human readable event name
#  EVENT_ID    - event identification 
#  TERMINAL_ID - event's terminal identification 
dbLoadRecords "db/_APPNAME_FTE.template",   "PREFIX=_APPNAME_, ASYN_PORT=$(PORT), ASYN_ADDR=0, EVENT_NAME=1, EVENT_ID=1,  TERMINAL_ID=PXITRG1"

## Loading Pulse's events
dbLoadRecords "db/_APPNAME_Pulse.template", "PREFIX=_APPNAME_, ASYN_PORT=$(PORT), ASYN_ADDR=1, EVENT_NAME=2, EVENT_ID=13, TERMINAL_ID=PXISTAR2"

## Loading Clock's events
dbLoadRecords "db/_APPNAME_Clock.template", "PREFIX=_APPNAME_, ASYN_PORT=$(PORT), ASYN_ADDR=2, EVENT_NAME=3, EVENT_ID=21, TERMINAL_ID=PFI0"

## Run this to trace the stages of iocInit
#traceIocInit

cd ${TOP}/iocBoot/${IOC}
iocInit

## Start any sequence programs
#seq sncExample, "user=_USER_Host"
