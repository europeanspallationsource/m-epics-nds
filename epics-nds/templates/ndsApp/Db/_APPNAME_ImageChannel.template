# ----------------------------- [ Image Channel ] ----------------------------
# Template file: _APPNAME_ImageChannel.template
#

include "_APPNAME_Channel.template"

record(subArray, "$(PREFIX)-$(CHANNEL_ID)") {
	field(DESC, "Read image.")
	field(INP,  "$(PREFIX)-$(CHANNEL_ID)-RAW")
	field(FTVL, "UCHAR")
	field(MALM, "$(NELM)")
	field(NELM, "$(NELM)")
	field(PINI, "NO")
}

record(longin, "$(PREFIX)-$(CHANNEL_ID)-WDTH") {
	field(DESC, "Width current value.")
	field(DTYP, "asynInt32")
	field(EGU, "pix")	
	field(INP,  "@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))Width")
    field(FLNK, "$(PREFIX)-$(CHANNEL_ID)-HGHT")
	field(SCAN, "I/O Intr")
}

record(longin, "$(PREFIX)-$(CHANNEL_ID)-HGHT") {
	field(DESC, "Height current value.")
	field(EGU, "pix")	
	field(DTYP, "asynInt32")
	field(INP,  "@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))Height")
    field(FLNK, "$(PREFIX)-$(CHANNEL_ID)-RES")
}

record(longin, "$(PREFIX)-$(CHANNEL_ID)-RES") {
	field(DESC, "Specifies the resolution of channel")
	field(EGU,  "bps")		
	field(DTYP, "asynInt32")
	field(INP,	"@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))Resolution")
	field(FLNK, "$(PREFIX)-$(CHANNEL_ID)-SPP")			
}

record(longin, "$(PREFIX)-$(CHANNEL_ID)-SPP") {
	field(DESC, "Number of samples per pixel")
	field(EGU,  "spp")		
	field(DTYP, "asynInt32")
	field(INP,	"@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))SamplesPerPixel")
    field(FLNK, "$(PREFIX)-$(CHANNEL_ID)-SIZE")	
}

record(calcout, "$(PREFIX)-$(CHANNEL_ID)-SIZE") {
	field(DESC, "Image size")
	field(EGU,  "byte")
	field(INPA, "$(PREFIX)-$(CHANNEL_ID)-WDTH")
	field(INPB, "$(PREFIX)-$(CHANNEL_ID)-HGHT")
	field(INPC, "$(PREFIX)-$(CHANNEL_ID)-RES")
	field(INPD, "$(PREFIX)-$(CHANNEL_ID)-SPP")	
	field(CALC, "A*B*C*D/8" )
	field(OUT,  "$(PREFIX)-$(CHANNEL_ID).NELM" )
}

record(waveform, "$(PREFIX)-$(CHANNEL_ID)-RAW") {
	field(DESC, "Raw image buffer.")
	field(DTYP, "asynOctetRead")
	field(INP,  "@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR)) BufferInt8")
	field(FTVL, "UCHAR")
	field(NELM, "$(NELM)")
	field(PINI, "NO")
	field(SCAN, "I/O Intr")
    field(FLNK, "$(PREFIX)-$(CHANNEL_ID)")	
}

record(longin, "$(PREFIX)-$(CHANNEL_ID)-ORGX") {
	field(DESC, "Reagion X current value.")
	field(DTYP, "asynInt32")
	field(EGU, "pix")	
	field(INP,  "@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))OriginX")
}

record(longin, "$(PREFIX)-$(CHANNEL_ID)-ORGY") {
	field(DESC, "Region Y current value.")
	field(DTYP, "asynInt32")
	field(EGU, "pix")	
	field(INP,  "@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))OriginY")
}

# List of image types should be changed according with
# hardware supported types.
record(mbbo, "$(PREFIX)-$(CHANNEL_ID)-IMGT") {
	field(DESC, "Available image types.")
	field(DTYP, "asynInt32")
	field(OUT,  "@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))ImageType")
	field(ZRVL, "0")
	field(ZRST, "GRAYSCALE")
	field(ONVL, "1")
	field(ONST, "RGB")
	field(TWVL, "2")
	field(TWST, "BGR")
	field(THVL, "3")
	field(THST, "RGBG")
	field(FRVL, "4")
	field(FRST, "GRGB")
	field(FVVL, "5")
	field(FVST, "RGGB")
}

# List of image formats should be changed according with 
# hardware supported formats
record(mbbo, "$(PREFIX)-$(CHANNEL_ID)-FMTS") {
	field(DESC, "Available image formats.")
	field(DTYP, "asynInt32")
	field(OUT,  "@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))ImageFormat")
	field(ZRVL, "0")
	field(ZRST, "32x32 8bits")
	field(ONVL, "1")
	field(ONST, "64x64 8bits")
	field(TWVL, "2")
	field(TWST, "128x128 8bits")
}
