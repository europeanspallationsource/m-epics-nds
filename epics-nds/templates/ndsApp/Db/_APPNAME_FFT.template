# ----------------------------------------------------------------------------|
############################## FFT transform read #############################
# Add this file to propriate template where this functionality required.
# ----------------------------------------------------------------------------|

record(waveform, "$(PREFIX)-$(CHANNEL_ID)-FFTB") {
	field(DESC, "Read parameters of Fourier transform")
	field(DTYP, "asynFloat64ArrayIn")
	field(INP,	"@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))FFTBuffer")
	field(FTVL, "DOUBLE")
	field(NELM, "$(NELM)")
	field(PINI, "NO")
	field(SCAN, "I/O Intr")
}

record(ao, "$(PREFIX)-$(CHANNEL_ID)-FFTN") {
	field(DESC, "Configure the size of frame")
	field(DTYP, "asynFloat64")
	field(OUT,	"@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))FFTSize")
}

record(ao, "$(PREFIX)-$(CHANNEL_ID)-FFTO") {
	field(DESC, "Configure the size of frame")
	field(DTYP, "asynFloat64")
	field(OUT,	"@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))FFTOverlap")
}

record(mbbo, "$(PREFIX)-$(CHANNEL_ID)-FFTW") {
	field(DESC, "Specifies windowing mode for FFT")
	field(DTYP, "asynInt32")
	field(OUT,	"@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))FFTWindow")
	field(ZRVL, "0")
	field(ZRST, "NONE")
	field(ONVL, "1")
	field(ONST, "BARLETT")
	field(TWVL, "2")
	field(TWST, "BLACKMAN")
	field(THVL, "3")
	field(THST, "FLATTOP")
	field(FRVL, "4")
	field(FRST, "HANN")
	field(FVVL, "5")
	field(FVST, "HAMMING")
	field(FVVL, "6")
	field(FVST, "TUKEY")
	field(FVVL, "7")
	field(FVST, "WELCH")
	field(VAL,	"0")
	field(IVOV, "0")
}

record(ao, "$(PREFIX)-$(CHANNEL_ID)-FFTS") {
	field(DESC, "Set the level of smoothing the FFT")
	field(DTYP, "asynFloat64")
	field(OUT,	"@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))FFTSmoothing")
}

record(longout, "$(PREFIX)-$(CHANNEL_ID)-FFT") {
	field(DESC, "Start FFT for current buffer")
	field(DTYP, "asynInt32")
	field(INP,	"@asyn($(ASYN_PORT).$(CHANNEL_PREFIX), $(ASYN_ADDR))FFT")
	field(PINI, "NO")
	field(SCAN, "Passive")
}
