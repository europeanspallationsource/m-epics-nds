/*
 * DIChannel.h
 *
 *  Created on: 19. jul. 2013
 *      Author: Slava Isaev
 */

#ifndef DICHANNEL_H_
#define DICHANNEL_H_

#include <epicsTypes.h>
#include <epicsTimer.h>

#include "ndsTaskService.h"
#include "ndsTimer.h"
#include "ndsADIOChannel.h"

class ExDIChannel: public nds::ADIOChannel
{
protected:
    int _intIdDigital;
    epicsUInt32 _digValue;

    nds::TimerPtr _timer;

public:
    ExDIChannel();
    virtual ~ExDIChannel();

    /// Test will register additional device PV handlers
    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    epicsTimerNotify::expireStatus handleOnTimer(nds::TaskService &service, const epicsTime & currentTime );

    ndsStatus onSwitchOn(nds::ChannelStates prevState, nds::ChannelStates currState);

    virtual void destroy();
};


#endif /* DICHANNEL_H_ */
