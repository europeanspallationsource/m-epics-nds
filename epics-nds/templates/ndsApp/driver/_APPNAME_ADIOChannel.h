/*
 * exChannel.h
 *
 *  Created on: 18. jul. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef EXCHANNEL_H_
#define EXCHANNEL_H_

#include <string>
#include <epicsMutex.h>
#include <epicsTypes.h>
#include <epicsTimer.h>

#include "ndsTypes.h"
#include "ndsTimer.h"
#include "ndsADIOChannel.h"
#include "ndsChannel.h"
#include "ndsTaskService.h"
#include "ndsPeriodicTask.h"
#include "ndsPollingTask.h"

class Event;

class ExADIOChannel: public nds::ADIOChannel
{
protected:
    epicsMutexId outFileMutex;
    char *fileName; ///< Name of the input pipe.
    int   fileFD;
    FILE *outFileHandle;
    volatile float lastValueRead;

//    volatile int stopped;
    /** Event file descriptor for sending events to the threads. */
//    int eventSamplerFD;
//    int eventReaderFD;
//    int eventAckFD;

    int channelType;
    std::string prefix;
    std::string suffix;
    int userIndex;

    int idFindChannel;
    int idFindChannelGroup;
    int idEvent;
    int idTimeStamp;
    int idReason;

	/// additional buffer used with negative trigger delay
	epicsFloat32* ringBuffer;

	/// positions where lastValueRead is written in _BufferFloat32 and ringBuffer
	size_t bufferPosition;

	size_t ringBufferPosition;

	/// trigger delay expressed in number of samples
	size_t sampleDelay;

	size_t ringBufferSize;

	/// used only with negativ delay - the position of a value in ringBuffer to be copied to _BufferFloat32
	int triggerPosition;

	/// used only with negativ delay, determines if acquisition should stop
	bool stopAcquisition;

	/// used in the state machines of dataAcquisition() and preDataAcquisition()
	int state;

	/// used only with negativ delay - ringBufferPosition of the value that breaks the trigger condition first
	int stopTriggerPosition;

    epicsFloat64 localTriggerDelay;

    /// used to verify if the _TriggerDelay has changed
    size_t oldSampleDelay;


	/// enumerted states for state machines dataAcquisition() and preDataAcquisition()
	enum states {
		STATE_NOT_TRIGGERED,
		STATE_TRIGGERED,
		STATE_TRANSITION
	};

	int eventCounter;

	epicsTime timestamp;
	nds::TimerPtr eventTimer;

	nds::PeriodicTask* taskPeriodic;
	nds::PollingTask*  taskPolling;

public:
    /// Constructor
    ExADIOChannel(int type, const std::string& prefix, const std::string&, int i, int bufSize);

    /// Destructor
    virtual ~ExADIOChannel();

    /// declaration of the default PV Record handler
    virtual ndsStatus setClockFrequency(asynUser* pasynUser, epicsFloat64 value);

    /// Concrete channel specific member
    ndsStatus createChannelPipeAndThreads();

    /// Concrete channel specific member
    void processSamplingBody(nds::TaskServiceBase& service);

    /// Concrete channel specific member
    void processReadBody(nds::TaskServiceBase& service, const struct epoll_event& event);

	/// used to sample data when trigger delay is non-negative
	void dataAcquisition();

	/// used to sample data when trigger delay is negative
	void preDataAcquisition();

    /// Concrete channel specific member
    ndsStatus writeToOutput(epicsFloat64 value);

    /// Test will register additional device PV handlers
    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    virtual ndsStatus setValueFloat64(asynUser* pasynUser, epicsFloat64 value);

    ndsStatus printFrequency(asynUser* pasynUser, const nds::Message& value);

    ndsStatus findChannelByName(asynUser *pasynUser, const char *value, size_t maxChars, size_t *nActual);

    ndsStatus findChannelGroup(asynUser *pasynUser, const char *value, size_t maxChars, size_t *nActual);

    /// Member of class which is implements event generation
    ndsStatus getValueFloat64(asynUser* pasynUser, epicsFloat64 *value);

    /// Member of class which is implements event generation
    epicsTimerNotify::expireStatus generateEvent(nds::TaskService &service, const epicsTime & currentTime);

    /// Example how to handle reasons arguments from the reson's handler
    ndsStatus setReason(asynUser* pasynUser, epicsInt32 value);

    ndsStatus onSwitchOn(nds::ChannelStates prevState, nds::ChannelStates currState);

    ndsStatus onProcessingRequested(nds::ChannelStates prevState,
            nds::ChannelStates currState);

    ndsStatus stopProcessing(nds::ChannelStates prevState,
            nds::ChannelStates currState);

    ndsStatus closeFiles();

    virtual void destroy();
};

#endif /* EXCHANNEL_H_ */
