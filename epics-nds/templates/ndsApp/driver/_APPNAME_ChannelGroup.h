/*
 * exChannelGroup.h
 *
 *  Created on: 19. sep. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef EXCHANNELGROUP_H_
#define EXCHANNELGROUP_H_

#include <ndsChannelGroup.h>

class ExChannelGroup:public nds::ChannelGroup
{
  public:
    ExChannelGroup(const std::string& name);
    virtual ~ExChannelGroup();

    ndsStatus onReset( nds::ChannelStates prevState, nds::ChannelStates currState);

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    virtual ndsStatus setTriggerDelay(asynUser* pasynUser, epicsInt32 value);

    virtual ndsStatus getTriggerDelay(asynUser* pasynUser, epicsInt32 *value);

    virtual ndsStatus setClockMultiplier(asynUser* pasynUser, epicsInt32 value);

    virtual ndsStatus getClockMultiplier(asynUser* pasynUser, epicsInt32 *value);

    virtual ndsStatus setClockDivisor(asynUser* pasynUser, epicsInt32 value);

    virtual ndsStatus getClockDivisor(asynUser* pasynUser, epicsInt32 *value);

    virtual ndsStatus setRange(asynUser* pasynUser, epicsInt32 value);

    virtual ndsStatus getRange(asynUser* pasynUser, epicsInt32 *value);

    virtual ndsStatus getSamplesCount(asynUser *pasynUser, epicsInt32 *value);

    virtual ndsStatus onTriggerConditionParsed(asynUser *pasynUser, const nds::Trigger& trigger);
};


#endif /* EXCHANNELGROUP_H_ */
