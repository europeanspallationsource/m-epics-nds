/*
 * exImageChannel.h
 *
 *  Created on: 22. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef EXNDARRAYCHANNEL_H_
#define EXNDARRAYCHANNEL_H_

#include <cstddef>

#include "ndsNDArrayChannel.h"

/***
 *  Example implementation of image channel with areaDetector support.
 */
class ExNDArrayChannel : public nds::NDArrayChannel
{
public:
	/// C-tor
	ExNDArrayChannel( int maxBuffers, size_t maxMemory );

	/// D-tor
	virtual ~ExNDArrayChannel();

	/// Example of image source
	virtual ndsStatus setLoadFile(asynUser *pasynUser, const char *str, size_t numchars, size_t *nbytesTransfered);

};

#endif /*  */
