/*
 * exChannelGroup.cpp
 *
 *  Created on: 19. sep. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <epicsThread.h>
#include "ndsTriggerCondition.h"

#include "_APPNAME_ChannelGroup.h"

ExChannelGroup::ExChannelGroup(const std::string& name) :
        nds::ChannelGroup(name)
{
    // TODO Auto-generated constructor stub

    /*
     * Registering state change handlers.
     * State machine supports 3 levels of handlers.
     */

    /*
     * 1st level onRequest handlers
     * These handlers are executed before state change and should succeed
     *  to allow transfer to new state.
     */
//      registerOnRequestStateHandler(CHANNEL_STATES_OFF, CHANNEL_STATES_ON, );
    /*
     * 2nd level onLeave handlers for the current state
     * These handlers are executed when state machine leaving current state.
     */
//      registerOnLeaveStateHandler();
    /*
     *  3rd level onEnter handler for the new state
     *  These Hnadlers are executed right after the state was changed.
     */
    registerOnEnterStateHandler(nds::CHANNEL_STATE_RESETTING, // Requested state
            boost::bind(&ExChannelGroup::onReset, this, _1, _2));

    // If you like synchronize switching ON of all channels with ChannelGroup
    // uncomment following lines:
    //        registerOnEnterStateHandler(CHANNEL_STATE_ON,
    //                  boost::bind(&ChannelGroup::switchChannelsOn, this) );

    // If you like synchronize switching OFF of all channels with ChannelGroup
    // uncomment following lines:
    //        registerOnEnterStateHandler(CHANNEL_STATE_OFF,
    //                  boost::bind(&ChannelGroup::switchChannelsOff, this) );
}

ExChannelGroup::~ExChannelGroup()
{
    // TODO Auto-generated destructor stub
}

ndsStatus ExChannelGroup::onReset(nds::ChannelStates prevState,
        nds::ChannelStates currState)
{
    NDS_INF("Reset DI segment.");
    epicsThreadSleep(1.0);
    NDS_INF("Resetting complete.");
    getCurrentStateObj()->requestState(this, nds::CHANNEL_STATE_PROCESSING);
    return ndsSuccess;
}

ndsStatus ExChannelGroup::registerHandlers(nds::PVContainers* pvContainers)
{
    return nds::ChannelGroup::registerHandlers(pvContainers);
}

ndsStatus ExChannelGroup::setTriggerDelay(asynUser* pasynUser, epicsInt32 value)
{
    NDS_INF("setTriggerDelay");
    return nds::ChannelGroup::setTriggerDelay(pasynUser, value);
}

ndsStatus ExChannelGroup::getTriggerDelay(asynUser* pasynUser, epicsInt32 *value)
{
    NDS_INF("getTriggerDelay");
    return nds::ChannelGroup::getTriggerDelay(pasynUser, value);
}

ndsStatus ExChannelGroup::setClockMultiplier(asynUser* pasynUser, epicsInt32 value)
{
    NDS_INF("setClockMultiplier");
    return nds::ChannelGroup::setClockMultiplier(pasynUser, value);
}

ndsStatus ExChannelGroup::getClockMultiplier(asynUser* pasynUser, epicsInt32 *value)
{
    NDS_INF("getClockMultiplier");
    return nds::ChannelGroup::getClockMultiplier(pasynUser, value);
}

ndsStatus ExChannelGroup::setClockDivisor(asynUser* pasynUser, epicsInt32 value)
{
    NDS_INF("setClockDivisor");
    return nds::ChannelGroup::setClockDivisor(pasynUser, value);
}

ndsStatus ExChannelGroup::getClockDivisor(asynUser* pasynUser, epicsInt32 *value)
{
    NDS_INF("getClockDivisor");
    return nds::ChannelGroup::getClockDivisor(pasynUser, value);
}

ndsStatus ExChannelGroup::setRange(asynUser* pasynUser, epicsInt32 value)
{
    NDS_INF("setRange");
    return nds::ChannelGroup::setRange(pasynUser, value);
}

ndsStatus ExChannelGroup::getRange(asynUser* pasynUser, epicsInt32 *value)
{
    NDS_INF("getRange");
    return nds::ChannelGroup::getRange(pasynUser, value);
}

ndsStatus ExChannelGroup::getSamplesCount(asynUser *pasynUser, epicsInt32 *value)
{
    NDS_INF("getSamplesCount");
    return nds::ChannelGroup::getSamplesCount(pasynUser, value);
}

ndsStatus ExChannelGroup::onTriggerConditionParsed(asynUser *pasynUser, const nds::Trigger& trigger)
{
    NDS_INF("onTriggerConditionParsed");

    // Iterating through trigger conditions
//    for( nds::ConditionVector::iterator itr= trigger.conditions.begin();
//            itr!=trigger.conditions.end();
//            ++itr)
    if ( !trigger.isEmpty() )
    {
        nds::ITriggerCondition* cond = trigger.getFirst();

        // subject
        NDS_INF("ChannelNumber: %d", cond->getChannel()->getChannelNumber() );
        NDS_INF("ChannelGroup: %s", cond->getChannel()->getChannelGroup()->getName().c_str() );

        // polarity
        if ( cond->isNegated() )
        {
            // inverse polarity
            NDS_INF("INVERSE");
        }

        // rising falling edge
        switch ( cond->getOperation() )
        {
        case NDS_TC_OP_NONE:          // no operation
            NDS_INF("Operation is: NONE");
            break;
        case NDS_TC_OP_GT:            // greater then
            NDS_INF("Operation is: GT");
            break;
        case NDS_TC_OP_LT:            // less then
            NDS_INF("Operation is: LT");
            break;
        case NDS_TC_OP_RISING:        // rising edge
            NDS_INF("Operation is: RISING");
            break;
        case NDS_TC_OP_FALLING:       // falling edge
            NDS_INF("Operation is: FALLING");
            break;
        case NDS_TC_OP_RISING_FALLING: // rising edge and falling edge
            NDS_INF("Operation is: RISING || FALLINIG");
            break;
        default:
            NDS_INF("Operation is: UNKNOWN");
            break;
        };
    }
    return ndsSuccess;
}

