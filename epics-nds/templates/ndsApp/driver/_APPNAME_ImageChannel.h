/*
 * exImageChannel.h
 *
 *  Created on: 22. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef EXIMAGECHANNEL_H_
#define EXIMAGECHANNEL_H_

#include <cstddef>

#ifdef AREA_DETECTOR
#include "ndsNDArrayChannel.h"
#else
#include "ndsImageChannel.h"
#endif

#ifdef AREA_DETECTOR
class ExImageChannel : public nds::NDArrayChannel
#else
class ExImageChannel : public nds::ImageChannel
#endif
{
public:
	ExImageChannel(
			const std::string& file,
			const std::string& pref,
			int idx);
	virtual ~ExImageChannel();
};

#endif /* EXIMAGECHANNEL_H_ */
