/*
 * example.h
 *
 *  Created on: 18. jul. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef DEF__APPNAME__H_
#define DEF__APPNAME__H_

#define CHANNEL_TYPE_AI      0
#define CHANNEL_TYPE_AO      1
#define CHANNEL_TYPE_DI      2
#define CHANNEL_TYPE_DO      3
#define CHANNEL_TYPE_DIO     4
#define CHANNEL_TYPE_IMAGE   5
#define CHANNEL_TYPE_COUNT   (CHANNEL_TYPE_IMAGE + 1)


#endif /* EXAMPLE_H_ */
