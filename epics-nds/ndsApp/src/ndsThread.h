/*
 * ndsThread.h
 *
 *  Created on: 8. avg. 2013
 *      Author: Slava Isaev
 */

#ifndef NDSTHREAD_H_
#define NDSTHREAD_H_

#include <map>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

#include <epicsThread.h>
#include <epicsEvent.h>

#include "ndsTaskService.h"
#include "ndsBaseTask.h"

namespace nds
{

class Thread;

class Thread: public BaseTask
{
private:
    Thread();
    Thread(const Thread&);
    Thread& operator=(const Thread&);

protected:
    TaskServiceBase* _service;
    epicsThreadId _thread;

    virtual ndsStatus startPrv();
    virtual ndsStatus cancelPrv();

public:
    Thread(const std::string& name,
           unsigned int     stackSize,
           unsigned int     priority,
           TaskServiceBase* service );

    virtual ~Thread();

    void runPrv();

    virtual void run(TaskServiceBase &service);

    void resume();
};


} /* namespace nds */
#endif /* NDSTHREAD_H_ */
