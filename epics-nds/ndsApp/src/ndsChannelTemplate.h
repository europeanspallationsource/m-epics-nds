/*
 * ndsChannel.h
 *
 *  Created on: 18. jul. 2012
 *      Authors: Klemen Zagar, Slava isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSCHANNEL_H_
#define NDSCHANNEL_H_

#include <map>
#include <boost/bind.hpp>
#include <loki/TypeManip.h>
#include "errlog.h"

#include "ndsTypes.h"
#include "ndsPVContainer.h"
#include "ndsBase.h"
#include "ndsAsynMacro.h"
#include "ndsChannelStates.h"
#include "ndsBaseChannel.h"
#include "ndsDevice.h"

namespace nds
{

class ChannelGroup;
class Device;

#define NDS_CHANNEL_CLASS ChannelTemplate<T>

/** Channel tempalte class
 *
 */
template<typename T>
class ChannelTemplate: public BaseChannel
{
    friend class ChannelGroup;
public:

    typedef T BufferType;
	enum
	{
		asyn_type_index = Loki::TL::IndexOf<CHANNEL_BUFFER_TYPES, T>::value
	};
	typedef typename Loki::TL::TypeAt<CHANNEL_VALUE_TYPES, asyn_type_index>::Result ValueType;

    /// Ctor
	ChannelTemplate()
	{

	}
	/// Dtor
	virtual ~ChannelTemplate()
	{

	}

	/// Register PV record's handlers
    virtual ndsStatus registerHandlers(PVContainers* pvContainers)
    {
    	BaseChannel::registerHandlers(pvContainers);

		#define NDS_INSTANTIATE NDS_REG_SIMPL_FUNCTIONS
    	#define NDS_INSTANTIATE_OCTET NDS_REG_OCTET_FUNCTIONS
    	#define NDS_INSTANTIATE_ARRAY NDS_REG_ARRAY_FUNCTIONS
    		NDS_INSTANTIATE_FOR_EACH_CHANNEL_FUNCTION
    	#undef NDS_INSTANTIATE
    	#undef NDS_INSTANTIATE_OCTET
    	#undef NDS_INSTANTIATE_ARRAY

    	NDS_PV_REGISTER_INT32("FFT", &ChannelTemplate<T>::setFFT, &ChannelTemplate<T>::getInt32, &_interruptIdInt32);

    	registerBufferHandler(pvContainers);
    	return ndsSuccess;
    }

    ndsStatus registerBufferHandler(PVContainers* pvContainers)
    {
        return registerBufferHandler(pvContainers, Loki::Type2Type<T>());
    }

    ndsStatus registerBufferHandler(PVContainers* pvContainers, Loki::Type2Type<epicsInt8>)
    {
    	NDS_PV_REGISTER_INT8ARRAY("Buffer",     &ChannelTemplate<T>::setBuffer,     &ChannelTemplate<T>::getBuffer,     &_interruptIdBuffer);
    	NDS_PV_REGISTER_INT8ARRAY("Conversion", &ChannelTemplate<T>::setConversion, &ChannelTemplate<T>::getConversion, &_interruptIdConversion);
    	NDS_PV_REGISTER_INT8ARRAY("Filter",     &ChannelTemplate<T>::setFilter,     &ChannelTemplate<T>::getFilter,     &_interruptIdFilter);
    	NDS_PV_REGISTER_INT8ARRAY("FFTBuffer",  &ChannelTemplate<T>::setFFTBuffer,  &ChannelTemplate<T>::getFFTBuffer,  &_interruptIdFFTBuffer);

    	NDS_PV_REGISTER_INT32("Value",  &ChannelTemplate<T>::setValue,  &ChannelTemplate<T>::getValue,  &_interruptIdValue);

    	return ndsSuccess;
    }

    ndsStatus registerBufferHandler(PVContainers* pvContainers, Loki::Type2Type<epicsInt16>)
    {
    	NDS_PV_REGISTER_INT16ARRAY("Buffer",     &ChannelTemplate<T>::setBuffer,  	 &ChannelTemplate<T>::getBuffer,     &_interruptIdBuffer);
    	NDS_PV_REGISTER_INT16ARRAY("Conversion", &ChannelTemplate<T>::setConversion, &ChannelTemplate<T>::getConversion, &_interruptIdConversion);
    	NDS_PV_REGISTER_INT16ARRAY("Filter",     &ChannelTemplate<T>::setFilter,     &ChannelTemplate<T>::getFilter,     &_interruptIdFilter);
    	NDS_PV_REGISTER_INT16ARRAY("FFTBuffer",  &ChannelTemplate<T>::setFFTBuffer,  &ChannelTemplate<T>::getFFTBuffer,  &_interruptIdFFTBuffer);

    	return ndsSuccess;
    }

    ndsStatus registerBufferHandler(PVContainers* pvContainers, Loki::Type2Type<epicsInt32>)
    {
    	NDS_PV_REGISTER_INT32ARRAY("Buffer",     &ChannelTemplate<T>::setBuffer,     &ChannelTemplate<T>::getBuffer,     &_interruptIdBuffer);
    	NDS_PV_REGISTER_INT32ARRAY("Conversion", &ChannelTemplate<T>::setConversion, &ChannelTemplate<T>::getConversion, &_interruptIdConversion);
    	NDS_PV_REGISTER_INT32ARRAY("Filter",     &ChannelTemplate<T>::setFilter,     &ChannelTemplate<T>::getFilter,     &_interruptIdFilter);
    	NDS_PV_REGISTER_INT32ARRAY("FFTBuffer",  &ChannelTemplate<T>::setFFTBuffer,  &ChannelTemplate<T>::getFFTBuffer,  &_interruptIdFFTBuffer);

    	return ndsSuccess;
    }

    ndsStatus registerBufferHandler(PVContainers* pvContainers, Loki::Type2Type<epicsFloat32>)
    {
    	NDS_PV_REGISTER_FLOAT32ARRAY("Buffer",     &ChannelTemplate<T>::setBuffer, &ChannelTemplate<T>::getBuffer, &_interruptIdBuffer);
    	NDS_PV_REGISTER_FLOAT32ARRAY("Conversion", &ChannelTemplate<T>::setConversion, &ChannelTemplate<T>::getConversion, &_interruptIdConversion);
    	NDS_PV_REGISTER_FLOAT32ARRAY("Filter",     &ChannelTemplate<T>::setFilter, &ChannelTemplate<T>::getFilter, &_interruptIdFilter);
    	NDS_PV_REGISTER_FLOAT32ARRAY("FFTBuffer",  &ChannelTemplate<T>::setFFTBuffer, &ChannelTemplate<T>::getFFTBuffer, &_interruptIdFFTBuffer);

    	return ndsSuccess;
    }

    ndsStatus registerBufferHandler(PVContainers* pvContainers, Loki::Type2Type<epicsFloat64>)
    {
    	NDS_PV_REGISTER_FLOAT64ARRAY("Buffer",     &ChannelTemplate<T>::setBuffer, &ChannelTemplate<T>::getBuffer, &_interruptIdBuffer);
    	NDS_PV_REGISTER_FLOAT64ARRAY("Conversion", &ChannelTemplate<T>::setConversion, &ChannelTemplate<T>::getConversion, &_interruptIdConversion);
    	NDS_PV_REGISTER_FLOAT64ARRAY("Filter",     &ChannelTemplate<T>::setFilter, &ChannelTemplate<T>::getFilter, &_interruptIdFilter);
    	NDS_PV_REGISTER_FLOAT64ARRAY("FFTBuffer",  &ChannelTemplate<T>::setFFTBuffer, &ChannelTemplate<T>::getFFTBuffer, &_interruptIdFFTBuffer);
    	return ndsSuccess;
    }

    /** Period of the sampling clock (expressed in nanoseconds). */
    long long clockPeriod()
    {
    	return 1.0e9/_ClockFrequency;
    }


    /**
     * Function to notify EPICS device support driver that new value ready.
     *
     * There are 2 types of data supported in NDS - single value and buffer.
     * This function used when user need to notify driver that next single value
     * is ready to be processed.
     *
     * \parameter value - new value to pass to record.
     */
    ndsStatus inputChanged(epicsFloat64 value)
    {
    	return ndsSuccess;
    }

    /**
     * Function to notify EPICS device support driver that new value ready.
     *
     * There are 2 types of data supported in NDS - single value and buffer.
     * This function used when user need to notify driver that next single value
     * is ready to be processed.
     *
     * \parameter value - new value to pass to record.
     */
    ndsStatus inputChanged(epicsInt32   value)
    {
    	return ndsSuccess;
    }

    /**
     * Function to notify EPICS device support driver that new Int8Array buffer ready
     * to be processed.
     *
     * There are 2 types of data supported in NDS - single value and buffer.
     * This function used when user need to notify driver that next buffer
     * is ready to be processed.
     * Buffer of according type will be taken and send to asynDriver.
     *
     */
    ndsStatus bufferProcessed()
    {
    	bufferProcessedInternal(Loki::Type2Type<T>());
    }

    ndsStatus bufferProcessed(Loki::Type2Type<epicsInt8>)
    {
    	return doCallbacksInt8Array(_Buffer, _BufferSize, _interruptIdBuffer, _portAddr);
    }

    ndsStatus bufferProcessed(Loki::Type2Type<epicsInt16>)
    {
    	return doCallbacksInt16Array(_Buffer, _BufferSize, _interruptIdBuffer, _portAddr);
    }

    ndsStatus bufferProcessed(Loki::Type2Type<epicsInt32>)
    {
    	return doCallbacksInt32Array(_Buffer, _BufferSize, _interruptIdBuffer, _portAddr);
    }

    ndsStatus bufferProcessed(Loki::Type2Type<epicsFloat32>)
    {
    	return doCallbacksFloat32Array(_Buffer, _BufferSize, _interruptIdBuffer, _portAddr);
    }

    ndsStatus bufferProcessed(Loki::Type2Type<epicsFloat64>)
    {
    	return doCallbacksFloat64Array(_Buffer, _BufferSize, _interruptIdBuffer, _portAddr);
    }

    /** \brief Output channels: the contents of the buffer to output to the channel. [PV record setter]
     *
	 *  Asyn Reason: BufferFloat32()\n
	 *  PV Record suffix: no suffix\n
	 *
	 *  Output channels: the contents of the buffer to output to the channel.
	 *  The values are subject to conversion from engineering units to raw units.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setBuffer(asynUser* pasynUser, T *inArray,  size_t nelem)
    {
    	return ndsSuccess;
    }

    /** \brief Input channels: retrieve the next buffer. [PV record getter]
     *
	 *  Asyn Reason: BufferFloat32\n
	 *  PV Record suffix: no suffix\n
	 *
	 *  Input channels: retrieve the next buffer.
	 *  NORD field specifies how many samples are read. NELM field specifies maximal number of samples.
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getBuffer(asynUser* pasynUser, T *outArray, size_t nelem,  size_t *nIn)
    {
    	return ndsSuccess;
    }

    /** \brief Set conversion of the value. [PV record setter]
     *
	 *  Asyn Reason: Conversion\n
	 *  PV Record suffix: CVT\n
	 *
	 *  Define conversion from raw values to engineering values as described in section 4.4.
	 *  Conversion is defined as a segmented cubic spline.
	 *  Waveform array consists of five-tuples, whose elements are the start of the segment,
	 *  followed by the four cubic spline coefficients.
	 *  If an empty array is given, conversion is performed according to the LINR,
	 *  EGUF and EGUL fields of the IN and OUT records.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setConversion(asynUser* pasynUser, T *inArray,  size_t nelem)
    {
    	return ndsSuccess;
    }

    /** \brief Value conversion. [PV record getter]
     *
	 *  Asyn Reason: Conversion\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getConversion(asynUser* pasynUser, T *outArray, size_t nelem,  size_t *nIn)
    {
    	return ndsSuccess;
    }

    /** \brief Set the filter for the analog input signal. [PV record setter]
     *
	 *  Asyn Reason: Filter\n
	 *  PV Record suffix: FILT\n
	 *
	 *  See section 4.4. of programmers guide.
	 *  If an empty array is given, the SMOO field of the IN record is used.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setFilter(asynUser* pasynUser, T *inArray,  size_t nelem)
    {
    	return ndsSuccess;
    }

    /** \brief [PV record getter]
     *
	 *  Asyn Reason: Filter\n
	 *  PV Record suffix: -\n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getFilter(asynUser* pasynUser, T *outArray, size_t nelem,  size_t *nIn)
    {
    	return ndsSuccess;
    }

    /** \brief  [PV record setter]
     *
	 *  Asyn Reason: ValueInt32\n
	 *  PV Record suffix: IN\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setValue(asynUser* pasynUser, ValueType value)
    {
    	_Value = value;
    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: ValueInt32\n
	 *  PV Record suffix: OUT\n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getValue(asynUser* pasynUser, ValueType *value)
    {
		*value = _Value;
    	return ndsSuccess;
    }

    /** \brief Set decimation factor [PV record setter]
     *
	 *  Asyn Reason: DecimationFactor\n
	 *  PV Record suffix: DECF\n
	 *
	 *  For input channels, only every DECF-th sample (or frame) will be sampled.
	 *  For output channel outputting a waveform, only every DECF-th sample will be output.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setDecimationFactor(asynUser* pasynUser, epicsInt32 value)
    {
    	_DecimationFactor = value;

    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason:\n
	 *  PV Record suffix:\n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getDecimationFactor(asynUser* pasynUser, epicsInt32 *value)
    {
    	*value = _DecimationFactor;
    	return ndsSuccess;
    }

    /** \brief  [PV record setter]
     *
	 *  Asyn Reason: DecimationOffset\n
	 *  PV Record suffix: DECO\n
	 *
	 *  Specify the index of the first sample that is not decimated.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setDecimationOffset(asynUser* pasynUser, epicsInt32 value)
    {
    	_DecimationOffset = value;
    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: \n
	 *  PV Record suffix: \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getDecimationOffset(asynUser* pasynUser, epicsInt32 *value)
    {
    	*value = _DecimationOffset;
    	return ndsSuccess;
    }

    /** \brief Set the type of output signal that is generated by the output channel. [PV record setter]
     *
	 *  Asyn Reason:setSignalType\n
	 *  PV Record suffix: SGNT\n
	 *
	 *	It can be one of the following values:
	 *	0	WAVEFORM	Waveform as defined in WF record.
	 *	1	SPLINE	Spline (piecewise-cubic function) as defined in SPLN record
	 *	2	SIN	Sine curve (amplitude SGNA, offset SGNO, frequency SGNF, phase SGNP).
	 *	3	PULSE	Pulse train (amplitude SGNA, offset SGNO, frequency SGNF, phase SGNP, duty cycle SGNP).
	 *	4	SAWTOOTH	Sawtooth output (amplitude SGNA, offset SGNO, frequency SGNF, phase SGNP, rising for fraction SGNP, falling for fraction 1-SGNP).
	 *
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setSignalType(asynUser* pasynUser, epicsInt32 value)
    {
    	_SignalType = value;
    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getSignalType(asynUser* pasynUser, epicsInt32 *value)
    {
    	*value = _SignalType;
    	return ndsSuccess;
    }

    /** \brief Frequency of those types of signal that are periodic. [PV record setter]
     *
	 *  Asyn Reason: SignalFrequency\n
	 *  PV Record suffix: SGNF\n
	 *
	 *	See SGNT record for specification of the signal type.
	 *	Frequency is specified in Hz as a floating point number.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setSignalFrequency(asynUser* pasynUser, epicsFloat64 value)
    {
    	_SignalFrequency = value;
    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getSignalFrequency(asynUser* pasynUser, epicsFloat64 *value)
    {
    	*value = _SignalFrequency;
    	return ndsSuccess;
    }

    /** \brief Amplitude of the output signal. [PV record setter]
     *
	 *  Asyn Reason: SignalAmplitude\n
	 *  PV Record suffix: SGNA\n
	 *
	 *	E.g., the sine signal without offset would range from –SGNA to +SGNA.
	 *	Amplitude is specified in terms of the output’s engineering units.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setSignalAmplitude(asynUser* pasynUser, epicsFloat64 value)
    {
    	_SignalAmplitude = value;
    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getSignalAmplitude(asynUser* pasynUser, epicsFloat64 *value)
    {
    	*value = _SignalAmplitude;
    	return ndsSuccess;
    }

    /** \brief Offset to add to the output signal. [PV record setter]
     *
	 *  Asyn Reason: \n
	 *  PV Record suffix: SGNO\n
	 *
	 *  E.g., the sine signal would range from SGNO-SGNA to SGNO+SGNA.
	 *  Offset is specified in terms of the output’s engineering units.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setSignalOffset(asynUser* pasynUser, epicsFloat64 value)
    {
    	_SignalOffset = value;
    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getSignalOffset(asynUser* pasynUser, epicsFloat64 *value)
    {
    	*value = _SignalOffset;
    	return ndsSuccess;
    }

    /** \brief Phase of the generated output signal. [PV record setter]
     *
	 *  Asyn Reason: SignalPhase\n
	 *  PV Record suffix: SGNP \n
	 *
	 *  Phase is relative to the epoch of the time base (e.g., the TCN).
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setSignalPhase(asynUser* pasynUser, epicsFloat64 value)
    {
    	_SignalPhase = value;
    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getSignalPhase(asynUser* pasynUser, epicsFloat64 *value)
    {
    	*value = _SignalPhase;
    	return ndsSuccess;
    }

    /** \brief Set the duty cycle of the output signal. [PV record setter]
     *
	 *  Asyn Reason: SignalDutyCycle\n
	 *  PV Record suffix: SGND\n
	 *
	 *  Duty cycle is expressed as a fraction of the whole cycle when the output is in the high state (pulse train) or rising (sawtooth).
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setSignalDutyCycle(asynUser* pasynUser, epicsFloat64 value)
    {
    	_SignalDutyCycle = value;
    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getSignalDutyCycle(asynUser* pasynUser, epicsFloat64 *value)
    {
    	*value = _SignalDutyCycle;
    	return ndsSuccess;
    }

    // FFT functions

    /** \brief Configure size of the frame for the Fourier transform. [PV record setter]
     *
	 *  Asyn Reason: FFTSize\n
	 *  PV Record suffix: FFTN\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setFFTSize(asynUser* pasynUser, epicsFloat64 value)
    {
    	_FFTSize = value;
    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getFFTSize(asynUser* pasynUser, epicsFloat64 *value)
    {
    	*value = _FFTSize;
    	return ndsSuccess;
    }

    /** \brief Configure the amount of overlap between consecutive frames for the Fourier transform. [PV record setter]
     *
	 *  Asyn Reason:\n
	 *  PV Record suffix: FFTO \n
	 *
	 *  This function will be called when EPICS record's is read.
     */
    virtual ndsStatus setFFTOverlap(asynUser* pasynUser, epicsFloat64 value)
    {
    	_FFTOverlap = value;
    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: \n
	 *  PV Record suffix: \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getFFTOverlap(asynUser* pasynUser, epicsFloat64 *value)
    {
    	*value = _FFTOverlap;
    	return ndsSuccess;
    }

    /** \brief Configure the smoothing factor for averaging the Fourier transform. [PV record setter]
     *
	 *  Asyn Reason: FFTSmoothing\n
	 *  PV Record suffix: FFTS\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setFFTSmoothing(asynUser* pasynUser, epicsFloat64 value)
    {
    	_FFTSmoothing = value;
    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getFFTSmoothing(asynUser* pasynUser, epicsFloat64 *value)
    {
    	*value = _FFTSmoothing;
    	return ndsSuccess;
    }

    /** \brief The Fourier transform of the acquired signal. [PV record setter]
     *
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *
	 *  This function will be called when EPICS record's value is set.
     */
    virtual ndsStatus setFFTBuffer(asynUser* pasynUser, T *inArray,  size_t nelem)
    {
    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: FFTBuffer\n
	 *  PV Record suffix: FFTB\n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getFFTBuffer(asynUser* pasynUser, T *outArray, size_t nelem,  size_t *nIn)
    {
    	return ndsSuccess;
    }

    /** \brief  [PV record setter]
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setFFT(asynUser* pasynUser, epicsInt32 value)
    {
    	return ndsSuccess;
    }

    /** \brief Select the windowing function for the Fourier transform. [PV record setter]
     *
	 *  Asyn Reason: \n
	 *  PV Record suffix: FFTW\n
	 *
	 *  Possible values are:
	 *  0	NONE	No window (i.e., window[i] = 1).
	 *  1	BARLETT
	 *  2	BLACKMAN
	 *  3	FLATTOP
	 *  4	HANN
	 *  5	HAMMING
	 *  6	TUKEY
	 *  7	WELCH
	 *  	 *
	 *  This function will be called when EPICS record's value is set.
     */
    virtual ndsStatus setFFTWindow(asynUser* pasynUser, epicsInt32 value)
    {
    	_FFTWindow = value;
    	return ndsSuccess;
    }

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: \n
	 *  PV Record suffix: \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getFFTWindow(asynUser* pasynUser, epicsInt32 *value)
    {
    	*value = _FFTWindow;
    	return ndsSuccess;
    }

protected:
    /// @cond NDS_INTERNALS
    ChannelTemplate(const Channel&);
    ChannelTemplate& operator= (const Channel&);
    ndsStatus onEpicsInitialized(ChannelStates, ChannelStates);
    /// @endcond

	Device* _device;

    /// Direction of the channel: 0 - input, output otherwise.
    int _isOutput;

	/// Interrupt ID to dispatch Value change event
	int _interruptIdValue;

	/// Interrupt ID to dispatch DecimationFactor event
	int _interruptIdDecimationFactor;
	/// Interrupt ID to dispatch DecimationOffset event
	int _interruptIdDecimationOffset;
	/// Interrupt ID to dispatch SignalType event
	int _interruptIdSignalType;
	/// Interrupt ID to dispatch FFTWindow event
	int _interruptIdFFTWindow;
	/// Interrupt ID to dispatch SignalFrequency event
	int _interruptIdSignalFrequency;
	/// Interrupt ID to dispatch SignalAmplitude event
	int _interruptIdSignalAmplitude;
	/// Interrupt ID to dispatch SignalOffset event
	int _interruptIdSignalOffset;
	/// Interrupt ID to dispatch SignalPhase event
	int _interruptIdSignalPhase;
	/// Interrupt ID to dispatch SignalDutyCycle event
	int _interruptIdSignalDutyCycle;
	/// Interrupt ID to dispatch FFTSize event
	int _interruptIdFFTSize;
	/// Interrupt ID to dispatch FFTOverlap event
	int _interruptIdFFTOverlap;
	/// Interrupt ID to dispatch FFTSmoothing event
	int _interruptIdFFTSmoothing;

	/// Internal variable to store ValueInt32
	ValueType _Value;

	/// Internal variable to store DecimationFactor
	epicsInt32 _DecimationFactor;
	/// Internal variable to store DecimationOffset
	epicsInt32 _DecimationOffset;
	/// Internal variable to store SignalType
	epicsInt32 _SignalType;
	/// Internal variable to store FFTWindow
	epicsInt32 _FFTWindow;

	/// Internal variable to store SignalFrequency
	epicsFloat64 _SignalFrequency;
	/// Internal variable to store SignalAmplitude
	epicsFloat64 _SignalAmplitude;
	/// Internal variable to store SignalOffset
	epicsFloat64 _SignalOffset;
	/// Internal variable to store SignalPhase
	epicsFloat64 _SignalPhase;
	/// Internal variable to store SignalDutyCycle
	epicsFloat64 _SignalDutyCycle;
	/// Internal variable to store FFTSize
	epicsFloat64 _FFTSize;
	/// Internal variable to store FFTOverlap
	epicsFloat64 _FFTOverlap;
	/// Internal variable to store FFTSmoothing
	epicsFloat64 _FFTSmoothing;

	/// Interrupt ID to dispatch BufferInt32 event
	int _interruptIdBuffer;
	/// Interrupt ID to dispatch Conversion event
	int _interruptIdConversion;
	/// Interrupt ID to dispatch Filter event
	int _interruptIdFilter;
	/// Interrupt ID to dispatch FFTBuffer event
	int _interruptIdFFTBuffer;

	/// epicsInt32 buffer
	T* _Buffer;
	/// buffer for Conversion
	T* _Conversion;
	/// buffer for Filter
	T* _Filter;
	/// buffer for FFTBuffer
	T* _FFTBuffer;

	/// Size of Int32 buffer
    size_t _BufferSize;
	/// Size of Conversion buffer
    size_t _ConversionSize;
	/// Size of Filter buffer
    size_t _FilterSize;
	/// Size of FFT buffer
    size_t _FFTBufferSize;

};

}

#endif /* NDSCHANNEL_H_ */
