/*
 * ndsBuffer.h
 *
 *  Created on: 8. mar. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSBUFFER_H_
#define NDSBUFFER_H_


namespace nds
{

/*** Buffer template for template channel class
 *
 */
template<typename T>
class Buffer
{
	T* buffer;
public:
	void bufferProcessed(Channel* )
	{
		Channel->bufferProcessed(T*, Type2Type<T>);
	}

	ndsStatus registerBufferHandler(PVContainers* pvContainers)
	{
		return registerBufferHandler(pvContainers, Loki::Type2Type<T>());
	}

	ndsStatus registerBufferHandler(PVContainers* pvContainers, Loki::Type2Type<epicsInt8>)
	{
		NDS_PV_REGISTER_INT8ARRAY("Buffer",     &ChannelTemplate<T>::setBuffer,     &ChannelTemplate<T>::getBuffer,     &_interruptIdBuffer);
		NDS_PV_REGISTER_INT8ARRAY("Conversion", &ChannelTemplate<T>::setConversion, &ChannelTemplate<T>::getConversion, &_interruptIdConversion);
		NDS_PV_REGISTER_INT8ARRAY("Filter",     &ChannelTemplate<T>::setFilter,     &ChannelTemplate<T>::getFilter,     &_interruptIdFilter);
		NDS_PV_REGISTER_INT8ARRAY("FFTBuffer",  &ChannelTemplate<T>::setFFTBuffer,  &ChannelTemplate<T>::getFFTBuffer,  &_interruptIdFFTBuffer);

		NDS_PV_REGISTER_INT32("Value",  &ChannelTemplate<T>::setValue,  &ChannelTemplate<T>::getValue,  &_interruptIdValue);

		return ndsSuccess;
	}

	    ndsStatus registerBufferHandler(PVContainers* pvContainers, Loki::Type2Type<epicsInt16>)
	    {
	    	NDS_PV_REGISTER_INT16ARRAY("Buffer",     &ChannelTemplate<T>::setBuffer,  	 &ChannelTemplate<T>::getBuffer,     &_interruptIdBuffer);
	    	NDS_PV_REGISTER_INT16ARRAY("Conversion", &ChannelTemplate<T>::setConversion, &ChannelTemplate<T>::getConversion, &_interruptIdConversion);
	    	NDS_PV_REGISTER_INT16ARRAY("Filter",     &ChannelTemplate<T>::setFilter,     &ChannelTemplate<T>::getFilter,     &_interruptIdFilter);
	    	NDS_PV_REGISTER_INT16ARRAY("FFTBuffer",  &ChannelTemplate<T>::setFFTBuffer,  &ChannelTemplate<T>::getFFTBuffer,  &_interruptIdFFTBuffer);

	    	return ndsSuccess;
	    }

	    ndsStatus registerBufferHandler(PVContainers* pvContainers, Loki::Type2Type<epicsInt32>)
	    {
	    	NDS_PV_REGISTER_INT32ARRAY("Buffer",     &ChannelTemplate<T>::setBuffer,     &ChannelTemplate<T>::getBuffer,     &_interruptIdBuffer);
	    	NDS_PV_REGISTER_INT32ARRAY("Conversion", &ChannelTemplate<T>::setConversion, &ChannelTemplate<T>::getConversion, &_interruptIdConversion);
	    	NDS_PV_REGISTER_INT32ARRAY("Filter",     &ChannelTemplate<T>::setFilter,     &ChannelTemplate<T>::getFilter,     &_interruptIdFilter);
	    	NDS_PV_REGISTER_INT32ARRAY("FFTBuffer",  &ChannelTemplate<T>::setFFTBuffer,  &ChannelTemplate<T>::getFFTBuffer,  &_interruptIdFFTBuffer);

	    	return ndsSuccess;
	    }

	    ndsStatus registerBufferHandler(PVContainers* pvContainers, Loki::Type2Type<epicsFloat32>)
	    {
	    	NDS_PV_REGISTER_FLOAT32ARRAY("Buffer",     &ChannelTemplate<T>::setBuffer, &ChannelTemplate<T>::getBuffer, &_interruptIdBuffer);
	    	NDS_PV_REGISTER_FLOAT32ARRAY("Conversion", &ChannelTemplate<T>::setConversion, &ChannelTemplate<T>::getConversion, &_interruptIdConversion);
	    	NDS_PV_REGISTER_FLOAT32ARRAY("Filter",     &ChannelTemplate<T>::setFilter, &ChannelTemplate<T>::getFilter, &_interruptIdFilter);
	    	NDS_PV_REGISTER_FLOAT32ARRAY("FFTBuffer",  &ChannelTemplate<T>::setFFTBuffer, &ChannelTemplate<T>::getFFTBuffer, &_interruptIdFFTBuffer);

	    	return ndsSuccess;
	    }

	    ndsStatus registerBufferHandler(PVContainers* pvContainers, Loki::Type2Type<epicsFloat64>)
	    {
	    	NDS_PV_REGISTER_FLOAT64ARRAY("Buffer",     &ChannelTemplate<T>::setBuffer, &ChannelTemplate<T>::getBuffer, &_interruptIdBuffer);
	    	NDS_PV_REGISTER_FLOAT64ARRAY("Conversion", &ChannelTemplate<T>::setConversion, &ChannelTemplate<T>::getConversion, &_interruptIdConversion);
	    	NDS_PV_REGISTER_FLOAT64ARRAY("Filter",     &ChannelTemplate<T>::setFilter, &ChannelTemplate<T>::getFilter, &_interruptIdFilter);
	    	NDS_PV_REGISTER_FLOAT64ARRAY("FFTBuffer",  &ChannelTemplate<T>::setFFTBuffer, &ChannelTemplate<T>::getFFTBuffer, &_interruptIdFFTBuffer);
	    	return ndsSuccess;
	    }
}

}

#endif /* NDSBUFFER_H_ */
