/*
 * ndsDriverCommand.cpp
 *
 *  Created on: 17. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <algorithm>

#include "ndsDebug.h"
#include "ndsDriverCommand.h"

namespace nds
{

const std::string whiteSpaces( " \f\n\r\t\v" );

void trimRight( std::string& str,
      const std::string& trimChars = whiteSpaces )
{
   std::string::size_type pos = str.find_last_not_of( trimChars );
   str.erase( pos + 1 );
}


void trimLeft( std::string& str,
      const std::string& trimChars = whiteSpaces )
{
   std::string::size_type pos = str.find_first_not_of( trimChars );
   str.erase( 0, pos );
}


std::string trim(const std::string& str, const std::string& trimChars = whiteSpaces )
{
   std::string result(str);
   trimRight( result, trimChars );
   trimLeft( result, trimChars );
   return result;
}

DriverCommand::DriverCommand(std::string command):
    _reason(command)
	,_command(command)
{
	size_t idxStart = command.find("(");
	size_t idxEnd   = command.find(")");

	if ( idxStart != std::string::npos
			&& idxEnd != std::string::npos
			&& idxStart < idxEnd
			&& idxStart > 0 )
	{
		_reason = command.substr(0, idxStart);
		NDS_TRC( "'%s' '%s' %lu %lu %lu", command.c_str(), _reason.c_str(), idxStart, idxEnd, idxEnd-idxStart );
		if ( idxEnd-idxStart > 1)
		{
			std::string arguments = command.substr(idxStart+1, idxEnd-idxStart-1 );
			NDS_TRC( "args(%s)", arguments.c_str() );
			idxStart=0;
			size_t length = arguments.length();
			size_t found  = arguments.find_first_of(",");
			NDS_TRC( "%lu %lu", length, found );

			while ( found != std::string::npos )
			{
				idxEnd = found+1;
				length = found - idxStart;

				NDS_TRC( "%lu %lu", idxStart, length );
				Args.push_back( trim(arguments.substr(idxStart, length)) );

				found  = arguments.find_first_of(",",found+1);
				idxStart = idxEnd;
			}

			length = arguments.length() - idxStart;
			Args.push_back( trim(arguments.substr(idxStart, length)) );
		}
	}
}

DriverCommand::~DriverCommand()
{
}

} /* namespace nds */
