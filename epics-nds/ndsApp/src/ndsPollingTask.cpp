/*
 * ndsPollingThread.cpp
 *
 *  Created on: 23.08.2013
 *      Author: Slava
 */

#include <boost/bind.hpp>
#include <sys/eventfd.h>

#include "ndsDebug.h"
#include "ndsTaskManager.h"
#include "ndsPollingTask.h"
#include "map_functors.h"

namespace nds
{

PollingTask* PollingTask::create(
        const std::string& name,
        unsigned int priority,
        unsigned int stackSize)
{
    PollingTask* task = new PollingTask(name, priority, stackSize);
    TaskManager::getInstance().registerTask(name, task);
    return task;
}

std::string getEpollEventStr (uint32_t event)
{
    switch(event)
    {
    case EPOLLIN:
        return "EPOLLIN";
    case EPOLLOUT:
        return "EPOLLOUT";
    case EPOLLRDHUP:
        return "EPOLLRDHUP";
    case EPOLLPRI:
        return "EPOLLPRI";
    case EPOLLERR:
        return "EPOLLERR";
    case EPOLLHUP:
        return "EPOLLHUP";
    case EPOLLET:
        return "EPOLLET";
    case EPOLLONESHOT:
        return "EPOLLONESHOT";
    }
    return "Unknown";
}



PollingTask::PollingTask(
        const std::string& name,
        unsigned int priority,
        unsigned int stackSize):
        Thread( name, priority, stackSize, new TaskService() ),
        _fdCancelEvent(eventfd(0,0)),
        _fdCancelAckEvent(eventfd(EFD_SEMAPHORE, 0)),
        _epollFD(0)
{
    init();
}

PollingTask::~PollingTask()
{

}

void PollingTask::init()
{
    memset(&_eventCancel, 0, sizeof(struct epoll_event));
    _eventCancel.events = EPOLLIN;
    _eventCancel.data.fd = _fdCancelEvent;
}

ndsStatus PollingTask::addFile(int fileFd, FileEventHandler handler, uint32_t events)
{
    if ( fileFd==-1 )
    {
        NDS_ERR("File descriptor is not valid.");
        return ndsError;
    }

    FileDescriptor* fd = new FileDescriptor(*_service, fileFd, events, handler);
    _events.insert(
            FDContainer::value_type(fileFd, fd)
    );
    return ndsSuccess;
}

void PollingTask::createPollFD()
{
    if (_epollFD)
        close(_epollFD);

    _epollFD = epoll_create(_events.size()+1);

    // adding break event
    epoll_ctl(_epollFD, EPOLL_CTL_ADD, _fdCancelEvent, &_eventCancel );

    for ( FDContainer::iterator itr=_events.begin();
            itr != _events.end();
            ++itr )
    {
        epoll_ctl(_epollFD, EPOLL_CTL_ADD, itr->second->getFD(), itr->second->getEpollEvent() );
    }
}

void PollingTask::run(TaskServiceBase& service)
{
    NDS_STK("PollingTask[%s]::run", getName().c_str());
    uint64_t one;

    struct epoll_event *epollEvents = (struct epoll_event *)malloc( sizeof(struct epoll_event) * _events.size()+1 );
    createPollFD();

    do
    {
        int nfds = epoll_wait(_epollFD, epollEvents, sizeof(epollEvents), -1);
        NDS_TRC("[%s] Event received", getName().c_str());
        for(int i = 0; i < nfds; ++i)
        {
            NDS_TRC("PollingTask[%s]::run i: %d event[%d]: %s"
                    , getName().c_str()
                    , i
                    , epollEvents[i].events
                    , getEpollEventStr(epollEvents[i].events).c_str() );

            if ( epollEvents[i].data.fd == _fdCancelEvent )
            {
               NDS_DBG("PollingTask[%s]::run cancel received.", getName().c_str());
               read( _fdCancelEvent, (void*)&one, sizeof(uint64_t) );
               break;
            }else
            {
            	try
            	{
                    NDS_TRC("PollingTask[%s]::run Invoking handler", getName().c_str());
            		_events[ epollEvents[i].data.fd ]->notify( epollEvents[i] );
            	}catch( ... )
            	{
            		NDS_ERR("Polling handler throws exception. ");
            	}
            }
        }
    }while( !service.isCancelled() );

    one = 1;
    write( _fdCancelAckEvent, &one, sizeof(one));
}

ndsStatus PollingTask::cancelPrv()
{
    NDS_TRC("PollingTask[%s]::cancelPrv", getName().c_str() );
    uint64_t one = 1;

    _service->cancel();
    write( _fdCancelEvent,    &one, sizeof(one) );
    read ( _fdCancelAckEvent, &one, sizeof(one) );

    std::for_each(_events.begin(),
            _events.end(),
            &deleteMapObj<FDContainer::value_type>);
    _events.clear();

    _state = ndsThreadStopped;
    return ndsSuccess;
}

} /* namespace nds */
