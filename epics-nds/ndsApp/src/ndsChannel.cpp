/*
 * ndsChannel.cpp
 *
 *  Created on: 18. jul. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <boost/assign.hpp>

#include "ndsDebug.h"
#include "ndsChannel.h"
#include "ndsPV.h"
#include "ndsPVContainer.h"
#include "ndsAsynMacro.h"

using namespace nds;

#define NDS_CHANNEL_CLASS Channel

Channel::Channel()
{

}

long long Channel::clockPeriod()
{
	return (0 == _ClockFrequency)? 0 : 1.0e9/_ClockFrequency;
}


ndsStatus Channel::onEpicsInitialized(ChannelStates, ChannelStates)
{
	return ndsSuccess;
}


ndsStatus Channel::registerHandlers(PVContainers* pvContainers)
{
	BaseChannel::registerHandlers(pvContainers);

#define NDS_INSTANTIATE NDS_REG_SIMPL_FUNCTIONS
#define NDS_INSTANTIATE_OCTET NDS_REG_OCTET_FUNCTIONS
#define NDS_INSTANTIATE_ARRAY NDS_REG_ARRAY_FUNCTIONS
    NDS_INSTANTIATE(Int32,   ValueInt32)
    NDS_INSTANTIATE(Float64, ValueFloat64)
    NDS_INSTANTIATE_ARRAY(Int32,   BufferInt32)
    NDS_INSTANTIATE_ARRAY(Int16,   BufferInt16)
    NDS_INSTANTIATE_ARRAY(Int8,    BufferInt8)
    NDS_INSTANTIATE_ARRAY(Float32, BufferFloat32)
    NDS_INSTANTIATE_ARRAY(Float64, BufferFloat64)
    NDS_INSTANTIATE_ARRAY(Float64, Conversion)
    NDS_INSTANTIATE_ARRAY(Float64, Filter)
    NDS_INSTANTIATE_ARRAY(Float64, FFTBuffer)
	NDS_INSTANTIATE_FOR_EACH_CHANNEL_FUNCTION
#undef NDS_INSTANTIATE
#undef NDS_INSTANTIATE_OCTET
#undef NDS_INSTANTIATE_ARRAY

	NDS_PV_REGISTER_INT32("FFT", &Channel::setFFT, &Channel::getFFT, &_interruptIdInt32);

    NDS_PV_REGISTER_OCTET("StreamingURL", &Channel::setStreamingURL, &Channel::getStreamingURL, &_interruptIdStreamingURL);

	return ndsSuccess;
}

ndsStatus Channel::setFFT(asynUser* pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus Channel::getFFT(asynUser* pasynUser, epicsInt32 *value)
{
    return ndsSuccess;
}

ndsStatus Channel::inputChanged(epicsFloat64 value)
{
	return doCallbacksFloat64(value, _interruptIdValueFloat64, _portAddr);
}

ndsStatus Channel::inputChanged(epicsInt32   value)
{
	return doCallbacksInt32(value, _interruptIdValueInt32, _portAddr);
}

ndsStatus Channel::bufferInt8Processed()
{
	return doCallbacksInt8Array(_BufferInt8, _BufferInt8Size, _interruptIdBufferInt8, _portAddr);
}

ndsStatus Channel::bufferInt16Processed()
{
	return doCallbacksInt16Array(_BufferInt16, _BufferInt16Size, _interruptIdBufferInt16, _portAddr);
}

ndsStatus Channel::bufferInt32Processed()
{
	return doCallbacksInt32Array(_BufferInt32, _BufferInt32Size, _interruptIdBufferInt32, _portAddr);
}

ndsStatus Channel::bufferFloat32Processed()
{
	return doCallbacksFloat32Array(_BufferFloat32, _BufferFloat32Size, _interruptIdBufferFloat32, _portAddr);
}

ndsStatus Channel::bufferFloat64Processed()
{
	return doCallbacksFloat64Array(_BufferFloat64, _BufferFloat64Size, _interruptIdBufferFloat64, _portAddr);
}

ndsStatus Channel::bufferProcessed()
{
	return bufferFloat64Processed();
}

ndsStatus Channel::setStreamingURL(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered)
{
    _StreamingURL = data;
    return ndsSuccess;
}

ndsStatus Channel::getStreamingURL(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return ndsSuccess;
}

void Channel::destroyPrivate()
{
    destroy();
}

void Channel::onRegistered()
{
}

ndsStatus Channel::propagateRegisterHandlers(PVContainers* pvContainers)
{
    return this->registerHandlers(pvContainers);
}

epicsInt32 Channel::getChannelNumber()
{
    return _portAddr;
}

nds::ChannelGroup*  Channel::getChannelGroup()
{
    return _group;
}

void Channel::printStructure(const std::string& prefix)
{
    PRINT("%sChannel: %d State: %s\n", prefix.c_str(), _portAddr, ChannelBaseState::toString(this->getCurrentState()).c_str() );
}

#define NDS_INSTANTIATE NDS_IMPLEMENT_FUNCTIONS
#define NDS_INSTANTIATE_OCTET NDS_IMPLEMENT_OCTET_FUNCTIONS
#define NDS_INSTANTIATE_ARRAY NDS_IMPLEMENT_ARRAY_FUNCTIONS
	NDS_INSTANTIATE(Int32,   ValueInt32)
	NDS_INSTANTIATE(Float64, ValueFloat64)
	NDS_INSTANTIATE_ARRAY(Int32,   BufferInt32)
	NDS_INSTANTIATE_ARRAY(Int16,   BufferInt16)
	NDS_INSTANTIATE_ARRAY(Int8,    BufferInt8)
	NDS_INSTANTIATE_ARRAY(Float32, BufferFloat32)
	NDS_INSTANTIATE_ARRAY(Float64, BufferFloat64)
	NDS_INSTANTIATE_ARRAY(Float64, Conversion)
	NDS_INSTANTIATE_ARRAY(Float64, Filter)
	NDS_INSTANTIATE_ARRAY(Float64, FFTBuffer)
	NDS_INSTANTIATE_FOR_EACH_CHANNEL_FUNCTION
#undef NDS_INSTANTIATE
#undef NDS_INSTANTIATE_OCTET
#undef NDS_INSTANTIATE_ARRAY

