/*
 * ndsMessaging.hpp
 *
 *  Created on: 28. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSMESSAGING_HPP_
#define NDSMESSAGING_HPP_

#include <boost/bind.hpp>
#include <string>
#include <sstream>
#include <map>

#include "ndsDebug.h"
#include "ndsTypes.h"


namespace nds
{

typedef std::map<std::string, std::string> MessageParameters;

/**
 *	Message container.
 *
 *	Message container is used to represent messages in easy to handle way.
 *	It provides encode/decode functionality for strings.
 *
 *	Interface is represented by std::string message type and
 *	a map of parameters (std::string, std::string).
 *
 *  To simplify adding parameter to message template static insert function could be used.
 *  Any type which has stream operator implemented could be used with this function.
 *
 */
class Message
{
	/// @cond NDS_INTERNALS
	Message(const Message& msg);
	Message& operator =(const Message& msg);
	/// @endcond

public:
	/// Message type
	std::string messageType;

	/** Message parameters
	 *
	 *  Message parameters are accessible as a public member to
	 *  get user flexibility of adding, changing them in free manner.
	 */
	MessageParameters parameters;

	/// Ctor
	Message(){}

	/// Load message from string
	ndsStatus load(const std::string& msg);

	/// Load message from buffer
	ndsStatus load(const char *data, size_t numchars, size_t *nbytesTransfered);

	/// Return message as a string
	const std::string str() const;

	/** Insert message parameter
	 *
	 *	Insert parameter and it's value to parameter list.
	 *
	 * @param name - parameter name
	 * @param value - parameter value
	 */
    void insert(const std::string& name, const std::string& value);

    /** Get parameter value as string
     *
     *	It returns value of requested parameter.
     *	In case parameter is absent default value will be returned.
     *
     * @ param parameter - parameter name
     * @ param def - default value which will be returned in case parameter is not set
     */
	const std::string& getStrParam(const std::string& param, const std::string& def) const;

    /** Get parameter value as integer
     *
     *	It returns value of requested parameter converted to integer.
     *	In case parameter is absent default value will be returned.
     *
     * @ param parameter - parameter name
     * @ param def - default value which will be returned in case parameter is not set
     */
	int getIntParam(const std::string& parameter, int def) const;

	/** Inserts message parameter and its value
	 *
	 *  Any types which has stream operator implemented could be used with this function.
	 *
	 * @param message - message where parameter will be inserted.
	 * @param name - parameter name. It will be converted to string.
	 * @param value - parameter value. It will be converted to string.
	 */
	template<typename V, typename T>
	friend void insert( Message& message, const V& name, const T& value);
};

template<typename V, typename T>
void insert( Message& message, const V& name, const T& value)
{
  std::stringstream ssName (std::stringstream::in | std::stringstream::out);
  std::stringstream ssValue (std::stringstream::in | std::stringstream::out);
  ssName  << name;
  ssValue << value;
  message.insert( ssName.str(), ssValue.str() );
}


}


#endif /* NDSMESSAGING_HPP_ */
