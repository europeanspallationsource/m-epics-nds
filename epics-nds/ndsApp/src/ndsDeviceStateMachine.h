/*
 * ndsDeviceStateMachine.h
 *
 *  Created on: 21. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSDEVICESTATEMACHINE_H_
#define NDSDEVICESTATEMACHINE_H_

#include "ndsDebug.h"
#include "ndsTypes.h"
#include "ndsDeviceStates.h"
#include "ndsAbstractStateMachine.h"

namespace nds
{

typedef AbstractStateMachine<DeviceStates> DeviceStateMachine;

/**
 * Base class for device's states
 */
class DeviceBaseState : public AbstractState<DeviceStates>
{
public:
	static const std::string toString(const DeviceStates& state);

	DeviceBaseState()
	{
	}

	/**
	 *	Request ERROR state
	 */
	virtual ndsStatus error(DeviceStateMachine* obj)
	{
		setMachineState(obj, DEVICE_STATE_ERROR);
		return ndsSuccess;
	}

	virtual ndsStatus on(DeviceStateMachine* obj)
	{
		NDS_WRN("%s: Unexpected state requested: %s\n",
				toString(getState()).c_str(),
				"On");
		return ndsError;
	}

	virtual ndsStatus off(DeviceStateMachine* obj)
	{
		NDS_WRN("%s: Unexpected state requested: %s\n",
				toString(getState()).c_str(),
								"Off");
		return ndsError;
	}

	virtual ndsStatus IOCinit(DeviceStateMachine* obj)
	{
		NDS_WRN("%s: Unexpected state requested: %s\n",
				toString(getState()).c_str(),
								"IOCInit");
		return ndsError;
	}

	virtual ndsStatus defunct(DeviceStateMachine* obj) __attribute__ ((deprecated))
	{
        setMachineState(obj, DEVICE_STATE_DEFUNCT);
		return ndsError;
	}

    virtual ndsStatus fault(DeviceStateMachine* obj)
    {
        setMachineState(obj, DEVICE_STATE_FAULT);
        return ndsError;
    }

    virtual ndsStatus reset(DeviceStateMachine* obj)
    {
        NDS_WRN("%s: Unexpected state requested: %s\n",
                toString(getState()).c_str(),
                                "reset");
        return ndsError;
    }

};

/**
 * Device's state Unknown
 */
class DeviceStateUnknown: public DeviceBaseState
{
public:
	DeviceStates getState(){ return DEVICE_STATE_UNKNOWN; }
	ndsStatus IOCinit(DeviceStateMachine* obj)
	{
		return setMachineState(obj, DEVICE_STATE_IOC_INIT);
	}
};

/**
 * Device's state IOC initialization
 */
class DeviceStateIOCInit: public DeviceBaseState
{
public:
	virtual DeviceStates getState(){ return DEVICE_STATE_IOC_INIT; }
	virtual ndsStatus off(DeviceStateMachine* obj)
	{
		return setMachineState(obj, DEVICE_STATE_OFF);
	}
};

/**
 * Device's state initialization
 */
class DeviceStateInit: public DeviceBaseState
{
public:
	virtual DeviceStates getState(){ return DEVICE_STATE_INIT; }

	virtual ndsStatus on(DeviceStateMachine* obj)
	{
		return requestState(obj, DEVICE_STATE_ON);
	}
};


/**
 * Device's state OFF
 */
class DeviceStateOff: public DeviceBaseState
{
public:
	virtual DeviceStates getState(){ return DEVICE_STATE_OFF; }
	virtual ndsStatus init(DeviceStateMachine* obj)
	{
		return requestState(obj, DEVICE_STATE_INIT);
	}

	virtual ndsStatus on(DeviceStateMachine* obj)
	{
		return requestState(obj, DEVICE_STATE_INIT);
	}
};

/**
 * Device's state ON
 */
class DeviceStateOn: public DeviceBaseState
{
public:
	virtual DeviceStates getState(){ return DEVICE_STATE_ON; }
	virtual ndsStatus off(DeviceStateMachine* obj)
	{
		return requestState(obj, DEVICE_STATE_OFF);
	}

    virtual ndsStatus reset(DeviceStateMachine* obj)
    {
        return setMachineState(obj, DEVICE_STATE_RESETTING);
    }
};

/**
 * Device's state ERROR
 */
class DeviceStateError: public DeviceBaseState
{
public:

	/**
	 * 	Returns state id.
	 */
	virtual DeviceStates getState(){ return DEVICE_STATE_ERROR; }

    virtual ndsStatus reset(DeviceStateMachine* obj)
    {
        return setMachineState(obj, DEVICE_STATE_RESETTING);
    }

    virtual ndsStatus off(DeviceStateMachine* obj)
    {
        setMachineState(obj, DEVICE_STATE_OFF);
        return ndsSuccess;
    }
};

/**
 * Device's state FAULT
 */
class DeviceStateFault: public DeviceBaseState
{
public:
	virtual DeviceStates getState(){ return DEVICE_STATE_FAULT; }
};
typedef DeviceStateFault DeviceStateDefunct;

/**
 * Device's state RESET
 */
class DeviceStateReset: public DeviceBaseState
{
public:
    virtual DeviceStates getState(){ return DEVICE_STATE_RESETTING; }
};


}

#endif /* NDSDEVICESTATEMACHINE_H_ */
