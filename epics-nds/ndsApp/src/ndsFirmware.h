/*
 * ndsFirmware.h
 *
 *  Created on: 11. mar. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSFIRMWARE_H_
#define NDSFIRMWARE_H_

#include <list>
#include <vector>
#include <string>
#include <libxml/parser.h>

#include "ndsTypes.h"

namespace nds
{

/*** Description of the hardware/software to check compatibility.
 *
 *  This class is used by firmware update procedure.
 */
class Target
{
public:
	std::string device; 		///< Device model
	std::string module;			///< Module to upload firmware
	std::string update_method;	///< Firmware's update method

	std::list<std::string> hardware; ///< List of compatible hardware revisions
	std::list<std::string> firmware; ///< List of compatible firmwares. From this versions update possible.

	/*** Parses xml document.
	 *
	 */
	void loadXML(xmlDocPtr doc, xmlNodePtr cur);
};

/*** Firmware description
 *
 */
class Firmware
{
public:
	std::string metaFile;		///< Image URI from XML file.
	std::string image;			///< Image URI from XML file.
	std::string imageAbsPath;	///< Image's absolute path.
	std::string version;		///< Firmware version.
	std::string xmlns;
	std::string sha1;			///< SHA-1 key to check image file.
	bool localFileCreated;
	/***
	 *@param filePath is a path to file to parse
	 */
	ndsStatus loadFile(const std::string& filePath);

	/*** Deserialize instance from xml.
	 *
	 *@param doc is a pointer to instance of xmlDoc
	 *@param cur is a pointer to instance of xmlnode
	 */
	void loadXML(xmlDocPtr doc, xmlNodePtr cur);

	/*** Checks SHA of the provided image.
	 *
	 */
	ndsStatus checkSHA();

	/*** Downloads image in case provided URI isn't local path
	 *
	 */
	ndsStatus downloadImage();

	~Firmware();
};

}

/*** It downloads file from uri to selected file path.
 *
 *@param uri
 *@param filePath
 */
bool download(const std::string &uri, const std::string &filePath);


#endif /* NDSFIRMWARE_H_ */
