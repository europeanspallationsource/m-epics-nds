/*
 * ndsPV.h
 *
 *  Created on: 18. jul. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSPV_H_
#define NDSPV_H_

#include <boost/function.hpp>
#include <boost/bind.hpp>

#include "ndsTypes.h"
#include "ndsDebug.h"

namespace nds
{

/// @cond NDS_INTERNALS
template <typename T>
class SimpleType
{
public:
	typedef boost::function<ndsStatus (asynUser *pasynUser, T value)>  writer_type;
	typedef boost::function<ndsStatus (asynUser *pasynUser, T* value)> reader_type;

	template <class Owner>
	struct Handlers
	{
		typedef boost::function<ndsStatus (Owner*, asynUser *pasynUser, T value)>  owner_writer_type;
		typedef boost::function<ndsStatus (Owner*, asynUser *pasynUser, T* value)> owner_reader_type;

		static writer_type convertWriter(owner_writer_type writer, Owner* owner)
		{
			return	boost::bind(writer, owner, _1, _2);
		}

		static reader_type convertReader(owner_reader_type reader, Owner* owner)
		{
			return	boost::bind(reader, owner, _1, _2);
		}
	};

	SimpleType(writer_type writer, reader_type reader)
		: _writer(writer), _reader(reader){}

	ndsStatus  write(asynUser *pasynUser, T value)
	{
    	if (_writer)
    	{
   			return _writer(pasynUser, value);
    	}else
    	{
    		NDS_TRC("writer is null\n");
    	}
    	return ndsSuccess;

	}
    ndsStatus  read(asynUser *pasynUser, T* value)
    {
		if (_reader)
		{
			return _reader(pasynUser, value);
		}else
		{
    		NDS_TRC("reader is null\n");
		}
    	return ndsSuccess;
    }
protected:
    writer_type _writer;
    reader_type _reader;
};


template <typename T>
class PointerType
{
public:
	typedef boost::function<ndsStatus (asynUser *pasynUser, T* value)> writer_type;
	typedef boost::function<ndsStatus (asynUser *pasynUser, T* value)> reader_type;

	template <class Owner>
	struct Handlers
	{
		typedef boost::function<ndsStatus (Owner*, asynUser *pasynUser, T* value)> owner_writer_type;
		typedef boost::function<ndsStatus (Owner*, asynUser *pasynUser, T* value)> owner_reader_type;

		static writer_type convertWriter(owner_writer_type writer, Owner* owner)
		{
			return	boost::bind(writer, owner, _1, _2);
		}

		static reader_type convertReader(owner_reader_type reader, Owner* owner)
		{
			return	boost::bind(reader, owner, _1, _2);
		}
	};

	PointerType(writer_type writer, reader_type reader)
		: _writer(writer), _reader(reader){}

	ndsStatus  write(asynUser *pasynUser, T* value)
	{
    	if (_writer)
    	{
   			return _writer(pasynUser, value);
    	}else
    	{
    		NDS_TRC("writer is null\n");
    	}
    	return ndsSuccess;

	}
    ndsStatus  read(asynUser *pasynUser, T* value)
    {
		if (_reader)
		{
			return _reader(pasynUser, value);
		}else
		{
    		NDS_TRC("reader is null\n");
		}
    	return ndsSuccess;
    }
protected:
    writer_type _writer;
    reader_type _reader;
};

template <typename T>
class ArrayType
{
public:
	typedef T type;
	typedef boost::function<ndsStatus (asynUser *pasynUser, T *value, size_t nelements)> writer_type;
	typedef boost::function<ndsStatus (asynUser *pasynUser, T *value, size_t nelements, size_t *nIn)> reader_type;

	template <class Owner>
	struct Handlers
	{
		typedef boost::function<ndsStatus (Owner*, asynUser *pasynUser, T *value, size_t nelements)> owner_writer_type;
		typedef boost::function<ndsStatus (Owner*, asynUser *pasynUser, T *value, size_t nelements, size_t *nIn)> owner_reader_type;

		static writer_type convertWriter(owner_writer_type writer, Owner* owner)
		{
			return	boost::bind(writer, owner, _1, _2, _3);
		}

		static reader_type convertReader(owner_reader_type reader, Owner* owner)
		{
			return	boost::bind(reader, owner, _1, _2, _3, _4);
		}
	};

	ArrayType(writer_type writer, reader_type reader)
		: _writer(writer), _reader(reader){}

	ndsStatus write(asynUser *pasynUser, T *value, size_t nelements) const
    {
    	if (0 != _writer)
    	{
    		return _writer(pasynUser, value, nelements);
		}else
		{
    		NDS_TRC("writer is null\n");
		}
    	return ndsSuccess;
    }
	ndsStatus read(asynUser *pasynUser, T *value, size_t nelements, size_t *nIn) const
    {
		if (0 != _reader)
		{
			return _reader(pasynUser, value, nelements, nIn);
		}else
		{
    		NDS_TRC("reader is null\n");
		}
    	return ndsSuccess;
    }
protected:
    writer_type _writer;
    reader_type _reader;
};

template <typename T>
class DigitalType
{
public:
	typedef T type;
	typedef boost::function<ndsStatus (asynUser *pasynUser, T value, epicsUInt32 mask)> writer_type;
	typedef boost::function<ndsStatus (asynUser *pasynUser, T *value, epicsUInt32 mask)> reader_type;

	template <class Owner>
	struct Handlers
	{
		typedef boost::function<ndsStatus (Owner*, asynUser *pasynUser, T value, epicsUInt32 mask)> owner_writer_type;
		typedef boost::function<ndsStatus (Owner*, asynUser *pasynUser, T *value, epicsUInt32 mask)> owner_reader_type;

		static writer_type convertWriter(owner_writer_type writer, Owner* owner)
		{
			return	boost::bind(writer, owner, _1, _2, _3);
		}

		static reader_type convertReader(owner_reader_type reader, Owner* owner)
		{
			return	boost::bind(reader, owner, _1, _2, _3);
		}

	};

	DigitalType(writer_type writer, reader_type reader)
		: _writer(writer), _reader(reader){}

    ndsStatus write (asynUser *pasynUser, T value, epicsUInt32 mask) const
    {
    	if (0 != _writer)
    		return _writer(pasynUser, value, mask);
    	return ndsSuccess;
    }
    ndsStatus read (asynUser *pasynUser, T *value, epicsUInt32 mask) const
    {
		if (0 != _reader)
			return _reader(pasynUser, value, mask);
    	return ndsSuccess;
    }
protected:
    writer_type _writer;
    reader_type _reader;
};

template <typename T>
class OctetType
{
public:
	typedef T type;
	typedef boost::function<ndsStatus (asynUser*, const char*, size_t, size_t *)> writer_type;
	typedef boost::function<ndsStatus (asynUser*, char *,      size_t, size_t *, int *)> reader_type;

	template <class Owner>
	class Handlers
	{
	public:
		typedef boost::function<ndsStatus (Owner*, asynUser*, const char*, size_t, size_t*)> owner_writer_type;
		typedef boost::function<ndsStatus (Owner*, asynUser*, char *     , size_t, size_t*, int* )> owner_reader_type;

		static writer_type convertWriter(owner_writer_type writer, Owner* owner)
		{
			return	boost::bind(writer, owner, _1, _2, _3, _4);
		}

		static reader_type convertReader(owner_reader_type reader, Owner* owner)
		{
			return	boost::bind(reader, owner, _1, _2, _3, _4, _5);
		}

	};

	OctetType(writer_type writer, reader_type reader)
		: _writer(writer), _reader(reader){}

	ndsStatus write (asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered) const
    {
    	if (0 != _writer)
    		return _writer(pasynUser, data, numchars, nbytesTransfered);
    	return ndsSuccess;
    }
	ndsStatus read  (asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason) const
    {
		if (0 != _reader)
			return _reader(pasynUser, data, maxchars, nbytesTransfered, eomReason);
    	return ndsSuccess;
    }
protected:
    writer_type _writer;
    reader_type _reader;
};

template<class T, template <typename baseT> class Base>
class PV : public Base<T>
{
private:
	std::string _reason;
    std::string _writerName;
    std::string _readerName;
	int* _reasonCodeRef;
	int _reasonCode;
	int _portAddr;
protected:
	void setReasonCode(int reasonCode)
	{
		_reasonCode = reasonCode;
		if (_reasonCodeRef)
			*_reasonCodeRef = reasonCode;
	}
public:
	template<typename F>
	friend class AsynHandlerKeeper;

	PV(int portAddr,
			const std::string& reason,
			typename Base<T>::writer_type writer,
			typename Base<T>::reader_type reader,
            const std::string& writerName,
            const std::string& readerName,
			int* reasonCodeRef)
		:Base<T>(writer, reader)
		 , _reason(reason)
         , _writerName(writerName)
         , _readerName(readerName)
		 , _reasonCodeRef(reasonCodeRef)
		 , _portAddr(portAddr)
	{
	}

	template<class Owner>
	static PV<T, Base>* create(
			int portAddr,
			const std::string& reason,
			Owner* obj,
			typename Base<T>::template Handlers<Owner>::owner_writer_type writer,
			typename Base<T>::template Handlers<Owner>::owner_reader_type reader,
            const std::string& writerName,
            const std::string& readerName,
			int* reasonCodeRef)
	{
		return new PV<T, Base >(
				portAddr,
				reason,
				Base<T>::template Handlers<Owner>::convertWriter(writer, obj),
				Base<T>::template Handlers<Owner>::convertReader(reader, obj),
                writerName,
                readerName,
				reasonCodeRef
				);
	}

	int getReasonCode(){ return _reasonCode; }
	const std::string& getReasonStr(){ return _reason; }
	const std::string& getWriterNameStr(){ return _writerName; }
	const std::string& getReaderNameStr(){ return _readerName; }

	int getPortAddr(){ return _portAddr; }

    const char* getReasonCStr(){ return _reason.c_str(); }
    const char* getWriterNameCStr(){ return _writerName.c_str(); }
    const char* getReaderNameCStr(){ return _readerName.c_str(); }
};

typedef PV<epicsFloat64, SimpleType> PVFloat64;
typedef PV<epicsInt32, SimpleType>   PVInt32;
typedef PV<epicsFloat32, ArrayType>  PVFloat32Array;
typedef PV<epicsFloat64, ArrayType>  PVFloat64Array;
typedef PV<epicsInt8, ArrayType>     PVInt8Array;
typedef PV<epicsInt16, ArrayType>    PVInt16Array;
typedef PV<epicsInt32, ArrayType>    PVInt32Array;
typedef PV<char, OctetType>          PVOctet;
typedef PV<epicsUInt32, DigitalType> PVUInt32Digital;
typedef PV<void, PointerType>        PVGenericPointer;

/// @endcond

}

#endif /* NDSPV_H_ */
