/*
 * ndsThreadManager.h
 *
 *  Created on: 23.08.2013
 *      Author: Slava
 */

#ifndef NDSTHREADMANAGER_H_
#define NDSTHREADMANAGER_H_

#include <map>

#include <epicsTimer.h>

#include "ndsBaseTask.h"
#include "singleton.h"


namespace nds
{

class TaskManagerBase;

typedef singleton<TaskManagerBase> TaskManager;
typedef std::map<std::string, TaskPtr> TasksContainer;

class TaskManagerBase
{
private:
    TasksContainer   _nodes;
    epicsTimerQueueActive &_queue;

    std::string generateNamePrv();
    std::string generateNamePrv(const std::string&);

public:
    TaskManagerBase();
    ~TaskManagerBase();

    bool isTaskExists(const std::string& name);
    ndsStatus findTask(const std::string& name, TaskPtr* ptr);

    void registerTask(const std::string& name, TaskPtr task);
    void deleteTask(const std::string& name);
    void deleteTask(TaskPtr task);

    void stopAll();

    epicsTimerQueue& getQueue();

    void listTasks();
    void startTask(const std::string& str);
    void cancelTask(const std::string& str);

    static std::string generateName();
    static std::string generateName(const std::string&);

};

} /* namespace nds */
#endif /* NDSTHREADMANAGER_H_ */
