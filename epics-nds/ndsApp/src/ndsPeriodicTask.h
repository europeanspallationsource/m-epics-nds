/*
 * ndsPeriodicTask.h
 *
 *  Created on: 08.08.2013
 *      Author: Slava
 */

#ifndef NDSPERIODICTASK_H_
#define NDSPERIODICTASK_H_

#include <epicsThread.h>
#include <epicsEvent.h>

#include "ndsTypes.h"
#include "ndsBaseTask.h"
#include "ndsThread.h"

namespace nds
{

class PeriodicTask : public nds::Thread
{
protected:
    TaskBody _body;
    epicsTime _t0;
    double _period;

    PeriodicTask( const std::string& name,
                  unsigned int     stackSize,
                  unsigned int     priority,
                  TaskServiceBase* service,
                  TaskBody
                );

    virtual ndsStatus start();

public:

    static PeriodicTask* create(const std::string& name,
            unsigned int     stackSize,
            unsigned int     priority,
            TaskBody);

    virtual void run(TaskServiceBase&);
    /**
     *  Task will be started at time t0 for the first time and then restarted every period seconds.
     *
     *  t0 - reference time point.
     */
    virtual ndsStatus start(epicsTime t0, double period);
};


} /* namespace nds */
#endif /* NDSPERIODICTASK_H_ */
