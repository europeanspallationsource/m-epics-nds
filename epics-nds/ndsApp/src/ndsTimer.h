/*
 * ndsTimer.h
 *
 *  Created on: 23. jul. 2013
 *      Author: Slava Isaev
 */

#ifndef NDSTIMER_H_
#define NDSTIMER_H_

#include <map>

#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

#include <epicsTimer.h>

#include "ndsTypes.h"
#include "ndsBaseTask.h"
#include "ndsTaskManager.h"
#include "ndsTaskService.h"
#include "singleton.h"

namespace nds
{

class Timer;

typedef Timer* TimerPtr;

typedef boost::function<epicsTimerNotify::expireStatus (TaskService& service, const epicsTime & currentTime)> OnTimeHandler;

/** Example of timer implementation to generate events
 *
 */
class Timer : public BaseTask
{
public:

    class TimerNotify: public epicsTimerNotify
    {
    public:
        TimerNotify ( TaskService& service, OnTimeHandler p);
        virtual expireStatus expire(const epicsTime & currentTime);
    private:
        TaskService&  _service;
        OnTimeHandler _handler;
    };

    virtual ~Timer();

    virtual ndsStatus start(const epicsTime&);
    virtual ndsStatus start(const double&);

    static inline TimerPtr create(const std::string &name, OnTimeHandler p )
    {
        return Timer::create(name, nds::TaskManager::getInstance().getQueue(), p );
    }

    static inline TimerPtr create(const std::string &name, epicsTimerQueue &queue, OnTimeHandler p )
    {
        TimerPtr tmr = new nds::Timer(name, queue, p );
        nds::TaskManager::getInstance().registerTask(name, (BaseTask*)tmr);
        return tmr;
    }

protected:
    TaskService   _service;
    epicsTimer&   _timer;
    TimerNotify   _notify;

    bool _fromEpicsTime;
    epicsTime _etmDelay;
    double    _dblDelay;

    Timer ( const std::string &name, epicsTimerQueue &queue, OnTimeHandler p );

    virtual ndsStatus startPrv();
    virtual ndsStatus cancelPrv();

private:
    Timer(const Timer &timer);
    Timer();
    Timer& operator=(const Timer& timer);
};


} /* namespace nds */
#endif /* NDSTIMER_H_ */
