/*
 * ndsTrigger.h
 *
 *  Created on: 15. mar. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSTRIGGER_H_
#define NDSTRIGGER_H_

#include <vector>

namespace nds
{

class Device;
class ITriggerCondition;

typedef std::vector<ITriggerCondition*> ConditionVector;

class Trigger
{
protected:

	/// pointer to the device; it is used to access individual channels.
	Device* _device;

public:
    /// container for results of parsing
    ConditionVector condition;

	/// constructor
	Trigger(Device* device);

	/// destructor
	virtual ~Trigger();

	/// if true, checkTrigger() is true
	bool emptyString;

	/// call verifyTriggerCondition for each element in the condition vector
	virtual bool checkTrigger() const;

	/// parser for trigger condition
	ndsStatus parseTriggerCondition(const char* data);

	/// set pointer to the device
	void setDevice(nds::Device *dev);

	/// free memory, pointed to by elements of vector condition
	void cleanUp();

    /// container for results of parsing
	ITriggerCondition* getFirst() const;

	bool isEmpty() const;

};


} /* namespace nds */
#endif /* NDSTRIGGER_H_ */
