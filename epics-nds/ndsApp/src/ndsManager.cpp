/*
 * ndsManager.cpp
 *
 *  Created on: 18. jul. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <errlog.h>
#include <iostream>
#include <sstream>

#include "ndsManager.h"
#include "ndsAsynMacro.h"
#include "ndsAsynDriver.h"
#include "ndsPVContainer.h"
#include "ndsDevice.h"

#include "ndsTaskManager.h"

#ifdef TCN
#include "ndsTCN.h"
#endif

using namespace nds;

template<typename T>
void deleteDevices(T& obj)
{
    NDS_INF("Destroying: %s", obj.second->getPortName().c_str());
	obj.second->destroyPrivate();
	delete obj.second;
}

template<typename T>
void printDevice(T& obj)
{
    printf("%s\n", obj.second->getPortName().c_str() );
}

template<typename T>
void printDriver(T& obj)
{
    printf("%s\n", obj.second->getName().c_str()  );
}

template<typename T>
struct PrintFirst
{
	std::string oss;
	bool first;
	PrintFirst():first(true){}
	void operator()(T& obj)
	{
		if (!first)
			oss += ", ";
		else
			first = false;
		oss += obj.first;
	}
	std::string str()
	{
		return oss;
	}
};

ndsStatus ManagerBase::registerDriver(DriverBase* driver)
{
	Drivers::const_iterator itr = _drivers.find(driver->getName());
	if (_drivers.end() != itr)
		return ndsError;
	_drivers.insert(
			Drivers::value_type(driver->getName(),
					driver));
	return ndsSuccess;
}

ndsStatus ManagerBase::createDevice(const char* driverName,
		const char* portName,
		const char* params)
{
	// find driver
	Drivers::const_iterator itrDrv = _drivers.find(driverName);
	if (_drivers.end() == itrDrv)
	{
	    NDS_CRT("NDS driver '%s' is not registered!",
                driverName );
	    return ndsError;
	}

	AsynDevices::const_iterator itrDev  = _devices.find(portName);
	if (_devices.end() != itrDev)
	{
	    NDS_CRT("Port '%s' already exists !",
              portName);
		return ndsError;
	}

	Device* device;
	AsynDriver* asynDriver;
	try
	{
		//creating device
		device = itrDrv->second->createDevice(portName);

		//creating asyn Port
		asynDriver = new AsynDriver((Base*)device, portName );
		device->setAsynDriver(asynDriver);
	} catch (...)
	{
        NDS_CRT("Exception[%s]: ", portName);
		return ndsError;
	}

	try
	{
			if (ndsSuccess != device->createStructurePrivate(portName, params) )
			{
				NDS_CRT("ERROR: device->createStructure(%s, %s)", portName, params );
				return ndsError;
			}else
			{
					//Registering all handlers
					if ( asynSuccess != asynDriver->registerHandlers() )
					{
						NDS_CRT("ERROR: asynDriver->registerHandlers()");
						return ndsError;
					}
			}
	        NDS_INF("NDS: device %s created successfully", portName);
	}catch(...)
	{
          NDS_CRT("Exception[%s]: ", portName);
          return ndsError;
	}

	//Adding device to store
	_devices.insert(
		AsynDevices::value_type(
				portName,
				asynDriver));

	return ndsSuccess;
}

ndsStatus ManagerBase::findDevice(const char* portName,
        Device** device)
{
    *device = NULL;

    AsynDevices::iterator itr = _devices.find(portName);
    try
    {
        if ( _devices.end() != itr )
        {
            *device = dynamic_cast<Device*>( (itr->second)->getBase() );
            return ndsSuccess;
        }
    }catch(...)
    {
        NDS_ERR("NDS Manager: Can't convert device to requested type.");
    }
    return ndsError;
}


ManagerBase::~ManagerBase()
{
	std::for_each(_drivers.begin(),
			_drivers.end(),
			deleteMapObj<Drivers::value_type>);
	_drivers.empty();
}

void ManagerBase::ndsAtExit(void *param)
{
    errlogSevPrintf(
        errlogMajor,
        "NDS Manager got Exit signal!\n");

#ifdef SUPPORT_TCN
    TCN :: getInstance().wakeup();
#endif
    TaskManager::getInstance().stopAll();

    std::for_each(_devices.begin(),
			_devices.end(),
			deleteDevices<AsynDevices::value_type>);

	_devices.empty();
}

void ManagerBase::epicsInit(initHookState state)
{
	NDS_TRC("NDS Manager: IOCInit state: %d", state);
	_initHookState = state;
	switch(state)
	{
		case initHookAtBeginning:
			NDS_TRC( "initHookAtBeginning");
			std::for_each(_devices.begin(),
					_devices.end(),
					boost::bind(&AsynDriver::setIOCInitialization,
							boost::bind<AsynDriver*>(&AsynDevices::value_type::second, _1)  ) );
		break;
		case initHookAfterInitDatabase:
			NDS_TRC( "initHookAfterInitDatabase" );
		break;
		case initHookAtEnd:
			NDS_TRC( "initHookAtEnd" );
			std::for_each(_devices.begin(),
					_devices.end(),
					boost::bind(&AsynDriver::setIOCInitialized,
							boost::bind<AsynDriver*>(&AsynDevices::value_type::second, _1) ) );
			break;
		default:
		break;
	}
}

void ManagerBase::listDevices()
{
    std::for_each(_devices.begin(),
            _devices.end(),
            printDevice<AsynDevices::value_type> );
}

void ManagerBase::listDrivers()
{
    std::for_each(_drivers.begin(),
            _drivers.end(),
            printDriver<Drivers::value_type>
    );
}

void ManagerBase::printDeviceStructure(const std::string& portName)
{
    if ( portName.empty() )
    {
        NDS_ERR("Device name is not specified!");
        return;
    }

    AsynDevices::iterator itr = _devices.find(portName);
    try
    {
        if ( _devices.end() != itr )
        {
            itr->second->getBase()->printStructure("");
        }else
        {
            NDS_ERR("NDS Manager: Port 'portName' is not found.");
        }
    }catch(...)
    {
        NDS_ERR("NDS Manager: Can't convert device to requested type.");
    }

}

