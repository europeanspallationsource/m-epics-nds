/*
 * ndsThread.cpp
 *
 *  Created on: 8. avg. 2013
 *      Author: Slava Isaev
 */

#include <sstream>
#include <boost/bind.hpp>

#include "ndsDebug.h"
#include "ndsThread.h"

namespace nds
{

void runThreadBody(void* data)
{
    Thread* _thread = (Thread*)data;
    _thread->runPrv();
}

Thread::Thread(const std::string& name,
        unsigned int stackSize,
        unsigned int priority,
        TaskServiceBase* service ):
    BaseTask(name),
    _service(service),
    _thread(0)
{

}

Thread::~Thread()
{
    if( _service )
        delete _service;
}

ndsStatus Thread::cancelPrv()
{
    NDS_DBG("Thread[%s]::cancelPrv", _name.c_str() );
    _service->cancel();
    if(_thread && epicsThreadIsSuspended(_thread) )
        epicsThreadResume( _thread );
    return ndsSuccess;
}

ndsStatus Thread::startPrv()
{
    NDS_DBG("Thread[%s]::startPrv", _name.c_str() );
    try
    {
        _thread = epicsThreadCreate(
            _name.c_str(),
            epicsThreadPriorityMedium,
            epicsThreadGetStackSize(epicsThreadStackSmall),
            runThreadBody,
            (void*)this);

    }catch(...)
    {
        NDS_ERR("Start '%s' thread throws exception.", _name.c_str() );
        return ndsError;
    }
    return ndsSuccess;
}

void Thread::runPrv()
{
    // Empty method to be overridden.
    if ( !_service->isCancelled() )
    {
        setState(ndsThreadStarted);
        this->run( *_service );
    }
    setState(ndsThreadStopped);
    _service->restore();
    _thread = 0;
}

void Thread::run(nds::TaskServiceBase& service)
{
    // Empty method to be overridden.
}

void Thread::resume()
{
    if(_thread && epicsThreadIsSuspended(_thread) )
        epicsThreadResume( _thread );
}


} /* namespace nds */
