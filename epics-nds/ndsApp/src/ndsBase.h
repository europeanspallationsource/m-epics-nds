/*
 * ndsBase.cpp
 *
 *  Created on: 2. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSBASE_CPP_
#define NDSBASE_CPP_

#include <string>
#include <boost/function.hpp>
#include "ndsTypes.h"
#include "ndsPVContainer.h"
#include "ndsAsynDriver.h"
#include "ndsMacro.h"
#include "ndsMessage.h"

#define MSG_MAX_SIGE 255;

namespace nds
{

typedef boost::function<ndsStatus(asynUser *pasynUser, const Message& value)> MessageWriter;
typedef boost::function<ndsStatus(asynUser *pasynUser, Message *value)> MessageReader;
typedef std::vector<MessageWriter> MessageWriteHandlers;
typedef std::map<std::string, MessageWriteHandlers> MessageWriteHandlersMap;

/*! \mainpage NDS
 *
 * \section intro_sec Introduction
 *
 * This is the introduction.
 *
 * \section mainclases_sec Main classes
 *
 *  Main classes developer deals with are:
 *  Device
 *  Channel
 *  ChannelGroup
 *
 *	\subsection detalization_sec Channel Detalizations:
 *	ADIOChannel
 *  ImageChannel
 *
 *	\subsection asyn_sec asyn functionality:
 *	Base class exposes portAsynDriver functionallity
 *	This functionality is accessible from all main classes:
 *	Device, Channel, ChannelGroup
 *
 */

/* Base class to communicate with asyn driver.
 *
 */
class Base
{
private:
	/// @cond NDS_INTERNALS
	Base(const Base&);
	Base& operator = (const Base&);

	std::string _threadName;
	volatile bool _stopProcessing;

	MessageWriteHandlersMap _messageWriteHandlers;
	/// @endcond

protected:

	/// @cond NDS_INTERNALS
	AsynDriver*  _portDriver;
	std::string  _name; ///< instance name, assigned by driver
	int _portAddr; ///< Asyn port address is used to dispatch messaging.

#define NDS_INSTANTIATE NDS_DECLARE_SIMPL_PROP
#define NDS_INSTANTIATE_OCTET NDS_DECLARE_OCTET_PROP
#define NDS_INSTANTIATE_ARRAY NDS_DECLARE_ARRAY_PROP
	NDS_INSTANTIATE_STUB_FOR_EACH_TYPE
#undef NDS_INSTANTIATE
#undef NDS_INSTANTIATE_OCTET
#undef NDS_INSTANTIATE_ARRAY

	int _interruptIdStub;
	int _interruptIdCommand;
	int _interruptIdState;
	int _interruptIdEnabled;

	epicsInt32 _isEnabled;

	ndsStatus writeAsynMessage(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered);
	ndsStatus readAsynMessage(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason);
	ndsStatus responseMessagehandlerNotRegistered(asynUser *pasynUser, const Message& value);
	/// @endcond

	virtual ndsStatus updateMessage(const std::string& type, const std::string& text, int code );
	virtual ndsStatus updateMessage(const std::string& type, int code, const std::string& fmt, ... );

public:
	Base();
	virtual ~Base()
	{
	}

	/// @cond NDS_INTERNALS
	virtual void destroyPrivate()
	{
	}

	void processPrivate();

	//State related members
	virtual ndsStatus setIOCInitialization()=0;
	virtual ndsStatus setIOCInitialized()=0;

	ndsStatus lock();
	ndsStatus unlock();

	void setAsynDriver(AsynDriver* portDriver)
	{
		_portDriver = portDriver;
	}

	virtual void printStructure(const std::string&)=0;

	/** Message dispatcher
	 *
	 */
	ndsStatus dispatchWriteMessage(asynUser *pasynUser, const Message& message);

	// Asyn interface
	virtual void report(FILE *fp, int details)
	{
	}

	/**
	 *  User shouldn't be concerned by this function it is used
	 *  to provide base functionality
	 */
	virtual ndsStatus propagateRegisterHandlers(PVContainers* pvContainers);

	/// @endcond

	/// Get asyn port number
	int getPortAddr() const
	{
		return _portAddr;
	}

	/// Get assigned asyn port
	virtual const std::string& getPortName() const
	{
		return _portDriver->getPortName();
	}

	/// Returns string representation of the instance
	virtual const std::string toString() const;

	/// Set thread name
	void setThreadName(const std::string&);

	/// Initialize thread
	ndsStatus initThread();

	/// Thread body
	virtual void process(bool stop) {}

	/// Deterministic destroying
	virtual void destroy() {}

	virtual bool isEnabled();

	/**
	 * Register Message  write hadler.
	 *
	 *  User registering desired handler for specific message type.
	 *  Handler will be called when message of desired type is passed from PV record.
	 *
	 *  @param messageType - string message type (see programmer guide for details)
	 *	@param writer - handler's address to register
	 */
	ndsStatus registerMessageWriteHandler(const std::string& messageType,
			MessageWriter writer);

	/**
	 *  Default message read handler.
	 *
	 *  User can overwrite this handler to implement reading messages from the Device.
	 *  This method will be called when PV processed.
	 */
	virtual ndsStatus readMessage(asynUser *pasynUser, Message& value);

	/**
	 *  Default message write handler.
	 *  This handler dispatch messages through handlers
	 *  registered on specific message type.
	 *  User can overwrite this behavior.
	 */
	virtual ndsStatus writeMessage(asynUser *pasynUser, const Message& value);

	/** TEST message type handler.
	 *
	 * 	This method will be called when user send TEST message to instance.
	 *
	 */
	virtual ndsStatus doTest(asynUser *pasynUser, const Message& value);

	/** LIST message type handler
	 *
	 *  By default it lists all registered message types.
	 */
	virtual ndsStatus listCommands(asynUser *pasynUser, const Message& value);

	/** ON message type handler
	 *
	 */
	virtual ndsStatus handleOnMsg(asynUser *pasynUser, const Message& value);

	/** OFF message type handler
	 *
	 */
	virtual ndsStatus handleOffMsg(asynUser *pasynUser, const Message& value);

	/** RESET message type handler
	 *
	 */
	virtual ndsStatus handleResetMsg(asynUser *pasynUser, const Message& value);

	/** User create method [ASYN]
	 *
	 * This method is exposed from ASYN.
	 * User can override this method to pass user specific data to event handler.
	 * User must call parent function from the overloading.
	 * User data must be put to pasynUser variable.
	 */
	virtual ndsStatus drvUserCreate(asynUser *pasynUser, const char *drvInfo,
			const char **pptypeName, size_t *psize, int portAddr);

	/** User get type method [ASYN]
	 *
	 * This method is exposed from ASYN.
	 *
	 */
	virtual ndsStatus drvUserGetType(asynUser *pasynUser,
			const char **pptypeName, size_t *psize);

	/** User destroy method [ASYN]
	 *
	 * This method is exposed from ASYN.
	 * User can override this method to delete user specific.
	 *
	 * User specific data is stored in asynUser.
	 */
	virtual ndsStatus drvUserDestroy(asynUser *pasynUser);

	/** Connect method [ASYN]
	 *
	 * This method is exposed from ASYN.
	 * User can override this method if specific operations are required when driver
	 * connects to device
	 */
	virtual ndsStatus connect(asynUser *pasynUser);

	/** Disconnect method [ASYN]
	 *
	 * This method is exposed from ASYN.
	 *
	 * User can override this method if specific operations are required when driver
	 * disconnects from device
	 */
	virtual ndsStatus disconnect(asynUser *pasynUser);

	/** Register PV record's handlers
	 *
	 * User have to override this method if specific PV handlers should registered.
	 *
	 */
	virtual ndsStatus registerHandlers(PVContainers* pvContainers);

	/**
	 * Event dispatching functions.
	 * Theses functions should be used by the user to notify asyn driver when
	 * data is ready to be handled.
	 *
	 * Majority of the functions has interruptId and port address parameters.
	 * These parameters are used to select correspondent handler to process data.
	 * InterruptId is stored by the handler registering function (last parameter).
	 * Asyn address is stored during initialization in protected field _portAddr
	 */

	//@{
	/** asynInt8Array interrupt dispatching function.
	 *
	 * Dispatches Int8 array value to registered handlers.
	 * \param value is a new data (array)
	 * \param nElements is a number of elements in the array
	 * \param reason is an interrupId of the reason code to process new data
	 * \param addr   is a port address to select correct value
	 * \param timestamp is a timestamp to set record's processed time
	 * \return It returns status of the operation. If operation success it returns ndsSuccess vice versa ndsError
	 *
	 */
	ndsStatus doCallbacksInt8Array(epicsInt8 *value, size_t nElements,
			int reason, int addr, ndsStatus status=ndsSuccess);
	ndsStatus doCallbacksInt8Array(epicsInt8 *value, size_t nElements,
			int reason, int addr, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksInt8Array(epicsInt8 *value, size_t nElements,
            int reason, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksInt8Array(epicsInt8 *value, size_t nElements, int reason, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    //@}

    //@{
	/**  asynInt16Array interrupt dispatching functions
	 *
	 *   Dispatches Int16 array value to registered handlers.
	 *
     * \param value is a new data (array)
     * \param nElements is a number of elements in the array
     * \param reason is an interrupId of the reason code to process new data
     * \param addr   is a port address to select correct value
     * \param timestamp is a timestamp to set record's processed time
     * \return It returns status of the operation. If operation success it returns ndsSuccess vice versa ndsError
	 */
	ndsStatus doCallbacksInt16Array(epicsInt16 *value, size_t nElements,
			int reason, int addr, ndsStatus status=ndsSuccess);
	ndsStatus doCallbacksInt16Array(epicsInt16 *value, size_t nElements,
			int reason, int addr, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksInt16Array(epicsInt16 *value, size_t nElements,
            int reason, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksInt16Array(epicsInt16 *value, size_t nElements,
            int reason, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    //@}

    //@{
	/**  asynInt32Array interrupt dispatching function
	 *
	 *   Dispatch Int32 array value to registered handlers.
	 *
     * \param value is a new data (array)
     * \param nElements is a number of elements in the array
     * \param reason is an interrupId of the reason code to process new data
     * \param addr   is a port address to select correct value
     * \param timestamp is a timestamp to set record's processed time
     * \return It returns status of the operation. If operation success it returns ndsSuccess vice versa ndsError
	 */
	ndsStatus doCallbacksInt32Array(epicsInt32 *value, size_t nElements,
			int reason, int addr, ndsStatus status=ndsSuccess);
	ndsStatus doCallbacksInt32Array(epicsInt32 *value, size_t nElements,
			int reason, int addr, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksInt32Array(epicsInt32 *value, size_t nElements,
            int reason, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksInt32Array(epicsInt32 *value, size_t nElements,
            int reason, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    //@}

    //@{
    /**  asynFloat32Array interrupt dispatching function
	 *
	 *   Dispatch Float32 array value to registered handlers.
	 *
     * \param value is a new data (array)
     * \param nElements is a number of elements in the array
     * \param reason is an interrupId of the reason code to process new data
     * \param addr   is a port address to select correct value
     * \param timestamp is a timestamp to set record's processed time
     * \return It returns status of the operation. If operation success it returns ndsSuccess vice versa ndsError
	 *
	 */
	ndsStatus doCallbacksFloat32Array(epicsFloat32 *value,
			size_t nElements, int reason, int addr, ndsStatus status=ndsSuccess);
	ndsStatus doCallbacksFloat32Array(epicsFloat32 *value,
			size_t nElements, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksFloat32Array(epicsFloat32 *value,
            size_t nElements, int reason, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksFloat32Array(epicsFloat32 *value,
            size_t nElements, int reason, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    //@}

    //@{
	/**
	 *   asynFlaot64Array interrupt dispatching function
	 *   Dispatch Float64 array value to registered handlers.
	 *
     *  \param value is a new data (array)
     *  \param nElements is a number of elements in the array
     *  \param reason is an interrupId of the reason code to process new data
     *  \param addr   is a port address to select correct value
     *  \param timestamp is a timestamp to set record's processed time
     *  \return It returns status of the operation. If operation success it returns ndsSuccess vice versa ndsError
     *
	 */
	ndsStatus doCallbacksFloat64Array(epicsFloat64 *value,
			size_t nElements, int reason, int addr, ndsStatus status=ndsSuccess);
	ndsStatus doCallbacksFloat64Array(epicsFloat64 *value,
			size_t nElements, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksFloat64Array(epicsFloat64 *value,
            size_t nElements, int reason, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksFloat64Array(epicsFloat64 *value,
            size_t nElements, int reason, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    //@}

    //@{
	/**
	 *	GenericPointer interrupt dispatching function
	 * \param pointer is an new data
	 * \param reason is a reason code (numeric) to process new data
	 * \param addr is a port address to select handler for the reason
	 * \return It returns status of the operation
	 *
	 * Reason and port address are used to select appropriate handler to process data.
	 */
	ndsStatus doCallbacksGenericPointer(void *pointer, int reason, int addr);
    ndsStatus doCallbacksGenericPointer(void *pointer, int reason);
    //@}

    //@{
	/**
	 *   asynInt32 interrupt dispatching function
	 *   Dispatch Int32 value to registered handlers.
	 *
     * \param value is a new data (array)
     * \param reason is an interrupId of the reason code to process new data
     * \param addr   is a port address to select correct value
     * \param timestamp is a timestamp to set record's processed time
     * \return It returns status of the operation. If operation success it returns ndsSuccess vice versa ndsError
	 *
	 */
	ndsStatus doCallbacksInt32(epicsInt32 value, int reason, int addr, ndsStatus status=ndsSuccess);
	ndsStatus doCallbacksInt32(epicsInt32 value, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksInt32(epicsInt32 value, int reason, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksInt32(epicsInt32 value, int reason, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    //@}

    //@{
	/**
	 *   asynFloat64 interrupt dispatching function
	 *   Dispatch Flaot64 value to registered handlers.
	 *
     * \param value is a new data (array)
     * \param reason is an interrupId of the reason code to process new data
     * \param addr   is a port address to select correct value
     * \param timestamp is a timestamp to set record's processed time
     * \return It returns status of the operation. If operation success it returns ndsSuccess vice versa ndsError
	 *
	 */
	ndsStatus doCallbacksFloat64(epicsFloat64 value, int reason, int addr, ndsStatus status=ndsSuccess);
	ndsStatus doCallbacksFloat64(epicsFloat64 value, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksFloat64(epicsFloat64 value, int reason, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksFloat64(epicsFloat64 value, int reason, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    //@}

    //@{
	/**
	 *   asynOctet interrupt dispatching function.
	 *   Dispatch Octet value to registered handlers.
	 *
	 *   ASYN_EOM_CNT - Request count reached
	 *   ASYN_EOM_EOS - End of String detected
	 *   ASYN_EOM_END - End indicator detected
	 */
	ndsStatus doCallbacksOctet(char *data, size_t numchars, int eomReason, int reason, int addr, ndsStatus status=ndsSuccess);
	ndsStatus doCallbacksOctet(char *data, size_t numchars, int eomReason, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksOctet(char *data, size_t numchars, int eomReason, int reason, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksOctet(char *data, size_t numchars, int eomReason, int reason, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    //@}

    //@{
    /**
     *   asynOctet interrupt dispatching function.
     *   Dispatch Octet value to registered handlers.
     *
     *   @param eomReason
     *   ASYN_EOM_CNT - Request count reached
     *   ASYN_EOM_EOS - End of String detected
     *   ASYN_EOM_END - End indicator detected
     */
    ndsStatus doCallbacksOctetStr(const std::string &data, int eomReason, int reason, int addr, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksOctetStr(const std::string &data, int eomReason, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksOctetStr(const std::string &data, int eomReason, int reason, ndsStatus status=ndsSuccess);
    ndsStatus doCallbacksOctetStr(const std::string &data, int eomReason, int reason, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
//    ndsStatus doCallbacksOctetStr(const std::string &data, int reason, ndsStatus status=ndsSuccess);
//    ndsStatus doCallbacksOctetStr(const std::string &data, int reason, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    //@}

	/**
	 *   Message interrupt dispatching function.
	 *
	 *	 This function should be used to propagate new message to IOC.
	 *	 E.g. If message reading organized from thread, when new Message is ready to be processed
	 *	 user have to call this function.
	 *
	 *   @param message - new message which should be sent to IOC.
	 */
	ndsStatus doCallbacksMessage(const Message& message);

    /*** Message interrupt dispatching function
     *
     *   @param messageType - type of message
     *   @param code        - status code
     *   @param messageText - text message (details of status code)
     */
    ndsStatus doCallbacksMessage(const std::string& messageType,
                                 int code,
                                 const std::string& messageText);

    /*** Message interrupt dispatching function
     *
     *   @param messageType - type of message
     *   @param code        - status code
     *   @param messageId   - Id of message (should be set if request was marked by Id)
     *   @param messageText - text message (details of status code)
     */
    ndsStatus doCallbacksMessage(const std::string& messageType,
                                 int code,
                                 int messageId,
                                 const std::string& messageText);

    /**
     *  Stub getter for UInt32Digital type
     */
    virtual ndsStatus getUInt32Digital(asynUser *pasynUser, epicsUInt32 *value, epicsUInt32 mask);

    /**
     *  Stub getter for UInt32Digital type
     */
    virtual ndsStatus setUInt32Digital(asynUser *pasynUser, epicsUInt32 value, epicsUInt32 mask);

    //@{
    /*** asynUIn32Digital interrupt dispatching function
     *
     *   Dispatches UInt32Digital value to registered handlers.
     *
     * \param value is a new data (array)
     * \param reason is an interrupId of the reason code to process new data
     * \param addr   is a port address to select correct value
     * \param timestamp is a timestamp to set record's processed time
     * \return It returns status of the operation. If operation success it returns ndsSuccess vice versa ndsError
     *
     */
    virtual ndsStatus doCallbacksUInt32Digital(epicsUInt32 value, int reason, int addr, ndsStatus status=ndsSuccess);
    virtual ndsStatus doCallbacksUInt32Digital(epicsUInt32 value, int reason, int addr, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    virtual ndsStatus doCallbacksUInt32Digital(epicsUInt32 value, int reason, ndsStatus status=ndsSuccess);
    virtual ndsStatus doCallbacksUInt32Digital(epicsUInt32 value, int reason, epicsTimeStamp timestamp, ndsStatus status=ndsSuccess);
    //@}

#define NDS_INSTANTIATE NDS_DECLARE_FUNCTIONS
#define NDS_INSTANTIATE_OCTET NDS_DECLARE_OCTET_FUNCTIONS
#define NDS_INSTANTIATE_ARRAY NDS_DECLARE_ARRAY_FUNCTIONS
	NDS_INSTANTIATE_STUB_FOR_EACH_TYPE
#undef NDS_INSTANTIATE
#undef NDS_INSTANTIATE_OCTET
#undef NDS_INSTANTIATE_ARRAY

	/** Set the state of the entity.  [PV record setter]
	 *
	 */
	virtual ndsStatus setState(asynUser* pasynUser, epicsInt32 value);

	/** Get the state of the entity. [PV record getter]
	 *
	 */
	virtual ndsStatus getState(asynUser* pasynUser, epicsInt32 *value);

    /** Set the enabled status of the entity.  [PV record setter]
     *
     */
    virtual ndsStatus setEnabled(asynUser* pasynUser, epicsInt32 value)=0;

    /** Get the enabled status of the entity. [PV record getter]
     *
     */
    virtual ndsStatus getEnabled(asynUser* pasynUser, epicsInt32 *value)=0;

	/** Abstract method to switch ON command
	 *
	 */
	virtual ndsStatus on();

	/** Abstract method to switch OFF command
	 *
	 */
	virtual ndsStatus off();

	/** Abstract method to reset object
	 *
	 */
	virtual ndsStatus reset();
};

}


#endif /* NDSBASE_CPP_ */
