/*
 * ndsAsynChannelGroup.hpp
 *
 *  Created on: 24.07.2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSASYNDRIVER_HPP_
#define NDSASYNDRIVER_HPP_

#include <asynPortDriver.h>
#include "ndsAsynMacro.h"
#include "ndsPVContainer.h"
#include "loki/Typelist.h"

#define DEFINE_PORT_SIMPLE_HANDLERS(Type) \
virtual asynStatus write##Type(asynUser *pasynUser, epics##Type value);\
virtual asynStatus read##Type(asynUser *pasynUser, epics##Type *value);

#define DEFINE_PORT_ARRAY_HANDLERS(Type) \
virtual asynStatus write##Type##Array(asynUser *pasynUser, epics##Type *value, size_t nelements);\
virtual asynStatus read##Type##Array(asynUser *pasynUser, epics##Type *value, size_t nelements, size_t *nIn);

namespace nds
{

/// @cond NDS_INTERNALS
class Base;

/*
 *	Contract: This class is responsible to route all asynDriver requests to response.
 *	Inherited from asynPortDriver
 *
 *	Not used directly in NDS device specific drivers.
 */
class AsynDriver : public asynPortDriver
{
protected:
	PVContainers _pvContainers;
	AsynDriver();
	AsynDriver(const AsynDriver&);
	AsynDriver& operator =(const AsynDriver&);
	Base* _base;
	bool _epicsInitialized;
	std::string _portName;
public:
	AsynDriver(Base*, const char *portName);
	virtual ~AsynDriver();

	Base* getBase(){ return _base; }

	template<typename T>
    ndsStatus getBaseInstance(T** instance)
	{
	    try
	    {
	        *instance = dynamic_cast<T*>(_base);
	        return ndsSuccess;
	    }catch(std::bad_cast& bc)
	    {
	        NDS_ERR("Can't be casted to requested instance.");
	    }
	    return ndsError;
	}

	const std::string& getPortName() const;

	PVContainers* getPVContainers(){ return &_pvContainers; }
	asynStatus registerHandlers();
    asynStatus registerHandlers(Base* base);
    void destroyPrivate();
    void destroy();

    ndsStatus setIOCInitialization();
    ndsStatus setIOCInitialized();

    // Asyn functions
    virtual asynStatus drvUserCreate(asynUser *pasynUser, const char *drvInfo, const char **pptypeName, size_t *psize);
    virtual asynStatus drvUserGetType(asynUser *pasynUser, const char **pptypeName, size_t *psize);
    virtual asynStatus drvUserDestroy(asynUser *pasynUser);
    virtual void report(FILE *fp, int details);
    virtual asynStatus connect(asynUser *pasynUser);
    virtual asynStatus disconnect(asynUser *pasynUser);

    // Octet
    virtual asynStatus readOctet(asynUser *pasynUser, char *value, size_t maxChars, size_t *nActual, int *eomReason);
    virtual asynStatus writeOctet(asynUser *pasynUser, const char *value, size_t maxChars, size_t *nActual);

    // UInt32Digital
    virtual asynStatus readUInt32Digital(asynUser *pasynUser, epicsUInt32 *value, epicsUInt32 mask);
    virtual asynStatus writeUInt32Digital(asynUser *pasynUser, epicsUInt32 value, epicsUInt32 mask);
    virtual asynStatus doCallbacksUInt32Digital(epicsUInt32 value, int reason, int addr, ndsStatus);
    virtual asynStatus doCallbacksUInt32Digital(epicsUInt32 value, int reason, int addr, ndsStatus, epicsTimeStamp timestamp);

//    virtual asynStatus getBounds(asynUser *pasynUser, epicsInt32 *low, epicsInt32 *high);
    virtual asynStatus readGenericPointer(asynUser *pasynUser, void *pointer);
    virtual asynStatus writeGenericPointer(asynUser *pasynUser, void *pointer);
    virtual asynStatus doCallbacksGenericPointer(void *pointer, int reason, int addr);

    DEFINE_PORT_SIMPLE_HANDLERS(Int32)
    DEFINE_PORT_SIMPLE_HANDLERS(Float64)

    DEFINE_PORT_ARRAY_HANDLERS(Int8)
    DEFINE_PORT_ARRAY_HANDLERS(Int16)
    DEFINE_PORT_ARRAY_HANDLERS(Int32)
    DEFINE_PORT_ARRAY_HANDLERS(Float32)
    DEFINE_PORT_ARRAY_HANDLERS(Float64)

    /// Additional abstractions for doCallbacks functions
    template <typename epicsType, typename interruptType>
    asynStatus doCallbacksOneValue(epicsType value, int reason, int address, epicsTimeStamp, ndsStatus status, void *interruptPvt);

    /// Additional abstractions for doCallbacks functions
    template <typename epicsType, typename interruptType>
    asynStatus doCallbacksOctet(epicsType *data, size_t numchars, int eomReason, int reason, int addr, epicsTimeStamp, ndsStatus status, void *interruptPvt);

    template <typename epicsType, typename interruptType>
    asynStatus doCallbacksArray(epicsType *value, size_t nElements,
                                int reason, int address, epicsTimeStamp timestamp, ndsStatus status, void *interruptPvt);

    template <typename epicsType, typename interruptType>
    asynStatus doCallbacksUInt32Digital(epicsType value,
            int reason, int address, epicsTimeStamp timestamp, ndsStatus status, void* interruptPvt );

    // Additional doCallbacks functions
    DEFINE_PORT_SIMPLE_CALLBACKS(Int32)
	DEFINE_PORT_SIMPLE_CALLBACKS(Float64)

	///
	virtual asynStatus doCallbacksOctet(char *data, size_t numchars, int eomReason, int reason, int addr, ndsStatus);

	virtual asynStatus doCallbacksOctet(char *data, size_t numchars, int eomReason, int reason, int addr, ndsStatus, epicsTimeStamp timestamp);

	virtual asynStatus doCallbacksInt8Array(epicsInt8 *value,
                                    size_t nElements, int reason, int addr, ndsStatus);

    virtual asynStatus doCallbacksInt8Array(epicsInt8 *value,
                                    size_t nElements, int reason, int addr, ndsStatus, epicsTimeStamp timestamp);

	virtual asynStatus doCallbacksInt16Array(epicsInt16 *value,
                                    size_t nElements, int reason, int addr, ndsStatus);

    virtual asynStatus doCallbacksInt16Array(epicsInt16 *value,
                                    size_t nElements, int reason, int addr, ndsStatus, epicsTimeStamp timestamp);

	virtual asynStatus doCallbacksInt32Array(epicsInt32 *value,
                                    size_t nElements, int reason, int addr, ndsStatus);

	virtual asynStatus doCallbacksInt32Array(epicsInt32 *value,
                                    size_t nElements, int reason, int addr, ndsStatus, epicsTimeStamp timestamp);

	virtual asynStatus doCallbacksFloat32Array(epicsFloat32 *value,
                                    size_t nElements, int reason, int addr, ndsStatus);

	virtual asynStatus doCallbacksFloat32Array(epicsFloat32 *value,
                                    size_t nElements, int reason, int addr, ndsStatus, epicsTimeStamp timestamp);

    virtual asynStatus doCallbacksFloat64Array(epicsFloat64 *value,
                                    size_t nElements, int reason, int addr, ndsStatus);

	virtual asynStatus doCallbacksFloat64Array(epicsFloat64 *value,
                                    size_t nElements, int reason, int addr, ndsStatus, epicsTimeStamp timestamp);
};
/// @endcond


}

#endif /* NDSASYNCHANNELGROUP_HPP_ */
