/*
 * ndsPulse.cpp
 *
 *  Created on: 18. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include "ndsPulse.h"

namespace nds
{

Pulse::Pulse()
{
    // TODO Auto-generated constructor stub

}

Pulse::~Pulse()
{
    // TODO Auto-generated destructor stub
}

ndsStatus Pulse::registerHandlers(nds::PVContainers* pvContainers)
{
    TimeEvent::registerHandlers(pvContainers);

    NDS_PV_REGISTER_FLOAT64("Width", &Pulse::setWidth, &Pulse::getWidth, &_interruptIdWidth);

    return ndsSuccess;
}

ndsStatus Pulse::setWidth(asynUser* pasynUser, epicsFloat64 value)
{
    _Width = value;
    return ndsSuccess;
}

ndsStatus Pulse::getWidth(asynUser* pasynUser, epicsFloat64* value)
{
    *value = _Width;
    return ndsSuccess;
}

} /* namespace nds */
