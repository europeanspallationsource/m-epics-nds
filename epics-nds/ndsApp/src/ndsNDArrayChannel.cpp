/*
 * ndsNDArrayChannel.cpp
 *
 *  Created on: 19.01.2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

//#include "adLockKeeper.h"
//#include "utsDebug.h"
#include "utsBufferTools.h"

#include "ndsNDArrayChannel.h"

namespace nds
{

NDArrayChannel::NDArrayChannel(int maxBuffers, size_t maxMemory, NDDataType_t rawDataType, int maxSizeX, int maxSizeY):
    _sizeX( 2560 )
    ,_sizeY( 2160 )
    ,_maxSizeX( maxSizeX )
    ,_maxSizeY( maxSizeY )
    ,_rawDataType( rawDataType )
    ,_outDataType( NDUInt16 )
{
  NDS_DBG("Creating NDArray channel.");
  //  this->pNDArrayPool = new NDArrayPool(6, 100000000);
  this->pNDArrayPool = new NDArrayPool(maxBuffers, maxMemory);

  //  Allocate pArray pointer array
  this->pArrays = (NDArray **)calloc(1, sizeof(NDArray *));

  allocateBuffer();
}

NDArrayChannel::~NDArrayChannel()
{
  if (_ndaRaw)
    delete _ndaRaw;
}

ndsStatus NDArrayChannel::registerHandlers(nds::PVContainers* pvContainers)
{
	// IMPORTANT! It is important to call parent register function
	// to register all default handlers.
	nds::ImageChannel::registerHandlers(pvContainers);

	// Registration of additional PV reason handlers
	NDS_PV_REGISTER_GENERICPOINTER(
			"NDArrayData",
			&NDArrayChannel::setGenericPointer,
			&NDArrayChannel::getGenericPointer,
			&_idNDArrayData);

	NDS_PV_REGISTER_OCTET("LOAD_FILE",
			&NDArrayChannel::setLoadFile,
			&NDArrayChannel::getOctet,
			&_idLoadFile);

        NDS_PV_REGISTER_INT32("DATA_TYPE" , &NDArrayChannel::setDataType, &NDArrayChannel::getDataType, &_idDataType);
        NDS_PV_REGISTER_INT32("BIN_X"     , &NDArrayChannel::setBinX    , &NDArrayChannel::getBinX, &_idBinX);
        NDS_PV_REGISTER_INT32("BIN_Y"     , &NDArrayChannel::setBinY    , &NDArrayChannel::getBinY, &_idBinY);
        NDS_PV_REGISTER_INT32("MIN_X"     , &NDArrayChannel::setMinX    , &NDArrayChannel::getMinX, &_idMinX);
        NDS_PV_REGISTER_INT32("MIN_Y"     , &NDArrayChannel::setMinY    , &NDArrayChannel::getMinY, &_idMinY);
        NDS_PV_REGISTER_INT32("SIZE_X"    , &NDArrayChannel::setSizeX   , &NDArrayChannel::getSizeX, &_idSizeX);
        NDS_PV_REGISTER_INT32("SIZE_Y"    , &NDArrayChannel::setSizeY   , &NDArrayChannel::getSizeY, &_idSizeY);
        NDS_PV_REGISTER_INT32("REVERSE_X" , &NDArrayChannel::setReverseX, &NDArrayChannel::getReverseX, &_idReverseX);
        NDS_PV_REGISTER_INT32("REVERSE_Y" , &NDArrayChannel::setReverseY, &NDArrayChannel::getReverseY, &_idReverseY);

        return ndsSuccess;
}

ndsStatus NDArrayChannel::doCallbacksNDArray()
{
	return propogateImage();
}

ndsStatus NDArrayChannel::setGenericPointer(asynUser *pasynUser, void*)
{
  return ndsSuccess;
}

ndsStatus NDArrayChannel::getGenericPointer(asynUser *pasynUser, void*)
{
  return ndsSuccess;
}

ndsStatus NDArrayChannel::setLoadFile(asynUser *pasynUser, const char *str, size_t numchars, size_t *nbytesTransfered)
{
  NDS_STK("setLoadFile")

  *nbytesTransfered = numchars;

  if ( 0 != loadRawFromFile(str, (void**)&_BufferInt8, &_BufferInt8Size) )
	  return ndsError;

  if ( !_ndaRaw->pData )
      return ndsError;

  return propogateImage();
}

ndsStatus NDArrayChannel::convertImage()
{
    NDS_STK("propogateImage");

    int status = asynSuccess;

    NDDimension_t dimsOut[2];

    // TEST: For the error case testing comment these 2 lines.
    _ndaRaw->initDimension(&dimsOut[0], _sizeX);
    _ndaRaw->initDimension(&dimsOut[1], _sizeY);

    // X
    dimsOut[0].binning = _binX;
    dimsOut[0].offset  = _minX;
    dimsOut[0].reverse = _reverseX;
    // Y
    dimsOut[1].binning = _binY;
    dimsOut[1].offset  = _minY;
    dimsOut[1].reverse = _reverseY;


    // We save the most recent image buffer so it can be used in the read() function.
    //  Now release it before getting a new version.

	NDS_INF("Converting. Raw datatype: %d Datatype: %d BinX: %d BinY: %d MinX: %d MinY: %d SizeX: %d SizeY: %d ReverseX: %d ReverseY: %d",
			_rawDataType,
			_outDataType,
			dimsOut[0].binning,
			dimsOut[1].binning,
			dimsOut[0].offset,
			dimsOut[1].offset,
		    dimsOut[0].size,
		    dimsOut[1].size,
			dimsOut[0].reverse,
			dimsOut[1].reverse );

    if (this->pArrays[_portAddr])
      this->pArrays[_portAddr]->release();
    status = this->pNDArrayPool->convert(_ndaRaw,
                                         &this->pArrays[_portAddr],
                                         _outDataType,
                                         dimsOut);
    if (status)
    {
        NDS_ERR("NDArrayPool->convert(): error allocating buffer with: %d", status);
        return (ndsStatus)status;
    }

    return (ndsStatus)status;
}

ndsStatus NDArrayChannel::propogateImage()
{
  NDArray* pImage;
  NDArrayInfo_t arrayInfo;

  if ( !_ndaRaw->pData )
  {
	  NDS_ERR("NDArray is not initialized!");
      return ndsError;
  }

  if ( !_BufferInt8 || _BufferInt8Size<=0 )
  {
	  NDS_ERR("Image buffer is empty!");
      return ndsError;
  }

  NDS_TRC("propogating");
  {
	  	  NDS_INF("Raw buffer size: %ld", _BufferInt8Size);
	  	  memcpy(_ndaRaw->pData, _BufferInt8, _BufferInt8Size);

          if ( convertImage() == ndsSuccess )
          {
                  NDS_STK("processImage");
                  ++_imageCounter;
                  doCallbacksInt32( _imageCounter, _idNDArrayCounter    ,_portAddr );
                  doCallbacksInt32( _imageCounter, _idADNumImagesCounter,_portAddr );

                  pImage = this->pArrays[_portAddr];
                  // Put the frame number and timestamp into the buffer
                  pImage->uniqueId = _imageCounter;

                  pImage->getInfo(&arrayInfo);
                  doCallbacksInt32( arrayInfo.totalBytes, _idNDArraySize,  _portAddr);
                  doCallbacksInt32( pImage->dims[0].size, _idNDArraySizeX, _portAddr);
                  doCallbacksInt32( pImage->dims[1].size, _idNDArraySizeY, _portAddr);


#ifdef DEBUG_SAVE_RAW
                  std::stringstream ss (std::stringstream::in | std::stringstream::out);
                  ss << "img_" << pImage->uniqueId;
                  ss << ".raw";
                  saveRawToFile(ss.str().c_str(), _ndaRaw->pData, sizeof(WORD), _actualWidth* _actualHeight );
#endif

                  return callNDCallbacks( pImage );
          }
  }
  return ndsError;
}

ndsStatus NDArrayChannel::callNDCallbacks(NDArray* pImage)
{
        NDS_STK("callNDCallbacks");
        // Calling the NDArray callbacks
        // Must release the lock here, or we can get into a deadlock, because we can
        // block on the plugin lock, and the plugin can be calling us

        NDS_TRC("calling imageData callback");
        //
        return doCallbacksGenericPointer(pImage, NDArrayReason, _portAddr);
}

ndsStatus NDArrayChannel::allocateBuffer()
{
    int status = asynSuccess;
    NDArrayInfo_t arrayInfo;
    int dims[2];

    dims[0] = _maxSizeX;
    dims[1] = _maxSizeY;

    NDS_INF("Allocating NDArray. Data: %d maxSizeX: %d maxSizeY: %d", _rawDataType,  dims[0], dims[1] );
    _ndaRaw = (NDArray *) this->pNDArrayPool->alloc(2, dims, _rawDataType, 0, NULL);
//    _ndaRaw->dataSize = 0;

    if ( !_ndaRaw )
        return ndsError;

    // Make sure the raw array we have allocated is large enough.
    // We are allowed to change its size because we have exclusive use of it
    _ndaRaw->getInfo(&arrayInfo);

    if (arrayInfo.totalBytes > _ndaRaw->dataSize)
    {
        if (_ndaRaw->pData)
          free(_ndaRaw->pData);
        _ndaRaw->pData  = malloc(arrayInfo.totalBytes);
        _ndaRaw->dataSize = arrayInfo.totalBytes;
        NDS_DBG("Allocated raw NDBuffer %d bytes.", arrayInfo.totalBytes);
        if (!_ndaRaw->pData)
                status = ndsError;
    }else
    {
        NDS_INF("Buffer allocated. Total: %d", arrayInfo.totalBytes );
    }
    return (ndsStatus)status;
}

ndsStatus NDArrayChannel::setDataType(asynUser* pasynUser, epicsInt32 value)
{
  _outDataType = (NDDataType_t)value;
  doCallbacksInt32(value, _idDataType, _portAddr);
  return ndsSuccess;
}

ndsStatus NDArrayChannel::setBinX    (asynUser* pasynUser, epicsInt32 value)
{
  _binX = value;
  if (value < 1)
    _binX = 1;
  doCallbacksInt32(_binX, _idBinX, _portAddr);
  return ndsSuccess;
}

ndsStatus NDArrayChannel::setBinY    (asynUser* pasynUser, epicsInt32 value)
{
  _binY = value;
  if (value < 1)
    _binY = 1;
  doCallbacksInt32(_binY, _idBinY, _portAddr);
  return ndsSuccess;
}

ndsStatus NDArrayChannel::setMinX    (asynUser* pasynUser, epicsInt32 value)
{
  _minX = value;

  if (value < 0)
    _minX = 0;

  if (value > _maxSizeX - 1)
    _minX = _maxSizeX - 1;

  doCallbacksInt32(_minX, _idMinX, _portAddr);
  return ndsSuccess;
}

ndsStatus NDArrayChannel::setMinY    (asynUser* pasynUser, epicsInt32 value)
{
  _minY = value;

  if (value < 0)
    _minY = 0;

  if (value > _maxSizeY - 1)
    _minY = _maxSizeY - 1;

  doCallbacksInt32(_minY, _idMinY, _portAddr);
  return ndsSuccess;
}

ndsStatus NDArrayChannel::setSizeX   (asynUser* pasynUser, epicsInt32 value)
{
  _sizeX = value;
  if (value < 1 )
    _sizeX = 1;

  if (value > _maxSizeX - _minX )
    _sizeX = _maxSizeX - _minX;
  doCallbacksInt32(_sizeX, _idSizeX, _portAddr);
  return ndsSuccess;
}

ndsStatus NDArrayChannel::setSizeY   (asynUser* pasynUser, epicsInt32 value)
{
  _sizeY = value;
  if (value < 1 )
    _sizeY = 1;

  if (value > _maxSizeY - _minY )
    _sizeY = _maxSizeY - _minY;
  doCallbacksInt32(_sizeY, _idSizeY, _portAddr);
  return ndsSuccess;
}

ndsStatus NDArrayChannel::setReverseX(asynUser* pasynUser, epicsInt32 value)
{
  _reverseX = value;
  doCallbacksInt32(value, _idReverseX, _portAddr);
  return ndsSuccess;
}

ndsStatus NDArrayChannel::setReverseY(asynUser* pasynUser, epicsInt32 value)
{
  _reverseY = value;
  doCallbacksInt32(value, _idReverseY, _portAddr);
  return ndsSuccess;
}

ndsStatus NDArrayChannel::setOriginX(asynUser* pasynUser, epicsInt32 value)
{
  ndsStatus res = setMinX(pasynUser, value);
  ImageChannel::setOriginX(pasynUser, _OriginX);
  return res;
}

ndsStatus NDArrayChannel::setOriginY(asynUser* pasynUser, epicsInt32 value)
{
  ndsStatus res = setMinY(pasynUser, value);
  ImageChannel::setOriginY(pasynUser, _OriginY);
  return res;
}

ndsStatus NDArrayChannel::setWidth(asynUser* pasynUser, epicsInt32 value)
{
  ndsStatus res = setSizeX(pasynUser, value);
  ImageChannel::setWidth(pasynUser, _sizeX);
  return res;
}

ndsStatus NDArrayChannel::setHeight(asynUser* pasynUser, epicsInt32 value)
{
  ndsStatus res = setSizeY(pasynUser, value);
  ImageChannel::setHeight(pasynUser, _sizeY);
  return res;
}

ndsStatus NDArrayChannel::getDataType(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return ndsSuccess;
}

ndsStatus NDArrayChannel::getBinX    (asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return ndsSuccess;
}

ndsStatus NDArrayChannel::getBinY    (asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return ndsSuccess;
}

ndsStatus NDArrayChannel::getMinX    (asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return ndsSuccess;
}

ndsStatus NDArrayChannel::getMinY    (asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return ndsSuccess;
}

ndsStatus NDArrayChannel::getSizeX   (asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return ndsSuccess;
}

ndsStatus NDArrayChannel::getSizeY   (asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return ndsSuccess;
}

ndsStatus NDArrayChannel::getReverseX(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return ndsSuccess;
}

ndsStatus NDArrayChannel::getReverseY(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == CHANNEL_STATE_IOC_INITIALIZATION )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return ndsSuccess;
}

} /* namespace nds */
