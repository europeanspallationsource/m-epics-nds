/*
 * ChannelStates.h
 *
 *  Created on: 17. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDS_ChannelStateMACHINE_H_
#define NDS_ChannelStateMACHINE_H_

#include <string>
#include "ndsTypes.h"
#include "ndsAbstractStateMachine.h"

namespace nds
{

typedef AbstractStateMachine<ChannelStates> ChannelStateMachine;



/**
 * Base class for channel's business logic.
 */
class ChannelBaseState: public AbstractState<ChannelStates>
{
public:
    static const std::string toString(const ChannelStates& state);
};

class FunctionalChannelState: public ChannelBaseState
{
public:
    virtual ndsStatus startIOCinit(ChannelStateMachine* channel);

    virtual ndsStatus ready(ChannelStateMachine* channel);
	virtual ndsStatus on(ChannelStateMachine* channel)  __attribute__ ((deprecated));
	virtual ndsStatus off(ChannelStateMachine* channel);
	virtual ndsStatus start(ChannelStateMachine* channel);
	virtual ndsStatus defunct(ChannelStateMachine* channel);
    virtual ndsStatus reset(ChannelStateMachine* channel);

	/**
	 *	Request ERROR state
	 */
	virtual ndsStatus error(ChannelStateMachine* channel);

};

/**
 * Channel's state UNKNOWN
 */
class ChannelStateUnknown: public FunctionalChannelState
{
public:
	/// Return's current
	ChannelStates getState(){ return CHANNEL_STATE_UNKNOWN; }
	ndsStatus startIOCinit(ChannelStateMachine* channel);
};

/**
 * Channel's state IOC Initialization
 */
class ChannelStateIOCInitialization: public FunctionalChannelState
{
public:
	/// Return's current
	ChannelStates getState(){ return CHANNEL_STATE_IOC_INITIALIZATION; }
	ndsStatus off(ChannelStateMachine* channel);
};

/**
 * Channel's state OFF
 */
class ChannelStateOff: public FunctionalChannelState
{
public:
	/// Return's current
	ChannelStates getState(){ return CHANNEL_STATE_OFF; }

	/**
	 *  Request ON state
	 */
	ndsStatus on(ChannelStateMachine* channel);

	ndsStatus start(ChannelStateMachine* channel);
};

/**
 * Channel's state ON
 */
class ChannelStateOn: public FunctionalChannelState
{
public:
	/// Return's current
	ChannelStates getState(){ return CHANNEL_STATE_ON; }

	ndsStatus off(ChannelStateMachine* channel);
	ndsStatus start(ChannelStateMachine* channel);
    ndsStatus reset(ChannelStateMachine* channel);

};

/**
 * Channel state defunction
 */
class ChannelStateDefunct: public FunctionalChannelState
{
public:
	/// Return's current
	ChannelStates getState(){ return CHANNEL_STATE_DEFUNCT; }

};

/**
 * Channel's state processing
 */
class ChannelStateProcessing: public FunctionalChannelState
{
public:
	/// Return's current
	ChannelStates getState(){ return CHANNEL_STATE_PROCESSING; }

	ndsStatus off(ChannelStateMachine* channel);
    ndsStatus reset(ChannelStateMachine* channel);

};

/**
 *	Channel's Error state
 */
class ChannelStateError: public FunctionalChannelState
{
public:
	/**
	 *	Returns state id.
	 */
	ChannelStates getState(){ return CHANNEL_STATE_ERROR; }

    ndsStatus reset(ChannelStateMachine* channel);

	/**
	 *  Request OFF state
	 */
	ndsStatus off(ChannelStateMachine* channel);

	/**
	 *	Request ERROR state
	 */
	ndsStatus error(ChannelStateMachine* channel);
};


/**
 *  Channel's Ready state
 *
 *  Channel is configured and ready to process.
 *  Channel is waiting for additional conditions - triggering.
 *
 *  If channel model doesn't suppose to use additional conditions then this state could be omitted.
 */
class ChannelStateReady: public FunctionalChannelState
{
public:
    /**
     *  Returns state id.
     */
    ChannelStates getState(){ return CHANNEL_STATE_READY; }

    /**
     *  Request transition to OFF state
     */
    ndsStatus off(ChannelStateMachine* channel);

    /**
     *  Request transition to OFF state
     */
    ndsStatus on(ChannelStateMachine* channel);

    /**
     *  Request transition to PROCESSING state
     */
    ndsStatus start(ChannelStateMachine* channel);

    ndsStatus reset(ChannelStateMachine* channel);

};


class ChannelStateReset: public FunctionalChannelState
{
public:
    /**
     *  Returns state id.
     */
    ChannelStates getState(){ return CHANNEL_STATE_RESETTING; }
};

}

#endif /* ChannelStateS_H_ */
