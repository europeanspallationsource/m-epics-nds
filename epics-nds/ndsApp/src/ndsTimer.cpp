/*
 * ndsTimer.cpp
 *
 *  Created on: 23. jul. 2013
 *      Author: Slava Isaev
 */

#include <sstream>
#include <boost/bind.hpp>
#include <epicsTimer.h>
#include <epicsThread.h>

#include "ndsDebug.h"
#include "ndsTaskManager.h"
#include "ndsTimer.h"

using namespace nds;

Timer::Timer(const std::string &name, epicsTimerQueue &queue, OnTimeHandler p )
        : BaseTask(name), _timer(queue.createTimer()), _notify(_service, p)
{
}

Timer::~Timer()
{
    _timer.destroy();
}

ndsStatus Timer::start(const epicsTime& delay)
{
    _fromEpicsTime = true;
    _etmDelay = delay;
   return BaseTask::start();
}

ndsStatus Timer::start(const double& delay)
{
    _fromEpicsTime = false;
    _dblDelay = delay;
    return BaseTask::start();
}

ndsStatus Timer::startPrv()
{
    NDS_DBG("Starting '%s' timer", _name.c_str() );
    if (_fromEpicsTime)
        _timer.start(_notify, _etmDelay);
    else
        _timer.start(_notify, _dblDelay);
    _state = ndsThreadStarted;
    return ndsSuccess;
}

ndsStatus Timer::cancelPrv()
{
    NDS_DBG("Cancelling '%s' timer", _name.c_str() );
    _timer.cancel();
    _state = ndsThreadStopped;
    NDS_DBG("Timer '%s' cancelled", _name.c_str() );
    return ndsSuccess;
}

Timer::TimerNotify::TimerNotify(TaskService& service, OnTimeHandler p):
        _service(service), _handler(p)
{
}

epicsTimerNotify::expireStatus Timer::TimerNotify::expire(const epicsTime & currentTime)
{
   return _handler(_service, currentTime);
}
