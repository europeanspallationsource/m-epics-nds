/*
 * ndsDebug.h
 *
 *  Created on: 03.08.2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSDEBUG_H_
#define NDSDEBUG_H_

#ifndef NDS_LOG_OUT
#define NDS_LOG_OUT 0
#endif

#include <cstdio>
#include <string>

#include <cantProceed.h>
#include "errlog.h"

#if NDS_LOG_OUT == 1
#include <iostream>
#endif

#define PRINT(ARGS...) do{ printf(ARGS); }while(0)

#define NDS_LVL_NO_DEUG   0
#define NDS_LVL_CRITICAL  1
#define NDS_LVL_ERROR	  2
#define NDS_LVL_WARNING	  3
#define NDS_LVL_INFO 	  4
#define NDS_LVL_DEBUG 	  5
#define NDS_LVL_TRACE	  6
#define NDS_LVL_STACK 	  7

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

extern const char* strDebugLevels[];
extern const errlogSevEnum iocDebugLevels[];

epicsShareFunc int utsSetTraceLevel(int traceLevel);

epicsShareFunc int utsGetTraceLevel();

#ifdef __cplusplus
}
#endif  /* __cplusplus */


#ifndef NDS_LOG_LEVEL
#define NDS_LOG_LEVEL 0
#endif


#define NDS_LOG_EXPAND(LEVEL, args...)  NDS_LOG(LEVEL, __FILE__, __LINE__, "", args)
#define NDS_LOG(LEVEL, file, line, type, args...) \
do{ \
	if  (LEVEL <= utsGetTraceLevel() || LEVEL < NDS_LVL_WARNING)\
		NDS_LOG_OUT_FORMATTING(LEVEL, file, line, args);\
}while(0)

#define NDS_LOG_OUT_FORMATTING(LEVEL, file, line, args...) \
    do{\
        const size_t msg_size=255;\
        char msg[msg_size];\
        char format[msg_size];\
        snprintf(&msg[0], msg_size, args); \
        snprintf(&format[0], msg_size, "[%s] %s:%d %s\n", strDebugLevels[LEVEL], file, line, msg ); \
        NDS_LOG_OUT_TO(LEVEL, format);\
    }while(0)

#if NDS_LOG_OUT == 0
#define NDS_LOG_OUT_TO(LEVEL, args...) \
    do{\
        if (LEVEL == NDS_LVL_CRITICAL)\
            cantProceed("%s", args);\
        else\
            errlogSevPrintf(iocDebugLevels[LEVEL], "%s", args);\
    }while(0)
#else
#define NDS_LOG_OUT_TO(LEVEL, args...) \
	std::cout << args << std::endl
#endif

/// NDS_CRT(args...)
#define NDS_CRT(args...) NDS_LOG_EXPAND(NDS_LVL_CRITICAL, args)

/// NDS_ERR(args...)
#define NDS_ERR(args...) NDS_LOG_EXPAND(NDS_LVL_ERROR,    args)

/// NDS_WRN(args...)
#define NDS_WRN(args...) NDS_LOG_EXPAND(NDS_LVL_WARNING,  args)

/// NDS_INF(args...)
#define NDS_INF(args...) NDS_LOG_EXPAND(NDS_LVL_INFO,     args)

/// NDS_DBG(args...)
#define NDS_DBG(args...) NDS_LOG_EXPAND(NDS_LVL_DEBUG,    args)

/// NDS_TRC(args...)
#define NDS_TRC(args...) NDS_LOG_EXPAND(NDS_LVL_TRACE,    args)

/// NDS_STK(args...)
#define NDS_STK(args...) uts::StackHolder stackHolder##__LINE__(__FILE__, __LINE__, args);

/// NDS_CHECK(args...)
#define NDS_CRT_CHECK(expression, args...) NDS_CRT_CHECK_EXPAND(__FILE__, __LINE__, expression, args)

#define NDS_CRT_CHECK_EXPAND(FILE, LINE, expression, args)\
    do{\
        if(expression)\
            NDS_LOG(NDS_LVL_CRITICAL, FILE, LINE, "", args)\
    }while(0)


/// Universal Tracing System (UTS)
namespace uts
{
	class StackHolder
	{
		std::string _msg;
		std::string _file;
		int _line;
	public:
		StackHolder(const std::string msg, const std::string file, int line);
        StackHolder(const std::string file, int line, const char* format, ...);

		~StackHolder();
	};
}


#endif /* NDSDEBUG_H_ */
