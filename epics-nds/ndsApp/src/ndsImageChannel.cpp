/*
 * ndsImageChannel.cpp
 *
 *  Created on: 22. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include "ndsImageChannel.h"

using namespace nds;

#define NDS_CHANNEL_CLASS ImageChannel

ImageChannel::ImageChannel()
{
  addImageType("GRAYSCALE", 1);
  addImageType("RGB",  3);
  addImageType("BGR",  3);
  addImageType("RGBG", 4);
  addImageType("GRGB", 4);
  addImageType("RGGB", 4);

  // Registration of the handlers for the messaging mechanism
  registerMessageWriteHandler(
      "SET_IMG_FMT", /// Name of the message type which will be handled by this handler
      boost::bind(
          &ImageChannel::setImageFormatMsg, /// Address of the handler
          this,                            /// Object which owns the handler
          _1,_2)                           /// Stub functors (see boost::bind documentation for details.)
  );

  // Registration of the handlers for the messaging mechanism
  registerMessageWriteHandler(
      "LIST_IMG_FMTS", /// Name of the message type which will be handled by this handler
      boost::bind(
          &ImageChannel::listImageFormatsMsg, /// Address of the handler
          this,                              /// Object which owns the handler
          _1,_2)                             /// Stub functors (see boost::bind documentation for details.)
  );

  registerMessageWriteHandler(
      "GET_FMT", /// Name of the message type which will be handled by this handler
      boost::bind(
          &ImageChannel::getImageFormatParametersMsg, /// Address of the handler
          this,                              /// Object which owns the handler
          _1,_2)                             /// Stub functors (see boost::bind documentation for details.)
  );

}

ndsStatus ImageChannel::registerHandlers(PVContainers* pvContainers)
{
	Channel::registerHandlers(pvContainers);

    NDS_PV_REGISTER_INT32("Resolution", &ImageChannel::setResolution, &ImageChannel::getResolution, &_interruptIdResolution);
    NDS_PV_REGISTER_INT32("Width", &ImageChannel::setWidth, &ImageChannel::getWidth, &_interruptIdWidth);
    NDS_PV_REGISTER_INT32("Height", &ImageChannel::setHeight, &ImageChannel::getHeight, &_interruptIdHeight);
    NDS_PV_REGISTER_INT32("SamplesPerPixel", &ImageChannel::setSamplesPerPixel, &ImageChannel::getSamplesPerPixel, &_interruptIdSamplesPerPixel);
    NDS_PV_REGISTER_INT32("OriginX", &ImageChannel::setOriginX, &ImageChannel::getOriginX, &_interruptIdOriginX);
    NDS_PV_REGISTER_INT32("OriginY", &ImageChannel::setOriginY, &ImageChannel::getOriginY, &_interruptIdOriginY);

	NDS_PV_REGISTER_INT32("ImageType", &ImageChannel::setImageType, &Base::getInt32, &_interruptIdImageType );
    NDS_PV_REGISTER_INT32("ImageFormat", &ImageChannel::setImageFormat, &Base::getInt32, &_interruptIdImageFormat );

	return ndsSuccess;
}

ndsStatus ImageChannel::bufferProcessed()
{
	return bufferInt8Processed();
}

ndsStatus ImageChannel::listImageFormatsMsg(asynUser* pasynUser, const nds::Message& msg)
{
  Message response;
  response.messageType = msg.messageType;
  response.insert("TEXT", "Not implemented");
  response.insert("CODE", "-1");
  doCallbacksMessage(response);
  return ndsSuccess;
}

ndsStatus ImageChannel::getImageFormatParametersMsg(asynUser* pasynUser, const nds::Message& msg)
{
  Message response;
  response.messageType = msg.messageType;
  response.insert("TEXT", "Not implemented");
  response.insert("CODE", "-1");
  doCallbacksMessage(response);
  return ndsSuccess;
}

ndsStatus ImageChannel::setImageFormatMsg(asynUser* pasynUser, const nds::Message& msg)
{
  Message response;
  response.messageType = msg.messageType;
  response.insert("TEXT", "Not implemented");
  response.insert("CODE", "-1");
  doCallbacksMessage(response);
  return ndsSuccess;
}

void ImageChannel::notifyImageFormatChanged()
{
  doCallbacksInt32(_Width, _interruptIdWidth, _portAddr);
}

void ImageChannel::addImageType( const std::string& name, epicsUInt32 spp)
{
  ImageType type;
  type.typeName = name;
  type.spp = spp;
  _imageTypes.push_back(type);
}


void ImageChannel::addImageFormat( const std::string& name, epicsUInt32 width, epicsUInt32 height, epicsUInt32 res)
{
    ImageFormat fmt;
    fmt.name=name;
    fmt.width=width;
    fmt.height = height;
    fmt.res=res;
    _imageFormats.push_back(fmt);
}

ndsStatus ImageChannel::setImageFormat(asynUser* pasynUser, epicsInt32 id)
{
    _numImageFormats = _imageFormats.size();
    _ImageFormat = id;
    if ( id >= 0 && (size_t)id < _numImageFormats )
    {
        _Height          = _imageFormats[id].height;
        _Width           = _imageFormats[id].width;
        _Resolution      = _imageFormats[id].res;
        notifyImageFormatChanged();
        return ndsSuccess;
    }
    return ndsError;
}

ndsStatus ImageChannel::setImageType(asynUser* pasynUser, epicsInt32 id)
{
  _numImageTypes = _imageFormats.size();
  _ImageType = id;
  if ( id >= 0 && (size_t)id < _numImageTypes )
  {
      _SamplesPerPixel  = _imageTypes[id].spp;
      notifyImageFormatChanged();
      return ndsSuccess;
  }
  return ndsError;
}


#define NDS_INSTANTIATE NDS_IMPLEMENT_FUNCTIONS
#define NDS_INSTANTIATE_OCTET NDS_IMPLEMENT_OCTET_FUNCTIONS
#define NDS_INSTANTIATE_ARRAY NDS_IMPLEMENT_ARRAY_FUNCTIONS
	NDS_INSTANTIATE_FOR_EACH_IMAGE_CHANNEL_FUNCTION
#undef NDS_INSTANTIATE
#undef NDS_INSTANTIATE_OCTET
#undef NDS_INSTANTIATE_ARRAY
