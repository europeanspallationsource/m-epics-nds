/*
 * ndsBaseThread.cpp
 *
 *  Created on: 26.08.2013
 *      Author: Slava
 */

#include "ndsDebug.h"
#include "ndsLockKeeper.h"
#include "ndsBaseTask.h"

namespace nds
{

BaseTask::BaseTask(const std::string& name):
    _name(name),_state(ndsThreadStopped)
{

}

ndsStatus BaseTask::start()
{
    LOCK_MUTEX(&_muxState);
    if (ndsThreadStopped == _state)
    {
        _state = ndsThreadStarting;
        return startPrv();
    }
    else
    {
        NDS_DBG("Task [%s] is not stopped.", getName().c_str());
        return ndsError;
    }
}


ndsStatus BaseTask::cancel()
{
    LOCK_MUTEX(&_muxState);
    if (ndsThreadStarted == _state)
    {
        _state = ndsThreadStopping;
        return cancelPrv();
    }
    else
    {
        NDS_DBG("Task [%s] is not started.", getName().c_str());
        return ndsError;
    }
}

std::string BaseTask::getStateStr()
{
    switch(_state)
    {
    case ndsThreadStopped:
        return "Stopped";
    case ndsThreadStarted:
        return "Started";
    case ndsThreadStopping:
        return "Stopping";
    case ndsThreadStarting:
        return "Starting";
    default:
        break;
    }
    return "Unknown";
}

void BaseTask::setState(const ThreadState& state)
{
    LOCK_MUTEX(&_muxState);
    _state = state;
}


} /* namespace nds */
