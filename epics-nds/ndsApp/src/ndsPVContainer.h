/*
 * ndsPVContainer.h
 *
 *  Created on: 19.07.2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSPVCONTAINER_H_
#define NDSPVCONTAINER_H_

#include <map>
#include <vector>
#include <algorithm>
#include <string>
#include <iostream>

#include "singleton.h"
#include "loki/HierarchyGenerators.h"

#include "map_functors.h"
#include "ndsDebug.h"
#include "ndsTypeLists.h"
#include "ndsTypes.h"
#include "ndsPV.h"
#include "ndsAsynMacro.h"

#define NDS_PV_REGISTER_OCTET(reason, writer, reader, interruptId)\
do{\
	pvContainers->registerOctet(_portAddr, reason,\
			this,\
			writer, \
			reader, \
            #writer, \
            #reader, \
			interruptId);\
}while(0)

#define NDS_PV_REGISTER_INT32(reason, writer, reader, interruptId)\
do{\
	pvContainers->registerInt32(_portAddr,\
		reason,\
		this,\
		writer, \
		reader,\
            #writer, \
            #reader, \
		interruptId);\
}while(0)

#define NDS_PV_REGISTER_FLOAT64(reason, writer, reader, interruptId)\
do{\
	pvContainers->registerFloat64(_portAddr,\
		reason,\
		this,\
		writer,\
		reader, \
            #writer, \
            #reader, \
		interruptId); }while(0)

#define NDS_PV_REGISTER_FLOAT32ARRAY(reason, writer, reader, interruptId)\
do{\
	pvContainers->registerFloat32Array(_portAddr, reason,\
			this,\
			writer, \
			reader,\
            #writer, \
            #reader, \
			interruptId);\
}while(0)

#define NDS_PV_REGISTER_FLOAT64ARRAY(reason, writer, reader, interruptId)\
do{\
	pvContainers->registerFloat64Array(_portAddr, reason,\
			this,\
			writer,\
			reader,\
            #writer, \
            #reader, \
			interruptId);\
}while(0)

#define NDS_PV_REGISTER_INT8ARRAY(reason, writer, reader, interruptId)\
do{\
	pvContainers->registerInt8Array(_portAddr, reason,\
			this,\
			writer,\
			reader,\
            #writer, \
            #reader, \
			interruptId);\
}while(0)

#define NDS_PV_REGISTER_INT16ARRAY(reason, writer, reader, interruptId)\
do{\
	pvContainers->registerInt16Array(_portAddr, reason,\
			this,\
			writer,\
			reader,\
            #writer, \
            #reader, \
			interruptId);\
}while(0)

#define NDS_PV_REGISTER_INT32ARRAY(reason, writer, reader, interruptId)\
do{\
	pvContainers->registerInt32Array(_portAddr, reason,\
			this,\
			writer,\
			reader,\
            #writer, \
            #reader, \
			interruptId);\
}while(0)

#define NDS_PV_REGISTER_GENERICPOINTER(reason, writer, reader, interruptId)\
do{\
	pvContainers->registerGenericPointer(_portAddr, reason,\
			this,\
			writer,\
			reader,\
            #writer, \
            #reader, \
			interruptId);\
}while(0)

#define NDS_PV_REGISTER_UINT32D(reason, writer, reader, interruptId) NDS_PV_REGISTER_UINT32DIGITAL(reason, writer, reader, interruptId)

#define NDS_PV_REGISTER_UINT32DIGITAL(reason, writer, reader, interruptId)\
do{\
    pvContainers->registerUInt32Digital(_portAddr, reason,\
            this,\
            writer,\
            reader,\
            #writer, \
            #reader, \
            interruptId);\
}while(0)

namespace nds {

/// Class responsible to store PV's
class PVContainerBase
{
public:
  ~PVContainerBase(){}
};

template<typename T, class Base>
class RegPVHandler: public Base
{
public:
  void regPV(Loki::Type2Type<T> t){}
  ~RegPVHandler(){}
};

template<typename T>
class AsynHandlerKeeper
{
protected:
	typedef T asyn_type;
	enum
	{
		asyn_type_index = Loki::TL::IndexOf<ASYN_TYPE_LIST, T>::value
	};
	typedef typename Loki::TL::TypeAt<ASYN_2_PV, asyn_type_index>::Result pv_type;
	typedef std::vector<pv_type*> HandlerMap;
	HandlerMap _handlers;
public:
	//TODO:: fill 0 element by empty handler
	AsynHandlerKeeper(){}

	ndsStatus reg(pv_type *handlers)
	{
		size_t reasonCode = _handlers.size();
		_handlers.push_back( handlers );
		handlers->setReasonCode(reasonCode);
		return ndsSuccess;
	}

	pv_type* get(reason_type reason)
	{
		if ( reason < 0 || (unsigned int)reason >= _handlers.size() )
			return 0;
		return _handlers[reason];
	}

	void clear()
	{
	    _handlers.clear();
	}

	virtual ~AsynHandlerKeeper()
	{
		while (! _handlers.empty())
		{
			delete _handlers.back();
			_handlers.pop_back();
		}
	}
};

typedef Loki::GenScatterHierarchy< ASYN_TYPE_LIST, AsynHandlerKeeper > TypedHandlerKeeper;

template<typename T, class Base>
class RegisterPV: public Base
{
protected:
	typedef Base parent_type;
	typedef T asyn_type;
	enum
	{
		asyn_type_index = Loki::TL::IndexOf<ASYN_TYPE_LIST, T>::value
	};
	typedef typename Loki::TL::TypeAt<ASYN_2_PV, asyn_type_index>::Result pv_type;

public:
	RegisterPV()
	{
	}

	using Base::registerHandlers;
	virtual ndsStatus registerHandlers(Loki::Type2Type<T> t,  pv_type* handlers)
	{
		if (ndsSuccess == Loki::Field<T>(this->_handlerKeeper).reg(handlers) )
		{
			return this->registerReasonOnOwner(
					handlers->getPortAddr(),
					handlers->getReasonStr(),
					handlers->getReasonCode());
		}
		return ndsError;
	}

	using Base::getHandlers;
	virtual pv_type * getHandlers(Loki::Type2Type<T> t,  reason_type reason)
	{
		return Loki::Field<T>(this->_handlerKeeper).get(reason);
	}

	using Base::clear;
	void clear(Loki::Type2Type<T> t)
	{
	    Loki::Field<T>(this->_handlerKeeper).clear();
        Loki::Field<T>(this->_owners).clear();
	}

	virtual ~RegisterPV(){}
};

class BaseRegistrator
{
protected:
	typedef std::map<std::string, reason_type> Reasons;
	typedef std::vector< Reasons > Owners;

	TypedHandlerKeeper _handlerKeeper;
	Owners _owners;
	BaseRegistrator(const BaseRegistrator&);
	BaseRegistrator& operator= (const BaseRegistrator);

public:
	BaseRegistrator(){}
	virtual ndsStatus registerHandlers(){ return ndsError; }
	virtual void* getHandlers(){ return 0; }
    virtual void clear(){}

	ndsStatus registerReasonOnOwner(int portAddr, const std::string& command, reason_type reason)
	{
		if ( _owners.size() < (size_t)portAddr+1 )
			_owners.resize(portAddr+1);
		_owners[portAddr].insert(
				Reasons::value_type(command, reason));
		NDS_TRC("NDS: BaseRegistrator[%p]::registerReasonOnOwner: addr:%d reason: '%s' reason code:%d",
				this, portAddr, command.c_str(), _owners[portAddr][command]);
		return ndsSuccess;
	}

	ndsStatus getReason(int portAddr, const std::string& command, reason_type* reason)
	{
		NDS_TRC("NDS: BaseRegistrator[%p]::getReason: port %d: '%s'",
				this, portAddr, command.c_str());

		if ( _owners.size() > (size_t)portAddr )
		{
			Reasons::const_iterator itr = 	_owners[portAddr].find(command);
			if ( _owners[portAddr].end() != itr )
			{
				*reason = itr->second;
				return ndsSuccess;
			}else
			{
				NDS_WRN("NDS: getReason: port: %d: '%s' reason is not found.", portAddr, command.c_str());
			}
		}else
		{
			NDS_WRN("NDS: getReason: port: %d: '%s' portAddress is out of scope.", portAddr,
					command.c_str() );
		}
		return ndsError;
	}

	virtual ~BaseRegistrator(){}
};

class PVContainers: public Loki::GenLinearHierarchy< ASYN_TYPE_LIST, RegisterPV, BaseRegistrator >
{
public:
#define NDS_INSTANTIATE NDS_IMPL_REGISTRATION_FUNCTION
	NDS_INSTANTIATE_FOR_EACH_TYPE
#undef 	NDS_INSTANTIATE

	void clear()
	{
#define NDS_INSTANTIATE NDS_IMPL_CLEAR_FUNCTION
    NDS_INSTANTIATE_FOR_EACH_TYPE
#undef  NDS_INSTANTIATE
	}

};

typedef singleton<PVContainers> HandlerKeeper;


}  // namespace nds

#endif /* NDSPVCONTAINER_H_ */
