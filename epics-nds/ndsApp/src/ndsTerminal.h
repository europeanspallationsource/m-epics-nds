/*
 * ndsTerminal.h
 *
 *  Created on: 24. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSTERMINAL_H_
#define NDSTERMINAL_H_

#include "ndsChannel.h"

namespace nds
{

/*** Terminal class for timing devices
 *
 */
class Terminal : public Channel
{
protected:
    int _interruptIdInverted;
    epicsInt32 _Inverted;

public:
    /// C-tor
    Terminal();
    /// D-tor
    virtual ~Terminal();

    /// Add required PV's handlers
    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    /// Inverted setter
    virtual ndsStatus setInverted(asynUser* pasynUser, epicsInt32 value);

    /// Inverted getter
    virtual ndsStatus getInverted(asynUser* pasynUser, epicsInt32* value);

};

} /* namespace nds */
#endif /* NDSTERMINAL_H_ */
