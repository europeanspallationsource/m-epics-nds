/*
 * ndsLockKeeper.h
 *
 *  Created on: 13. dec. 2012
 *      Author: Viacheslav Isaev, (C) Cosylab 2013, Slovenia
 */

#ifndef NDSLOCKKeepER_H_
#define NDSLOCKKeepER_H_

#include <string>
#include <asynPortDriver.h>

#define LOCK_ASYN_PORT(port)   nds::AsynPortLocker locker##__LINE__(port, __FILE__, __LINE__);
#define UNLOCK_ASYN_PORT(port) nds::AsynPortUnLocker unlocker##__LINE__(port, __FILE__, __LINE__);
#define LOCK_MUTEX(mutex)      nds::MutexLocker mlocker##__LINE__(mutex, __FILE__, __LINE__);
#define UNLOCK_MUTEX(mutex)    nds::MutexUnlocker munlocker##__LINE__(mutex, __FILE__, __LINE__);


namespace nds
{

/** Helper class which make sure that epicsMutex will be unlocked on exit from current scope (RAII).
 *
 * Constructor accepts mutex which will be locked immediately after the object is created.
 */
class MutexLocker
{
	epicsMutex* _mutex;
	const std::string _file;
	int _line;
public:
	/* Constructor
	 *
	 * @param mutex - instance of the epcisMutex to protect
	 */
	MutexLocker(epicsMutex* mutex);
	MutexLocker(epicsMutex* mutex, const std::string file, int line);
	~MutexLocker();
};


class MutexUnlocker
{
    epicsMutex*       _mutex;
    const std::string _file;
    int _line;
public:
    /* Constructor
     *
     * @param mutex - instance of the epcisMutex to protect
     */
    MutexUnlocker(epicsMutex* mutex);
    MutexUnlocker(epicsMutex* mutex, const std::string file, int line);
    ~MutexUnlocker();
};


/** Helper class which make sure that asynDriver will be unlocked on exit from current scope (RAII).
 *
 * Constructor accepts pointer on asynPortDriver which will be locked immediately after the object is created.
 */
class AsynPortLocker
{
	asynPortDriver *_obj;
	const std::string _file;
	int _line;
public:
	/* Constructor
	 *
	 * @param obj - instance of the asynPortDriver to protect
	 */
	AsynPortLocker( asynPortDriver *obj );
	AsynPortLocker( asynPortDriver *obj, const std::string file, int line);
	virtual ~AsynPortLocker();
};


/** Helper class which make sure that asynDriver will be locked on exit from current scope (RAII).
 *
 *
 */
class AsynPortUnLocker
{
	asynPortDriver *_obj;
	const std::string _file;
	int _line;
public:
	AsynPortUnLocker( asynPortDriver *obj );
	AsynPortUnLocker( asynPortDriver *obj, const std::string file, int line);
	virtual ~AsynPortUnLocker();
};

/* Helper class which make sure that callParamCallbacks are called after the exit from scope.
 *
 * Constructors accepts pointer on asynPortDriver
 */
class ParametersCallbackCaller
{
	asynPortDriver* _obj;
	int _asynAddr;
public:
	/*
	 *
	 * @param obj - instance of the asynPortDriver to protect
	 * @param asynAddr - asynAddr to propogate changes
	 *
	 */
	ParametersCallbackCaller(asynPortDriver* obj, int asynAddr):
		_obj(obj), _asynAddr(asynAddr)
	{ }

	/* Destructor
	 *
	 */
	~ParametersCallbackCaller()
	{
	    _obj->callParamCallbacks(_asynAddr);
	}
};

} /* namespace ad */
#endif /* ADLOKER_H_ */
