/*
 * ndsAsynChannelGroup.cpp
 *
 *  Created on: 24.07.2012
 *      Author: Slava Isaev (C) Cosylab. Slovenia.
 */

#include "errlog.h"

#include "loki/TypeManip.h"
#include "loki/HierarchyGenerators.h"

#include "ndsDebug.h"
#include "ndsAsynDriver.h"
#include "ndsBase.h"
#include "ndsDriverCommand.h"

using namespace nds;


#define FUNC_CLASS AsynDriver
#define IMPL_PORT_SIMPLE_HANDLERS(Type) \
asynStatus FUNC_CLASS::write##Type(asynUser *pasynUser, epics##Type value)\
{\
	PV##Type* pv =  _pvContainers.getHandlers(Loki::Type2Type<asyn##Type>(),  pasynUser->reason);\
    if (pv)\
        try\
        {\
    	  return (asynStatus) pv->write(pasynUser, value);\
        }\
        catch(...)\
        {\
            NDS_ERR( "NDS: %s as a write%s: %s reason[%d]: %s throws exception.",  pv->getWriterNameCStr(), #Type, #Type, pasynUser->reason, pv->getReasonCStr() );\
        }\
    else\
        NDS_WRN( "NDS: write%s: %s reason: %d PV is not found",  #Type, #Type, pasynUser->reason);\
    return asynError;\
}\
asynStatus FUNC_CLASS::read##Type(asynUser *pasynUser, epics##Type *value)\
{\
	PV##Type* pv = _pvContainers.getHandlers(Loki::Type2Type<asyn##Type>(),  pasynUser->reason);\
	if (pv)\
	try\
	{\
	 	 return (asynStatus) pv->read(pasynUser, value);\
        }\
        catch(...)\
        {\
            NDS_ERR( "NDS: %s as a read%sArray: %s reason[%d]: %s throws exception.",  pv->getReaderNameCStr(), #Type, #Type, pasynUser->reason, pv->getReasonCStr());\
        }\
     else\
	         NDS_WRN( "NDS: read%s: %s reason: %d PV is not found",  #Type, #Type, pasynUser->reason);\
	return asynError;\
}

#define IMPL_PORT_ARRAY_HANDLERS(Type) \
asynStatus FUNC_CLASS::write##Type##Array(asynUser *pasynUser, epics##Type *value, size_t nelements)\
{\
	PV##Type##Array* pv =_pvContainers.getHandlers(Loki::Type2Type<asyn##Type##Array>(),  pasynUser->reason);\
	if (pv)\
	try\
        {\
		return (asynStatus) pv->write(pasynUser, value, nelements);\
        }\
        catch(...)\
        {\
          NDS_ERR( "NDS: %s as a write%s: %s reason[%d]: %s throws exception.",  pv->getWriterNameCStr(), #Type, #Type, pasynUser->reason, pv->getReasonCStr() );\
        }\
    else\
        NDS_WRN( "NDS: %s reason: %d PV is not found",  #Type, pasynUser->reason);\
	return asynError;\
}\
asynStatus FUNC_CLASS::read##Type##Array(asynUser *pasynUser, epics##Type *value, size_t nelements, size_t *nIn)\
{\
	PV##Type##Array* pv = _pvContainers.getHandlers(Loki::Type2Type<asyn##Type##Array>(),  pasynUser->reason);\
	if (pv)\
	try\
        {\
		return (asynStatus) pv->read(pasynUser, value, nelements, nIn);\
        }\
        catch(...)\
        {\
          NDS_ERR( "NDS: %s as a read%sArray: %s reason[%d]: %s throws exception.",  pv->getReaderNameCStr(), #Type, #Type, pasynUser->reason, pv->getReasonCStr());\
        }\
    else\
	     NDS_WRN( "NDS: %s reason: %d PV is not found\n",  #Type, pasynUser->reason);\
	return asynError;\
}

AsynDriver::AsynDriver(Base* base, const char *portName):
   asynPortDriver(portName,
                    256, /* maxAddr */
                    0,
asynCommonMask
| asynDrvUserMask
| asynOptionMask
| asynInt32Mask
| asynUInt32DigitalMask
| asynFloat64Mask
| asynOctetMask
| asynInt8ArrayMask
| asynInt16ArrayMask
| asynInt32ArrayMask
| asynFloat32ArrayMask
| asynFloat64ArrayMask
| asynGenericPointerMask  , /* Interface mask */

 asynInt32Mask
| asynUInt32DigitalMask
| asynInt32Mask
| asynFloat64Mask
| asynOctetMask
| asynInt8ArrayMask
| asynInt16ArrayMask
| asynInt32ArrayMask
| asynFloat32ArrayMask
| asynFloat64ArrayMask
| asynGenericPointerMask  , /* Interface mask */
        ASYN_CANBLOCK || ASYN_MULTIDEVICE, /* asynFlags.  This driver does not block and it is not multi-device, so flag is 0 */
                    1, /* Autoconnect */
                    0, /* Default priority */
                    0) /* Default stack size*/
	,_base(base)
	,_epicsInitialized(false)
	,_portName(portName)
{
	NDS_TRC("NDS: AsynDriver::Registering port: '%s'", portName);
}

const std::string& AsynDriver::getPortName() const
{
  return _portName;
}

asynStatus AsynDriver::registerHandlers()
{
  return registerHandlers(_base);
}

asynStatus AsynDriver::registerHandlers(Base* base)
{
    NDS_TRC( "NDS: AsynDriver::registerHandlers");
    try
    {
        return (asynStatus)base->propagateRegisterHandlers(&_pvContainers);
    }
    catch(...)
    {
      NDS_ERR( "NDS: registerHandlers: throws exception.");
    }
    return asynError;
}

void AsynDriver::destroyPrivate()
{
  try
  {
      lock();
      _pvContainers.clear();
      _base->destroyPrivate();
      unlock();
  }
  catch(...)
  {
    NDS_ERR( "NDS: destroyPrivate: throws exception.");
  }
}

asynStatus AsynDriver::drvUserCreate(asynUser *pasynUser, const char *drvInfo, const char **pptypeName, size_t *psize)
{
	int portAddr;
	pasynManager->getAddr(pasynUser, &portAddr);

    try
    {
        if( asynSuccess != (asynStatus)_base->drvUserCreate(pasynUser, drvInfo, pptypeName, psize, portAddr) )
        {
            return asynError;
        }

    }
    catch(...)
    {
        NDS_ERR( "NDS: drvUserCreate: %s throws exception.",  drvInfo);
    }

    DriverCommand* command = new DriverCommand(drvInfo);
    pasynUser->userData = (void*)command;

	if (  ndsError != _pvContainers.getReason( portAddr, command->getReason().c_str(), &pasynUser->reason) )
	{
		return asynSuccess;
	}else
	{
		 NDS_WRN( "NDS: %d : `%s`  reason NOT found.",  portAddr, drvInfo);
	}
	return asynError;
}

asynStatus AsynDriver::drvUserGetType(asynUser *pasynUser, const char **pptypeName, size_t *psize)
{
  try
  {
		return (asynStatus)_base->drvUserGetType(pasynUser, pptypeName, psize);
  }
  catch(...)
  {
    NDS_ERR( "NDS: drvUserGetType: throws exception.");
  }
  return asynError;
}

asynStatus AsynDriver::drvUserDestroy(asynUser *pasynUser)
{
  try
  {
	delete (DriverCommand*)pasynUser->userData;
	return (asynStatus)_base->drvUserDestroy(pasynUser);
  }
  catch(...)
  {
    NDS_ERR( "NDS: drvUserDestroy: throws exception.");
  }
  return asynError;
}

void AsynDriver::report(FILE *fp, int details)
{
}

asynStatus AsynDriver::connect(asynUser *pasynUser)
{
  try
  {
	return (asynStatus)_base->connect(pasynUser);
  }
  catch(...)
  {
    NDS_ERR( "NDS: connect: throws exception.");
  }
  return asynError;
}

asynStatus AsynDriver::disconnect(asynUser *pasynUser)
{
  try
  {
	return (asynStatus)_base->disconnect(pasynUser);
  }
  catch(...)
  {
    NDS_ERR( "NDS: disconnect: throws exception.");
  }
  return asynError;
}

asynStatus AsynDriver::readOctet(asynUser *pasynUser, char *value, size_t maxChars, size_t *nActual, int *eomReason)
{
	PVOctet* pv = _pvContainers.getHandlers(Loki::Type2Type<asynOctet>(),  pasynUser->reason);
	if (pv)
	  try
	  {
		return (asynStatus) pv->read(pasynUser, value, maxChars, nActual, eomReason);
          }
          catch(...)
          {
              NDS_ERR( "NDS: readOctet: %d throws exception.",  pasynUser->reason);
          }

	return asynError;
}

asynStatus AsynDriver::writeOctet(asynUser *pasynUser, const char *value, size_t maxChars, size_t *nActual)
{
	PVOctet* pv = _pvContainers.getHandlers(Loki::Type2Type<asynOctet>(),  pasynUser->reason);
	if (pv)
        try
	{
		return (asynStatus) pv->write(pasynUser, value, maxChars, nActual);
        }
        catch(...)
        {
          NDS_ERR( "NDS: writeOctet: %d throws exception.",  pasynUser->reason);
        }
	return asynError;
}

AsynDriver::~AsynDriver()
{
	if (_base)
		delete _base;
}

// Additional interrupt dispatchers
template <typename epicsType, typename interruptType>
asynStatus AsynDriver::doCallbacksOneValue(epicsType value, int reason, int address,
        epicsTimeStamp timestamp, ndsStatus status, void *interruptPvt)
{
    ELLLIST *pclientList;
    interruptNode *pnode;
    int addr;

    pasynManager->interruptStart(interruptPvt, &pclientList);
    pnode = (interruptNode *)ellFirst(pclientList);
    while (pnode) {
        interruptType *pInterrupt = (interruptType *)pnode->drvPvt;
        pasynManager->getAddr(pInterrupt->pasynUser, &addr);
        /* If this is not a multi-device then address is -1, change to 0 */
        if (addr == -1) addr = 0;
        if ((pInterrupt->pasynUser->reason == reason) &&
            (address == addr)) {
        	pInterrupt->pasynUser->timestamp = timestamp;
            pInterrupt->pasynUser->auxStatus = (asynStatus)status;
            pInterrupt->callback(pInterrupt->userPvt,
                                 pInterrupt->pasynUser,
                                 value);
        }
        pnode = (interruptNode *)ellNext(&pnode->node);
    }
    pasynManager->interruptEnd(interruptPvt);
    return(asynSuccess);
}

template <typename epicsType, typename interruptType>
asynStatus AsynDriver::doCallbacksOctet(epicsType *data, size_t numchars, int eomReason,
		int reason, int address, epicsTimeStamp timestamp, ndsStatus status, void *interruptPvt)
{
    ELLLIST *pclientList;
    interruptNode *pnode;
    int addr;

    pasynManager->interruptStart(interruptPvt, &pclientList);
    pnode = (interruptNode *)ellFirst(pclientList);
    while (pnode) {
        interruptType *pInterrupt = (interruptType *)pnode->drvPvt;
        pasynManager->getAddr(pInterrupt->pasynUser, &addr);
        /* If this is not a multi-device then address is -1, change to 0 */
        if (addr == -1) addr = 0;
        if ((pInterrupt->pasynUser->reason == reason) &&
            (address == addr)) {
        	pInterrupt->pasynUser->timestamp = timestamp;
            pInterrupt->pasynUser->auxStatus = (asynStatus)status;
            pInterrupt->callback(pInterrupt->userPvt,
                                 pInterrupt->pasynUser,
                                 data, numchars, eomReason);
        }
        pnode = (interruptNode *)ellNext(&pnode->node);
    }

    pasynManager->interruptEnd(interruptPvt);
    return(asynSuccess);
}

template <typename epicsType, typename interruptType>
asynStatus AsynDriver::doCallbacksArray(epicsType *value, size_t nElements,
                            int reason, int address, epicsTimeStamp timestamp, ndsStatus status, void *interruptPvt)
{
    ELLLIST *pclientList;
    interruptNode *pnode;
    int addr;

    pasynManager->interruptStart(interruptPvt, &pclientList);
    pnode = (interruptNode *)ellFirst(pclientList);
    while (pnode) {
        interruptType *pInterrupt = (interruptType *)pnode->drvPvt;
        pasynManager->getAddr(pInterrupt->pasynUser, &addr);
        /* If this is not a multi-device then address is -1, change to 0 */
        if (addr == -1) addr = 0;
        if ((pInterrupt->pasynUser->reason == reason) &&
            (address == addr)) {
        	pInterrupt->pasynUser->timestamp = timestamp;
        	pInterrupt->pasynUser->auxStatus = (asynStatus)status;
            pInterrupt->callback(pInterrupt->userPvt,
                                 pInterrupt->pasynUser,
                                 value, nElements);
        }
        pnode = (interruptNode *)ellNext(&pnode->node);
    }
    pasynManager->interruptEnd(interruptPvt);
    return(asynSuccess);
}

asynStatus AsynDriver::readGenericPointer(asynUser *pasynUser, void *pointer)
{
	PVGenericPointer* pv = _pvContainers.getHandlers(Loki::Type2Type<asynGenericPointer>(),  pasynUser->reason);
	if (pv)
	  try
	  {
		return (asynStatus) pv->read(pasynUser, pointer);
          }
          catch(...)
          {
              NDS_ERR( "NDS: readOctet: %d throws exception.\n",  pasynUser->reason);
          }

	return asynError;
}

asynStatus AsynDriver::writeGenericPointer(asynUser *pasynUser, void *pointer)
{
	PVGenericPointer* pv = _pvContainers.getHandlers(Loki::Type2Type<asynGenericPointer>(),  pasynUser->reason);
	if (pv)
		try
		{
			return (asynStatus) pv->write(pasynUser, pointer);
	    }
	    catch(...)
	    {
	          NDS_ERR( "NDS: writeOctet: %d throws exception.\n",  pasynUser->reason);
	    }
	return asynError;
}

asynStatus AsynDriver::doCallbacksGenericPointer(void *pointer, int reason, int addr)
{
	NDS_TRC("doCallbacksGenericPointer: Addr: %d Reason: %d can: %d", addr, reason, this->asynStdInterfaces.genericPointerCanInterrupt);

	return asynPortDriver::doCallbacksGenericPointer(pointer, reason, addr);
}

asynStatus AsynDriver::doCallbacksInt32(epicsInt32 value, int reason, int addr, ndsStatus status)
{
	epicsTime time;
	return doCallbacksOneValue<epicsInt32, asynInt32Interrupt>(value, reason, addr, time, status,
			this->asynStdInterfaces.int32InterruptPvt);
}

asynStatus AsynDriver::doCallbacksInt32(epicsInt32 value, int reason, int addr, ndsStatus status, epicsTimeStamp timestamp)
{
	return doCallbacksOneValue<epicsInt32, asynInt32Interrupt>(value, reason, addr, timestamp, status,
			this->asynStdInterfaces.int32InterruptPvt);
}

asynStatus AsynDriver::doCallbacksFloat64(epicsFloat64 value, int reason, int addr, ndsStatus status)
{
	epicsTime time;
	return doCallbacksOneValue<epicsFloat64, asynFloat64Interrupt>(value, reason, addr, time, status,
			this->asynStdInterfaces.float64InterruptPvt);
}

asynStatus AsynDriver::doCallbacksFloat64(epicsFloat64 value, int reason, int addr, ndsStatus status, epicsTimeStamp timestamp)
{
	return doCallbacksOneValue<epicsFloat64, asynFloat64Interrupt>(value, reason, addr, timestamp, status,
			this->asynStdInterfaces.float64InterruptPvt);
}

asynStatus AsynDriver::doCallbacksOctet(char *data, size_t numchars, int eomReason, int reason, int addr, ndsStatus status)
{
	NDS_TRC("AsynDriver::doCallbacksOctet: %d %d", addr, reason);
	epicsTime time;
	return doCallbacksOctet<char, asynOctetInterrupt>(data, numchars, eomReason, reason, addr, time, status,
			this->asynStdInterfaces.octetInterruptPvt);
}

asynStatus AsynDriver::doCallbacksOctet(char *data, size_t numchars, int eomReason, int reason, int addr, ndsStatus status, epicsTimeStamp timestamp)
{
	NDS_TRC("AsynDriver::doCallbacksOctet: %d %d", addr, reason);
	epicsTime time;
	return doCallbacksOctet<char, asynOctetInterrupt>(data, numchars, eomReason, reason, addr, timestamp, status,
			this->asynStdInterfaces.octetInterruptPvt);
}

asynStatus AsynDriver::doCallbacksInt8Array(epicsInt8 *value,
                                size_t nElements, int reason, int addr, ndsStatus status)
{
	epicsTime time;
    return(doCallbacksArray<epicsInt8, asynInt8ArrayInterrupt>(value, nElements, reason, addr, time, status,
                                        this->asynStdInterfaces.int8ArrayInterruptPvt));
}

asynStatus AsynDriver::doCallbacksInt16Array(epicsInt16 *value,
                                size_t nElements, int reason, int addr, ndsStatus status)
{
	epicsTime time;
    return(doCallbacksArray<epicsInt16, asynInt16ArrayInterrupt>(value, nElements, reason, addr, time, status,
                                        this->asynStdInterfaces.int16ArrayInterruptPvt));
}

asynStatus AsynDriver::doCallbacksInt32Array(epicsInt32 *value,
                                size_t nElements, int reason, int addr, ndsStatus status)
{
	epicsTime time;
    return(doCallbacksArray<epicsInt32, asynInt32ArrayInterrupt>(value, nElements, reason, addr, time, status,
                                        this->asynStdInterfaces.int32ArrayInterruptPvt));
}

asynStatus AsynDriver::doCallbacksInt8Array(epicsInt8 *value,
                                size_t nElements, int reason, int addr, ndsStatus status, epicsTimeStamp timestamp)
{
    return(doCallbacksArray<epicsInt8, asynInt8ArrayInterrupt>(value, nElements, reason, addr, timestamp, status,
                                        this->asynStdInterfaces.int8ArrayInterruptPvt));
}

asynStatus AsynDriver::doCallbacksInt16Array(epicsInt16 *value,
                                size_t nElements, int reason, int addr, ndsStatus status, epicsTimeStamp timestamp)
{
    return(doCallbacksArray<epicsInt16, asynInt16ArrayInterrupt>(value, nElements, reason, addr, timestamp, status,
                                        this->asynStdInterfaces.int16ArrayInterruptPvt));
}

asynStatus AsynDriver::doCallbacksInt32Array(epicsInt32 *value,
                                size_t nElements, int reason, int addr, ndsStatus status, epicsTimeStamp timestamp)
{
    return(doCallbacksArray<epicsInt32, asynInt32ArrayInterrupt>(value, nElements, reason, addr, timestamp, status,
                                        this->asynStdInterfaces.int32ArrayInterruptPvt));
}

asynStatus AsynDriver::doCallbacksFloat32Array(epicsFloat32 *value,
                                size_t nElements, int reason, int addr, ndsStatus status)
{
	epicsTime time;
    return(doCallbacksArray<epicsFloat32, asynFloat32ArrayInterrupt>(value, nElements, reason, addr, time, status,
                                        this->asynStdInterfaces.float32ArrayInterruptPvt));
}

asynStatus AsynDriver::doCallbacksFloat64Array(epicsFloat64 *value,
                                size_t nElements, int reason, int addr, ndsStatus status)
{
	epicsTime time;
    return(doCallbacksArray<epicsFloat64, asynFloat64ArrayInterrupt>(value, nElements, reason, addr, time, status,
                                        this->asynStdInterfaces.float64ArrayInterruptPvt));
}

asynStatus AsynDriver::doCallbacksFloat32Array(epicsFloat32 *value,
                                size_t nElements, int reason, int addr, ndsStatus status, epicsTimeStamp timestamp)
{
    return( doCallbacksArray<epicsFloat32, asynFloat32ArrayInterrupt>(value, nElements, reason, addr, timestamp, status,
                                        this->asynStdInterfaces.float32ArrayInterruptPvt));
}

asynStatus AsynDriver::doCallbacksFloat64Array(epicsFloat64 *value,
                                size_t nElements, int reason, int addr, ndsStatus status, epicsTimeStamp timestamp)
{
    return( doCallbacksArray<epicsFloat64, asynFloat64ArrayInterrupt>(value, nElements, reason, addr, timestamp, status,
                                        this->asynStdInterfaces.float64ArrayInterruptPvt));
}

asynStatus AsynDriver::readUInt32Digital(asynUser *pasynUser, epicsUInt32 *value, epicsUInt32 mask)
{
    PVUInt32Digital* pv = _pvContainers.getHandlers(Loki::Type2Type<asynUInt32Digital>(),  pasynUser->reason);
    if (pv)
        try
        {
            return (asynStatus) pv->read(pasynUser, value, mask);
        }
        catch(...)
        {
              NDS_ERR( "NDS: readUint32Digital: reason: %d throws exception.", pasynUser->reason);
        }
    else
        NDS_WRN( "NDS: Uin32Digital reason: %d PV is not found\n",  pasynUser->reason);
    return asynError;
}

asynStatus AsynDriver::writeUInt32Digital(asynUser *pasynUser, epicsUInt32 value, epicsUInt32 mask)
{
    PVUInt32Digital* pv =_pvContainers.getHandlers(Loki::Type2Type<asynUInt32Digital>(),  pasynUser->reason);
    if (pv)
        try
        {
            return (asynStatus) pv->write(pasynUser, value, mask);
        }
        catch(...)
        {
            NDS_ERR( "NDS: writeUin32Digital: reason: %d throws exception.", pasynUser->reason);
        }
    else
        NDS_WRN( "NDS: Uint32Digital reason: %d PV is not found", pasynUser->reason);
    return asynError;
}

template <typename epicsType, typename interruptType>
asynStatus AsynDriver::doCallbacksUInt32Digital(epicsType value,
        int reason, int address, epicsTimeStamp timestamp, ndsStatus status, void* interruptPvt )
{
    ELLLIST *pclientList;
    interruptNode *pnode;
    int addr;

    pasynManager->interruptStart(interruptPvt, &pclientList);
    pnode = (interruptNode *)ellFirst(pclientList);
    while (pnode)
    {
        interruptType *pInterrupt = (interruptType *)pnode->drvPvt;
        pasynManager->getAddr(pInterrupt->pasynUser, &addr);
        /* If this is not a multi-device then address is -1, change to 0 */
        if (addr == -1) addr = 0;
        if ((pInterrupt->pasynUser->reason == reason) &&
            (address == addr))
        {
            pInterrupt->pasynUser->timestamp = timestamp;
            pInterrupt->pasynUser->auxStatus = (asynStatus)status;
            pInterrupt->callback(pInterrupt->userPvt,
                                 pInterrupt->pasynUser,
                                 value);
        }
        pnode = (interruptNode *)ellNext(&pnode->node);
    }
    pasynManager->interruptEnd(interruptPvt);
    return(asynSuccess);
}

asynStatus AsynDriver::doCallbacksUInt32Digital(epicsUInt32 value, int reason, int addr, ndsStatus status, epicsTimeStamp timestamp)
{
    return (doCallbacksUInt32Digital<epicsUInt32, asynUInt32DigitalInterrupt > (value, reason, addr,
            timestamp, status,
            this->asynStdInterfaces.uInt32DigitalInterruptPvt));
}

asynStatus AsynDriver::doCallbacksUInt32Digital(epicsUInt32 value, int reason, int addr, ndsStatus status)
{
    epicsTime time;
    return doCallbacksUInt32Digital(value, reason, addr, status, time);
}

//IOC initialization handlers
ndsStatus AsynDriver::setIOCInitialization()
{
   NDS_TRC("AsynDriver::epicsInitialization");
   try
   {
	_base->setIOCInitialization();
	return ndsSuccess;
   }
   catch(...)
   {
     NDS_ERR( "NDS: setIOCInitialization: throws exception.");
   }
   return ndsError;
}

ndsStatus AsynDriver::setIOCInitialized()
{
  NDS_TRC("AsynDriver::epicsInitialized");
  //Database initialized
  _epicsInitialized = true;
  try
  {
      _base->setIOCInitialized();
      return ndsSuccess;
  }
  catch(...)
  {
    NDS_ERR( "NDS: setIOCInitialized: throws exception.");
  }
  return ndsError;
}

//Handler initialization
IMPL_PORT_SIMPLE_HANDLERS(Int32)
IMPL_PORT_SIMPLE_HANDLERS(Float64)

IMPL_PORT_ARRAY_HANDLERS(Int8)
IMPL_PORT_ARRAY_HANDLERS(Int16)
IMPL_PORT_ARRAY_HANDLERS(Int32)
IMPL_PORT_ARRAY_HANDLERS(Float32)
IMPL_PORT_ARRAY_HANDLERS(Float64)
