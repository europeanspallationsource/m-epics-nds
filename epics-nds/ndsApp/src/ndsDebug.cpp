/*
 * ndsDebug.cpp
 *
 *  Created on: 5. sep. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <stdio.h>
#include <stdarg.h>
#include <sstream>
#include <errlog.h>
#include "ndsDebug.h"

const char* strDebugLevels[] = {"???", "CRT", "ERR", "WRN", "INF", "DBG", "TRC", "STK"};

static int utsTraceLevel = 4;

const errlogSevEnum iocDebugLevels[] =
 {
		 errlogInfo,
		 errlogFatal,
		 errlogMajor,
		 errlogMinor,
		 errlogInfo,
		 errlogInfo,
		 errlogInfo,
		 errlogInfo
 };

uts::StackHolder::StackHolder(const std::string msg, const std::string file, int line):
	_msg(msg)
	,_file(file)
	,_line(line)
{
	//NDS_LOG(LEVEL, file, line, type, args...)
	//UTS_LOG_EXPAND(UTS_LVL_STACK, "Entering: ", _msg);
	NDS_LOG(NDS_LVL_STACK, _file.c_str(), _line, 0, "Entering: %s", _msg.c_str());
}

uts::StackHolder::StackHolder( const std::string file, int line, const char* format, ...):
    _file(file)
    ,_line(line)
{
    std::stringstream ss (std::stringstream::in | std::stringstream::out);
    char buff[255];
    va_list args;
    va_start (args, format);
    vsnprintf (buff, 255, format, args);
    va_end (args);
    ss << buff;
    _msg = ss.str();

    NDS_LOG(NDS_LVL_STACK, _file.c_str(), _line, 0, "Entering: %s", _msg.c_str());
}

uts::StackHolder::~StackHolder()
{
	//UTS_LOG_EXPAND(UTS_LVL_STACK, "Leaving: ", _msg, _file);
	NDS_LOG(NDS_LVL_STACK, _file.c_str(), _line, 0, "Leaving: %s", _msg.c_str());
}

epicsShareFunc int utsSetTraceLevel(int traceLevel)
{
    utsTraceLevel = traceLevel;
    return 0;
}

epicsShareFunc int utsGetTraceLevel()
{
    return utsTraceLevel;
}
