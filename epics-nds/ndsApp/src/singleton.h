/*
 * ndsTypes.h
 *
 *  Created on: 22.07.2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */
#ifndef SINGLETON_HPP
#define SINGLETON_HPP

#include <boost/utility.hpp>

namespace nds
{

/**
 * Class is defining singleton functionality
 */
template <typename T>
class singleton: public T, boost::noncopyable
{
private:
    struct instance_creator
    {
        instance_creator(){ singleton<T>::getInstance(); }
        void doNothing() const { }
    };
    static instance_creator _instance_creator_instance;

public:    
    typedef T value_type;
    static singleton<T>& getInstance()
    {
        static singleton<T> instance;
        _instance_creator_instance.doNothing();
        return instance;
    }
    
    static singleton<T>* getInstancePtr()
    {
        return &getInstance();
    }

};

template<typename T>
        typename singleton<T>::instance_creator singleton<T>::_instance_creator_instance;

}
#endif // SINGLETON_HPP
