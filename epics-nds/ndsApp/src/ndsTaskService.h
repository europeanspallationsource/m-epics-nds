/*
 * ndsTaskService.h
 *
 *  Created on: 27. avg. 2013
 *      Author: Slava Isaev
 */

#ifndef NDSTASKSERVICE_H_
#define NDSTASKSERVICE_H_

#include "epicsTime.h"
#include "epicsEvent.h"

namespace nds
{

typedef enum {ndsWaitStatusOk, ndsWaitStatusError, ndsWaitStatusTimeout, ndsWaitStatusEvent = ndsWaitStatusOk }
WaitStatus;

class TaskServiceBase
{
public:
    virtual ~TaskServiceBase(){}
    virtual volatile bool isCancelled() const =0;
    virtual WaitStatus sleep(double seconds) =0;
    virtual void cancel()=0;
    virtual epicsTime currentTime()=0;
    virtual void restore()=0;
};

class TaskService: public TaskServiceBase
{
private:
    TaskService(const TaskService&);
    TaskService& operator= (const TaskService&);

protected:
    epicsEvent    _event;
    volatile bool _cancelled;

public:
    TaskService();
    virtual volatile bool isCancelled() const;
    virtual WaitStatus sleep(double seconds);
    virtual void cancel();
    virtual epicsTime currentTime();
    virtual void restore();
};


} /* namespace nds */
#endif /* NDSTASKSERVICE_H_ */
