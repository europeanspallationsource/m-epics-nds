/*
 * ndsNDArrayChannel.h
 *
 *  Created on: 19.01.2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSNDARRAYCHANNEL_H_
#define NDSNDARRAYCHANNEL_H_

#include <NDArray.h>
#include <ADDriver.h>
#include "ndsImageChannel.h"

// NDArray PV reason
#define NDArrayReason 32

namespace nds
{

/*** Class to support AreaDetector's plugins
 *
 *
 */
class NDArrayChannel : public ImageChannel
{
public:
	/*** C-tor
	 *
	 *   maxBuffers
	 *   maxMemory
	 *   rawDataType
	 *   maxSizeX
	 *   maxSizeY
	 */
	NDArrayChannel(int maxBuffers, size_t maxMemory, NDDataType_t rawDataType, int maxSizeX, int maxSizeY);
        ~NDArrayChannel();

    /// Registering handlers
	ndsStatus registerHandlers(nds::PVContainers* pvContainers);

	/// Handlers for generic pointer
	virtual ndsStatus setGenericPointer(asynUser *pasynUser, void*);

	/// Handlers for generic pointer
	virtual ndsStatus getGenericPointer(asynUser *pasynUser, void*);

	/// Loads selected file for processing
	virtual ndsStatus setLoadFile(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered);

	/// Set output data type
	virtual ndsStatus setDataType(asynUser* pasynUser, epicsInt32 value);

	/// Set output X binning
	virtual ndsStatus setBinX    (asynUser* pasynUser, epicsInt32 value);

	/// Set output Y binning
	virtual ndsStatus setBinY    (asynUser* pasynUser, epicsInt32 value);

	/// Set X coordinate of left upper corner
	virtual ndsStatus setMinX    (asynUser* pasynUser, epicsInt32 value);

	/// Set Y coordinate of left upper corner
	virtual ndsStatus setMinY    (asynUser* pasynUser, epicsInt32 value);

	/// Set X size
	virtual ndsStatus setSizeX   (asynUser* pasynUser, epicsInt32 value);

	/// Set Y size
	virtual ndsStatus setSizeY   (asynUser* pasynUser, epicsInt32 value);

	/// Set X reverse
	virtual ndsStatus setReverseX(asynUser* pasynUser, epicsInt32 value);

	/// Set Y reverse
	virtual ndsStatus setReverseY(asynUser* pasynUser, epicsInt32 value);

	/// Set X region (inherited)
	virtual ndsStatus setOriginX(asynUser* pasynUser, epicsInt32 value);

	/// Set Y region (inherited)
	virtual ndsStatus setOriginY(asynUser* pasynUser, epicsInt32 value);

    /// Get output data type
    virtual ndsStatus getDataType(asynUser* pasynUser, epicsInt32 *value);

    /// Get output X binning
    virtual ndsStatus getBinX    (asynUser* pasynUser, epicsInt32 *value);

    /// Get output Y binning
    virtual ndsStatus getBinY    (asynUser* pasynUser, epicsInt32 *value);

    /// Get X coordinate of left upper corner
    virtual ndsStatus getMinX    (asynUser* pasynUser, epicsInt32 *value);

    /// Get Y coordinate of left upper corner
    virtual ndsStatus getMinY    (asynUser* pasynUser, epicsInt32 *value);

    /// Get X size
    virtual ndsStatus getSizeX   (asynUser* pasynUser, epicsInt32 *value);

    /// Get Y size
    virtual ndsStatus getSizeY   (asynUser* pasynUser, epicsInt32 *value);

    /// Get X reverse
    virtual ndsStatus getReverseX(asynUser* pasynUser, epicsInt32 *value);

    /// Get Y reverse
    virtual ndsStatus getReverseY(asynUser* pasynUser, epicsInt32 *value);


	///Set width (inherited)
	virtual ndsStatus setWidth(asynUser* pasynUser, epicsInt32 value);

	///Set height (inherited)
	virtual ndsStatus setHeight(asynUser* pasynUser, epicsInt32 value);

	/// Converts RAW buffer to NDArray
	virtual ndsStatus convertImage();

	/// Propagates image to all registered subscribers
	virtual ndsStatus propogateImage();

	/// Calls all registered callbacks
	virtual ndsStatus callNDCallbacks(NDArray* pImage);

	/// Calls all registered callbacks with default NDArray
	virtual ndsStatus doCallbacksNDArray();

	/// Allocates NDArray buffer according with maximum sizes
	virtual ndsStatus allocateBuffer();

protected:
	NDArray **pArrays;          ///< An array of NDArray pointers used to store data in the driver
	NDArrayPool *pNDArrayPool;  ///< An NDArrayPool object used to allocate and manipulate NDArray objects

	NDArray *_ndaRaw;			///< NDArray which keeps RAW data
	epicsInt32 _binX;			///< Binning on X axis
	epicsInt32 _binY;			///< Binning on Y axis
	epicsInt32 _minX;			///< X coordinate of left upper corner of ROI
	epicsInt32 _minY;			///< Y coordinate of left upper corner of ROI
	epicsInt32 _reverseX;		///< Reverse X
	epicsInt32 _reverseY;		///< Reverse Y
	epicsInt32 _sizeX;			///< X size
	epicsInt32 _sizeY;			///< Y size
	epicsInt32 _maxSizeX;		///< Maximum X size
	epicsInt32 _maxSizeY;		///< Maximum Y size
	epicsInt32 _imageCounter;   ///< Image counter
	epicsInt32 _imageRemaining; ///< Total image quantity to obtain
	NDDataType_t _rawDataType;	///< Raw    image format
	NDDataType_t _outDataType;	///< Output image format

	// reasons
	int _idDataType; 			///< Reason ID
	int _idBinX;     			///< Reason ID
	int _idBinY;     			///< Reason ID
	int _idMinX;     			///< Reason ID
	int _idMinY;	 			///< Reason ID
	int _idSizeX;	 			///< Reason ID
	int _idSizeY;    			///< Reason ID
	int _idReverseX; 			///< Reason ID
	int _idReverseY; 			///< Reason ID
	int _idNDArrayData;        	///< Reason ID
	int _idNDArrayCounter;     	///< Reason ID
	int _idADNumImagesCounter; 	///< Reason ID
	int _idNDArraySize;  		///< Reason ID
	int _idNDArraySizeX; 		///< Reason ID
	int _idNDArraySizeY; 		///< Reason ID
	int _idLoadFile;           	///< Reason ID
};

} /* namespace nds */
#endif /* NDSNDARRAYCHANNEL_H_ */
