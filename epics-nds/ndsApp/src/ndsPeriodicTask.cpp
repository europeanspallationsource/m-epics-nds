/*
 * ndsPeriodicTask.cpp
 *
 *  Created on: 08.08.2013
 *      Author: Slava
 */

#include "ndsDebug.h"
#include "ndsTaskManager.h"
#include "ndsPeriodicTask.h"

namespace nds
{

PeriodicTask* PeriodicTask::create(const std::string& name,
        unsigned int stackSize,
        unsigned int priority,
        TaskBody body)
{
    PeriodicTask* task = new PeriodicTask(name,
            stackSize,
            priority,
            new TaskService(),
            body);
    TaskManager::getInstance().registerTask(name, task);
    return task;
}

PeriodicTask::PeriodicTask(
        const std::string& name,
        unsigned int       stackSize,
        unsigned int       priority,
        TaskServiceBase*   service,
        TaskBody body):
      Thread(name, stackSize, priority, service),
      _body(body),
      _period(0)
{
}

void PeriodicTask::run(TaskServiceBase& service)
{
    while( !_service->isCancelled() )
    {
        _body(service);
        // Check if some execution were missed.
        // double delay = _t0 + (( (_service->currentTime() - _t0)/_period)+1)*_period;

        _service->sleep( _period );
    }
}

ndsStatus PeriodicTask::start(epicsTime t0, double period)
{
    if (period <= 0)
        return ndsError;

    _t0 = t0;
    _period = period;
    NDS_DBG("PeriodicTask[%s]::start period: %f", getName().c_str(), _period);
    return Thread::start();
}

ndsStatus PeriodicTask::start()
{
    return ndsError;
}


} /* namespace nds */
