/*
 * ndsBaseThread.h
 *
 *  Created on: 26.08.2013
 *      Author: Slava
 */

#ifndef NDSBASETHREAD_H_
#define NDSBASETHREAD_H_

#include <string>

#include <boost/function.hpp>

#include <epicsEvent.h>
#include <epicsMutex.h>

#include "ndsTypes.h"
#include "ndsTaskService.h"

namespace nds
{

typedef enum {ndsThreadUnknown, ndsThreadStopped, ndsThreadStarted, ndsThreadStopping, ndsThreadStarting}
ThreadState;

class BaseTask;

typedef boost::function<void (TaskServiceBase& service)> TaskBody;

typedef BaseTask* TaskPtr;

class BaseTask
{
private:
    BaseTask(const BaseTask&);
    BaseTask& operator= (const BaseTask&);
protected:
    std::string _name;
    ThreadState _state;
    epicsMutex  _muxState;

    virtual ndsStatus startPrv()=0;
    virtual ndsStatus cancelPrv()=0;

public:
    BaseTask(const std::string& name);
    virtual ~BaseTask(){}
    ndsStatus start();
    ndsStatus cancel();

    const std::string& getName(){ return _name; }
    std::string getStateStr();

    void setState(const ThreadState& state);

};


} /* namespace nds */
#endif /* NDSBASETHREAD_H_ */
