/*
 * ndsTypes.h
 *
 *  Created on: 17. jul. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSTYPELISTS_H_
#define NDSTYPELISTS_H_

#include <epicsTypes.h>

#include "loki/Typelist.h"
#include "loki/HierarchyGenerators.h"
#include "ndsPV.h"

namespace nds
{

typedef TYPELIST_10(
		asynFloat64, asynInt32, asynFloat32Array, asynFloat64Array,
		asynInt8Array, asynInt16Array, asynInt32Array, asynOctet, asynUInt32Digital, asynGenericPointer) ASYN_TYPE_LIST;

// Mapping asyn types to epics type
typedef TYPELIST_10(
		epicsFloat64, epicsInt32, epicsFloat32, epicsFloat64,
		epicsInt8, epicsInt16, epicsInt32, char, epicsInt32, void) ASYN_2_EPICS;

// Mapping asyn types to PV types
typedef TYPELIST_10(
		PVFloat64, PVInt32, PVFloat32Array, PVFloat64Array,
		PVInt8Array, PVInt16Array, PVInt32Array, PVOctet, PVUInt32Digital, PVGenericPointer) ASYN_2_PV;
}

// Define set of possible channel types
typedef TYPELIST_5(
		epicsInt8, epicsInt16, epicsInt32,
		epicsFloat32, epicsFloat64) CHANNEL_BUFFER_TYPES;

// Mapping EPCIS type of channel to value type
typedef TYPELIST_5(
		 epicsInt32, epicsInt32, epicsInt32,
		 epicsFloat64, epicsFloat64) CHANNEL_VALUE_TYPES;


#endif /* NDSTYPELISTS_H_ */
