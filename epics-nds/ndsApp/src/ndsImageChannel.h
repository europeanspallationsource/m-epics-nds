/*
 * ndsImageChannel.h
 *
 *  Created on: 22. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSIMAGECHANNEL_H_
#define NDSIMAGECHANNEL_H_

#include "ndsChannel.h"

namespace nds
{

#define NDS_INSTANTIATE_FOR_EACH_IMAGE_CHANNEL_FUNCTION \
		NDS_INSTANTIATE(Int32, Resolution) \
		NDS_INSTANTIATE(Int32, Width) \
		NDS_INSTANTIATE(Int32, Height) \
		NDS_INSTANTIATE(Int32, SamplesPerPixel)\
        NDS_INSTANTIATE(Int32, OriginX)\
        NDS_INSTANTIATE(Int32, OriginY)\

/** Description of the image type
 *
 */
struct ImageType
{
  std::string typeName; ///< Image type name
  char             spp; ///< Samples per Pixel in the specified image type
};

/** Description of the image format
 *
 */
struct ImageFormat
{
  std::string   name;   ///< Image format name
  unsigned long width; 	///< Image width in the specified image format
  unsigned long height; ///< Image height in the specified image format
  unsigned long res;    ///< Image resolution in the specified image format
};

/// Type of image types
typedef std::vector<ImageType>   ImageTypes;
/// Type of image formats
typedef std::vector<ImageFormat> ImageFormats;

/** Image channel class provides base functionality for image processing.
 *
 *
 *
 */
class ImageChannel : public Channel
{
protected:
#define NDS_INSTANTIATE NDS_DECLARE_SIMPL_PROP
#define NDS_INSTANTIATE_OCTET NDS_DECLARE_OCTET_PROP
#define NDS_INSTANTIATE_ARRAY NDS_DECLARE_ARRAY_PROP
	NDS_INSTANTIATE_FOR_EACH_IMAGE_CHANNEL_FUNCTION
#undef NDS_INSTANTIATE
#undef NDS_INSTANTIATE_OCTET
#undef NDS_INSTANTIATE_ARRAY

	/// List of image formats.
	ImageFormats _imageFormats;

	/// List of image types.
	ImageTypes _imageTypes;

	/// @cond NDS_INTERNALS
	size_t _numImageFormats;
	size_t _numImageTypes;
	/// @endcond

	/// Image format interrupt
	int _interruptIdImageFormat;
	/// Current image format
	epicsInt32 _ImageFormat;

	/// Image type interrupt
	int _interruptIdImageType;
	/// Current image type
	epicsInt32 _ImageType;
public:
	/// Ctor
	ImageChannel();

	/** \brief Register PV record handlers
	 *  Overload base method to register Image channel specific
	 *  PV's handlers.
	 */
	virtual ndsStatus registerHandlers(PVContainers* pvContainers);

	/** \brief Process buffer.
	 *  It is uspposed that Image channel will use Int8Array buffer
	 *  to handle data.
	 *  This overloading changes processing buffer to Int8Array buffer.
	 */
	virtual ndsStatus bufferProcessed();

	/**
	 *  \brief Handler for LIST_IMG_FMTS message.
	 *  Request parameters:
	 */
	virtual ndsStatus listImageFormatsMsg(asynUser* pasynUser,
			const nds::Message& msg);

	/**
	 *  \brief Handler for LIST_IMG_FMTS message.
	 *  Response parameters:
	 *     IDX: the zero-based index of the format.
	 *     N: total number of supported formats.
	 *
	 */
	virtual ndsStatus getImageFormatParametersMsg(asynUser* pasynUser,
			const nds::Message& msg);

	/**
	 *  \brief Handler for SET_IMG_FMT message.
	 *  Parameters:
	 *     ID: the zero-based index of the format.
	 *
	 */
	virtual ndsStatus setImageFormatMsg(asynUser* pasynUser,
			const nds::Message& msg);

	/** \brief Register supported image type
	 *  According with this type image size will be processed.
	 *  User have to register all possible types supported by hardware.
	 *  There are predefined types, which could be removed by cleaning _imageTypes field.
	 *  @param description - name of the image type;
	 *  @param spp - Samples Per pixel
	 */
	void addImageType(const std::string& name, epicsUInt32 spp);

	/** \brief Register supported image format
	 *  According with this formats image size will be processed.\n
	 *  User have to register all possible formats supported by hardware.
	 *  @param description - short description of the image format (E.g.: WxH R bytes);
	 *  @param width - Width of the image
	 *  @param height - Height of the image
	 *  @param res - Resolution of the image
	 */
	void addImageFormat(const std::string& description, epicsUInt32 width,
			epicsUInt32 height, epicsUInt32 res);

	/** \brief Notify when image format changed.
	 *  Start recalculation of image size.
	 */
	void notifyImageFormatChanged();

    /** \brief Type of the image raw format. [PV record setter]
	*  Asyn Reason: ImageType \n
	*  PV Record:   IMGT \n
	*/
	virtual ndsStatus setImageType(asynUser* pasynUser, epicsInt32 value);

    /** \brief [PV record setter]
	*  Asyn Reason: ImageFormat \n
	*  PV Record:   FMTS \n
	*/
	virtual ndsStatus setImageFormat(asynUser* pasynUser, epicsInt32 id);

    /** \brief X coordinate of the image upper-left corner (for cropping). [PV record setter]
	*  Asyn Reason: OriginX \n
	*  PV Record: ORGX \n
	*/
	virtual ndsStatus setOriginX(asynUser* pasynUser, epicsInt32 value);

    /** \brief Y coordinate of the image upper-left corner (for cropping).[PV record setter]
	 *  Asyn Reason: OriginY \n
	 *  PV Record: ORGY \n
	 */
	virtual ndsStatus setOriginY(asynUser* pasynUser, epicsInt32 value);

    /** \brief Resolution of the channel (number of bits per sample). For camera, number of bits per color. [PV record setter]
	 *  Asyn Reason: Resolution \n
	 *  PV Record: - \n
	 *  It is recommended to use setImageFormat to set this parameter.
	 */
	virtual ndsStatus setResolution(asynUser* pasynUser, epicsInt32 value);

    /** \brief [PV record setter]
	 *  Asyn Reason: Width \n
 	 *  PV Record: -\n
	 *  It is recommended to use setImageFormat to set this parameter.
	 */
	virtual ndsStatus setWidth(asynUser* pasynUser, epicsInt32 value);

    /** \brief Set height of the camera image [PV record setter]
	 *  Asyn Reason: Height \n
	 *  PV Record: -\n
	 *  It is recommended to use setImageFormat to set this parameter.
	 */
	virtual ndsStatus setHeight(asynUser* pasynUser, epicsInt32 value);

    /** \brief Number of samples per pixel.  [PV record setter]
	 *  Asyn Reason: SamplesPerPixel\n
	 *  PV Record: -\n
	 *  It is recommended to use setImageFormat to set this parameter.
 	 *  @param pasynUser - asynUser  (see <a src="http://www.aps.anl.gov/epics/modules/soft/asyn/R4-20/asynDriver.html#asynDriver">asynDriver documentation</a>).
	 *  @param value - new value of samples per pixel to set.
	 *  @return Function should return adequate ndsStatus value [ndsError,  ndsSuccess].
	 */
	virtual ndsStatus setSamplesPerPixel(asynUser* pasynUser, epicsInt32 value);

    /** \brief X coordinate of the image upper-left corner (for cropping). [PV record getter]
	*  Asyn Reason: OriginX\n
	*  PV Record: -\n
	*  @param pasynUser - asynUser  (see <a src="http://www.aps.anl.gov/epics/modules/soft/asyn/R4-20/asynDriver.html#asynDriver">asynDriver documentation</a>).
	*  @param value - current width value.
	*  @return Function should return adequate ndsStatus value [ndsError,  ndsSuccess].
	*/
	virtual ndsStatus getOriginX(asynUser* pasynUser, epicsInt32* value);

	/** \brief Y coordinate of the image upper-left corner (for cropping). [PV record getter]
	*  <b>Asyn Reason:<\b> OriginY \n
	*  PV Record: - \n
	*  @param pasynUser - asynUser  (see <a src="http://www.aps.anl.gov/epics/modules/soft/asyn/R4-20/asynDriver.html#asynDriver">asynDriver documentation</a>).
	*  @param value - current width value.
	*  @return Function should return adequate ndsStatus value [ndsError,  ndsSuccess].
	*/
	virtual ndsStatus getOriginY(asynUser* pasynUser, epicsInt32* value);

    /** \brief Resolution of the channel (number of bits per sample). For camera, number of bits per color. [PV record getter]
	*  Asyn Reason: Resolution \n
	*  PV Record: <b>RES</b> \n
	*  @param pasynUser - asynUser  (see <a src="http://www.aps.anl.gov/epics/modules/soft/asyn/R4-20/asynDriver.html#asynDriver">asynDriver documentation</a>).
	*  @param value - current width value.
	*  @return Function should return adequate ndsStatus value [ndsError,  ndsSuccess].
	*/
	virtual ndsStatus getResolution(asynUser* pasynUser, epicsInt32* value);

    /** \brief Width of the camera image. [PV record getter]
	*  Asyn Reason: Width \n
	*  PV Record: <b>WDTH</b> \n
	*  @param pasynUser - asynUser  (see <a src="http://www.aps.anl.gov/epics/modules/soft/asyn/R4-20/asynDriver.html#asynDriver">asynDriver documentation</a>).
	*  @param value - current width value.
	*  @return Function should return adequate ndsStatus value [ndsError,  ndsSuccess].
	*/
	virtual ndsStatus getWidth(asynUser* pasynUser, epicsInt32* value);

    /** \brief Height of the camera image.[PV record getter]
	*  Asyn Reason: Height \n
	*  PV Record: <b>HGHT</b> \n
	*  @param pasynUser - asynUser  (see <a src="http://www.aps.anl.gov/epics/modules/soft/asyn/R4-20/asynDriver.html#asynDriver">asynDriver documentation</a>).
	*  @param value - current height value.
	*  @return Function should return adequate ndsStatus value [ndsError,  ndsSuccess].
	*/
	virtual ndsStatus getHeight(asynUser* pasynUser, epicsInt32* value);

    /** \brief Number of samples per pixel. [PV record getter]
	*  Asyn Reason: SamplesPerPixel \n
	*  PV Record: <b>SPP</b>\n
	*  @param pasynUser - asynUser  (see <a src="http://www.aps.anl.gov/epics/modules/soft/asyn/R4-20/asynDriver.html#asynDriver">asynDriver documentation</a>).
	*  @param value - current samples per pixel value.
	*  @return Function should return adequate ndsStatus value [ndsError,  ndsSuccess].
	*/
	virtual ndsStatus getSamplesPerPixel(asynUser* pasynUser,
			epicsInt32* value);
};

}


#endif /* NDSIMAGECHANNEL_H_ */
