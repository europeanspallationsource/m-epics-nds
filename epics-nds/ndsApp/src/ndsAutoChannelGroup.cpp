/*
 * ndsAutoChannelGroup.cpp
 *
 *  Created on: 22. avg. 2013
 *      Author: Slava Isaev
 */

#include <ndsChannel.h>
#include "ndsAutoChannelGroup.h"

namespace nds
{


AutoChannelGroup::AutoChannelGroup(const std::string& name, const ChannelCreator& channelCreator)
:nds::ChannelGroup(name),
 _channelCreator(channelCreator)
{

}

AutoChannelGroup::~AutoChannelGroup()
{

}

nds::Channel* AutoChannelGroup::createChannel()
{
    nds::Channel* channel = _channelCreator();

    return channel;
}

ndsStatus AutoChannelGroup::drvUserCreate(asynUser *pasynUser, const char *drvInfo, const char **pptypeName, size_t *psize, int portAddr)
{
    if (CHANNEL_STATE_IOC_INITIALIZATION == this->getCurrentState() )
    {

        if ( _nodes.find( portAddr) == _nodes.end() )
        {
            NDS_DBG("Creating channel: %d", portAddr);
            nds::Channel *channel = this->createChannel();

            // Registering channel within ChannelGroup
            _nodes[portAddr] = channel;
            this->doPostRegistration(channel, portAddr);

            //Registeriign channel's PV handlers.
            _channelPortDriver->registerHandlers(channel);

            channel->setIOCInitialization();
        }
    }
    return ndsSuccess;
}


} /* namespace nds */
