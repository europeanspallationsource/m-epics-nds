/*
 * ndsTimeEvent.cpp
 *
 *  Created on: 18. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include "ndsTimeEvent.h"

namespace nds
{

TimeEvent::TimeEvent()
{
    prohibitTransition(nds::CHANNEL_STATE_ON, nds::CHANNEL_STATE_READY);
    prohibitTransition(nds::CHANNEL_STATE_ON, nds::CHANNEL_STATE_PROCESSING);
}

TimeEvent::~TimeEvent()
{

}

ndsStatus TimeEvent::registerHandlers(nds::PVContainers* pvContainers)
{
    Channel::registerHandlers(pvContainers);

    NDS_PV_REGISTER_FLOAT64("OriginTime", &TimeEvent::setOriginTime, &TimeEvent::getOriginTime, &_interruptIdOriginTime);
    NDS_PV_REGISTER_FLOAT64("Delay",      &TimeEvent::setDelay,      &TimeEvent::getDelay,      &_interruptIdDelay);
    NDS_PV_REGISTER_FLOAT64("Generated",  &Base::setFloat64,         &Base::getFloat64,         &_interruptIdGenerated);
    NDS_PV_REGISTER_INT32  ("Enabled",    &TimeEvent::setEnabled,    &TimeEvent::getEnabled,    &_interruptIdEnabled);
    NDS_PV_REGISTER_INT32  ("Level",      &TimeEvent::setLevel,      &TimeEvent::getLevel,      &_interruptIdLevel);


    cleanOnRequestStateHandlers(nds::CHANNEL_STATE_OFF, nds::CHANNEL_STATE_ON);
    return ndsSuccess;
}

ndsStatus TimeEvent::setOriginTime(asynUser* pasynUser, epicsFloat64 value)
{
    _OriginTime = value;
    return ndsSuccess;
}

ndsStatus TimeEvent::getOriginTime(asynUser* pasynUser, epicsFloat64* value)
{
    *value = _OriginTime;
    return ndsSuccess;
}

ndsStatus TimeEvent::setDelay(asynUser* pasynUser, epicsFloat64 value)
{
    _Delay = value;
    return ndsSuccess;
}

ndsStatus TimeEvent::getDelay(asynUser* pasynUser, epicsFloat64* value)
{
    *value = _Delay;
    return ndsSuccess;
}


ndsStatus TimeEvent::setEnabled(asynUser* pasynUser, epicsInt32 value)
{
    _Enabled = value;
    if (_Enabled)
        start();
    else
        disable();
    return ndsSuccess;
}

ndsStatus TimeEvent::getEnabled(asynUser* pasynUser, epicsInt32* value)
{
    if ( getCurrentState() == CHANNEL_STATE_PROCESSING )
    {
        *value = 1;
    }else
    {
        *value = 0;
    }
    return ndsSuccess;
}

ndsStatus TimeEvent::setLevel(asynUser* pasynUser, epicsInt32 value)
{
    _Level = value;
    return ndsSuccess;
}

ndsStatus TimeEvent::getLevel(asynUser* pasynUser, epicsInt32* value)
{
    *value = _Level;
    return ndsSuccess;
}


} /* namespace nds */
