/*
 * ndsChannel.h
 *
 *  Created on: 18. jul. 2012
 *      Authors: Klemen Zagar, Slava isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSCHANNEL_H_
#define NDSCHANNEL_H_

#include <map>
#include <boost/bind.hpp>
#include "errlog.h"

#include "ndsTypes.h"
#include "ndsPVContainer.h"
#include "ndsBase.h"
#include "ndsAsynMacro.h"
#include "ndsChannelStates.h"
#include "ndsBaseChannel.h"
#include "ndsDevice.h"

namespace nds
{

class ChannelGroup;
class Device;

/** Channel class
 *
 */
class Channel: public BaseChannel
{
public:
    friend class ChannelGroup;

protected:
    /// @cond NDS_INTERNALS
	Channel(const Channel&);
	Channel& operator= (const Channel&);
    ndsStatus onEpicsInitialized(ChannelStates, ChannelStates);
    /// @endcond

    /// Direction of the channel: 0 - input, output otherwise.
    int _isOutput;

	/// Interrupt ID to dispatch ValueInt32 event
	int _interruptIdValueInt32;

	/// Interrupt ID to dispatch DecimationFactor event
	int _interruptIdDecimationFactor;
	/// Interrupt ID to dispatch DecimationOffset event
	int _interruptIdDecimationOffset;
	/// Interrupt ID to dispatch SignalType event
	int _interruptIdSignalType;
	/// Interrupt ID to dispatch FFTWindow event
	int _interruptIdFFTWindow;
	/// Interrupt ID to dispatch ValueFloat64 event
	int _interruptIdValueFloat64;


	/// Interrupt ID to dispatch SignalFrequency event
	int _interruptIdSignalFrequency;
	/// Interrupt ID to dispatch SignalAmplitude event
	int _interruptIdSignalAmplitude;
	/// Interrupt ID to dispatch SignalOffset event
	int _interruptIdSignalOffset;
	/// Interrupt ID to dispatch SignalPhase event
	int _interruptIdSignalPhase;
	/// Interrupt ID to dispatch SignalDutyCycle event
	int _interruptIdSignalDutyCycle;
	/// Interrupt ID to dispatch FFTSize event
	int _interruptIdFFTSize;
	/// Interrupt ID to dispatch FFTOverlap event
	int _interruptIdFFTOverlap;
	/// Interrupt ID to dispatch FFTSmoothing event
	int _interruptIdFFTSmoothing;
	/// Interrupt ID to dispatch Streaming event
	int _interruptIdStreamingURL;

	/// Internal variable to store ValueInt32
	epicsInt32 _ValueInt32;
	/// Internal variable to store DecimationFactor
	epicsInt32 _DecimationFactor;
	/// Internal variable to store DecimationOffset
	epicsInt32 _DecimationOffset;
	/// Internal variable to store SignalType
	epicsInt32 _SignalType;
	/// Internal variable to store FFTWindow
	epicsInt32 _FFTWindow;
	/// Internal variable to store ValueFloat64
	epicsFloat64 _ValueFloat64;
	/// Internal variable to store SignalFrequency
	epicsFloat64 _SignalFrequency;
	/// Internal variable to store SignalAmplitude
	epicsFloat64 _SignalAmplitude;
	/// Internal variable to store SignalOffset
	epicsFloat64 _SignalOffset;
	/// Internal variable to store SignalPhase
	epicsFloat64 _SignalPhase;
	/// Internal variable to store SignalDutyCycle
	epicsFloat64 _SignalDutyCycle;
	/// Internal variable to store FFTSize
	epicsFloat64 _FFTSize;
	/// Internal variable to store FFTOverlap
	epicsFloat64 _FFTOverlap;
	/// Internal variable to store FFTSmoothing
	epicsFloat64 _FFTSmoothing;

	/// Interrupt ID to dispatch BufferInt32 event
	int _interruptIdBufferInt32;
	/// Interrupt ID to dispatch BufferInt16 event
	int _interruptIdBufferInt16;
	/// Interrupt ID to dispatch BufferInt8 event
	int _interruptIdBufferInt8;
	/// Interrupt ID to dispatch BufferFloat32 event
	int _interruptIdBufferFloat32;
	/// Interrupt ID to dispatch BufferFloat64 event
	int _interruptIdBufferFloat64;
	/// Interrupt ID to dispatch Conversion event
	int _interruptIdConversion;
	/// Interrupt ID to dispatch Filter event
	int _interruptIdFilter;
	/// Interrupt ID to dispatch FFTBuffer event
	int _interruptIdFFTBuffer;

	/// epicsInt32 buffer
	epicsInt32* _BufferInt32;
	/// epicsInt16 buffer
	epicsInt16* _BufferInt16;
	/// epicsInt8 buffer
	epicsInt8*  _BufferInt8;
	/// epicsFloat32 buffer
	epicsFloat32* _BufferFloat32;
	/// epicsFloat32 buffer
	epicsFloat64* _BufferFloat64;
	/// buffer for Conversion
	epicsFloat64* _Conversion;
	/// buffer for Filter
	epicsFloat64* _Filter;
	/// buffer for FFTBuffer
	epicsFloat64* _FFTBuffer;

	/// Size of Int32 buffer
    size_t _BufferInt32Size;
	/// Size of Int16 buffer
    size_t _BufferInt16Size;
	/// Size of Int8 buffer
    size_t _BufferInt8Size;
	/// Size of Float32 buffer
    size_t _BufferFloat32Size;
	/// Size of Float64 buffer
    size_t _BufferFloat64Size;
	/// Size of Conversion buffer
    size_t _ConversionSize;
	/// Size of Filter buffer
    size_t _FilterSize;
	/// Size of FFT buffer
    size_t _FFTBufferSize;

    std::string _StreamingURL;

    nds::ChannelGroup* _group;

public:
    /// Ctor
	Channel();

	/// Dtor
	virtual ~Channel(){}

    /// @cond NDS_INTERNALS
    virtual void destroyPrivate();
    /// @cond NDS_INTERNALS


    epicsInt32 getChannelNumber();
    nds::ChannelGroup* getChannelGroup();

	/// Register PV record's handlers
    virtual ndsStatus registerHandlers(PVContainers* pvContainers);

    /** Period of the sampling clock (expressed in nanoseconds). */
    long long clockPeriod();

    /**
     * Function to notify EPICS device support driver that new value ready.
     *
     * There are 2 types of data supported in NDS - single value and buffer.
     * This function used when user need to notify driver that next single value
     * is ready to be processed.
     *
     * \parameter value - new value to pass to record.
     */
    ndsStatus inputChanged(epicsFloat64 value);

    /**
     * Function to notify EPICS device support driver that new value ready.
     *
     * There are 2 types of data supported in NDS - single value and buffer.
     * This function used when user need to notify driver that next single value
     * is ready to be processed.
     *
     * \parameter value - new value to pass to record.
     */
    ndsStatus inputChanged(epicsInt32   value);

    /**
     * Function to notify EPICS device support driver that new Int8Array buffer ready
     * to be processed.
     *
     * There are 2 types of data supported in NDS - single value and buffer.
     * This function used when user need to notify driver that next buffer
     * is ready to be processed.
     * Buffer of according type will be taken and send to asynDriver.
     *
     */
    ndsStatus bufferInt8Processed();

    /**
     * Function to notify EPICS device support driver that new Int16Array buffer ready
     * to be processed.
     *
     * There are 2 types of data supported in NDS - single value and buffer.
     * This function used when user need to notify driver that next buffer
     * is ready to be processed.
     * Buffer of according type will be taken and send to asynDriver.
     *
     */
    ndsStatus bufferInt16Processed();

    /**
     * Function to notify EPICS device support driver that new Int32Array buffer ready
     * to be processed.
     *
     * There are 2 types of data supported in NDS - single value and buffer.
     * This function used when user need to notify driver that next buffer
     * is ready to be processed.
     * Buffer of according type will be taken and send to asynDriver.
     *
     */
    ndsStatus bufferInt32Processed();

    /**
     * Function to notify EPICS device support driver that new Float32Array buffer ready
     * to be processed.
     *
     * There are 2 types of data supported in NDS - single value and buffer.
     * This function used when user need to notify driver that next buffer
     * is ready to be processed.
     * Buffer of according type will be taken and send to asynDriver.
     *
     */
    ndsStatus bufferFloat32Processed();

    /**
     * Function to notify EPICS device support driver that new Float64Array buffer ready
     * to be processed.
     *
     * There are 2 types of data supported in NDS - single value and buffer.
     * This function used when user need to notify driver that next buffer
     * is ready to be processed.
     * Buffer of according type will be taken and send to asynDriver.
     *
     */
    ndsStatus bufferFloat64Processed();

    /**
     * This functions comes from NDS v1.0.
     * It is kept to provide backward compatibility.
     * By default it calls bufferFloat32Processed().
     * For ImageChannel it is overridden by one which calls
     * bufferInt8Processed();
     */
    virtual ndsStatus bufferProcessed();



    /** \brief Output channels: the contents of the buffer to output to the channel. [PV record setter]
     *
 	 *  Asyn Reason: BufferInt32\n
	 *  PV Record suffix: no suffix\n
	 *
	 *  Output channels: the contents of the buffer to output to the channel.
	 *  The values are subject to conversion from engineering units to raw units.
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setBufferInt32(asynUser* pasynUser, epicsInt32 *inArray,  size_t nelem);

    /** \brief Input channels: retrieve the next buffer. [PV record getter]
     *
	 *  Asyn Reason: BufferInt32\n
	 *  PV Record suffix: no suffix\n
	 *
	 *  Input channels: retrieve the next buffer.
	 *  NORD field specifies how many samples are read. NELM field specifies maximal number of samples.
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getBufferInt32(asynUser* pasynUser, epicsInt32 *outArray, size_t nelem,  size_t *nIn);

    /** \brief Output channels: the contents of the buffer to output to the channel. [PV record setter]
     *
	 *  Asyn Reason: BufferInt16\n
	 *  PV Record suffix: no suffix\n
	 *
	 *  Output channels: the contents of the buffer to output to the channel.
	 *  The values are subject to conversion from engineering units to raw units.
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setBufferInt16(asynUser* pasynUser, epicsInt16 *inArray,  size_t nelem);

    /** \brief Input channels: retrieve the next buffer. [PV record getter]
     *
	 *  Asyn Reason: BufferInt16\n
	 *  PV Record suffix: no suffix\n
	 *
	 *  Input channels: retrieve the next buffer.
	 *  NORD field specifies how many samples are read. NELM field specifies maximal number of samples.
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getBufferInt16(asynUser* pasynUser, epicsInt16 *outArray, size_t nelem,  size_t *nIn);

    /** \brief Output channels: the contents of the buffer to output to the channel. [PV record setter]
     *
	 *  Asyn Reason: setBufferInt8\n
	 *  PV Record suffix: no suffix\n
	 *
	 *  Output channels: the contents of the buffer to output to the channel.
	 *  The values are subject to conversion from engineering units to raw units.
	 *
	 *  This function will be called when EPICS record value is set.
     */
	virtual ndsStatus setBufferInt8(asynUser* pasynUser, epicsInt8 *inArray,  size_t nelem);

    /** \brief Input channels: retrieve the next buffer. [PV record getter]
     *
	 *  Asyn Reason: setBufferInt8\n
	 *  PV Record suffix: no suffix\n
	 *
	 *  Input channels: retrieve the next buffer.
	 *  NORD field specifies how many samples are read. NELM field specifies maximal number of samples.
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getBufferInt8(asynUser* pasynUser, epicsInt8 *outArray, size_t nelem,  size_t *nIn);

    /** \brief Output channels: the contents of the buffer to output to the channel. [PV record setter]
     *
	 *  Asyn Reason: BufferFloat32()\n
	 *  PV Record suffix: no suffix\n
	 *
	 *  Output channels: the contents of the buffer to output to the channel.
	 *  The values are subject to conversion from engineering units to raw units.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setBufferFloat32(asynUser* pasynUser, epicsFloat32 *inArray,  size_t nelem);

    /** \brief Input channels: retrieve the next buffer. [PV record getter]
     *
	 *  Asyn Reason: BufferFloat32\n
	 *  PV Record suffix: no suffix\n
	 *
	 *  Input channels: retrieve the next buffer.
	 *  NORD field specifies how many samples are read. NELM field specifies maximal number of samples.
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getBufferFloat32(asynUser* pasynUser, epicsFloat32 *outArray, size_t nelem,  size_t *nIn);

    /** \brief Output channels: the contents of the buffer to output to the channel. [PV record setter]
     *
	 *  Asyn Reason: BufferFloat64\n
	 *  PV Record suffix: no suffix\n
	 *
	 *  Output channels: the contents of the buffer to output to the channel.
	 *  The values are subject to conversion from engineering units to raw units.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setBufferFloat64(asynUser* pasynUser, epicsFloat64 *inArray,  size_t nelem);

    /** \brief Input channels: retrieve the next buffer. [PV record getter]
     *
	 *  Asyn Reason: setBufferFloat64\n
	 *  PV Record suffix: no suffix\n
	 *
	 *  Input channels: retrieve the next buffer.
	 *  NORD field specifies how many samples are read. NELM field specifies maximal number of samples.
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getBufferFloat64(asynUser* pasynUser, epicsFloat64 *outArray, size_t nelem,  size_t *nIn);

    /** \brief Set conversion of the value. [PV record setter]
     *
	 *  Asyn Reason: Conversion\n
	 *  PV Record suffix: CVT\n
	 *
	 *  Define conversion from raw values to engineering values as described in section 4.4.
	 *  Conversion is defined as a segmented cubic spline.
	 *  Waveform array consists of five-tuples, whose elements are the start of the segment,
	 *  followed by the four cubic spline coefficients.
	 *  If an empty array is given, conversion is performed according to the LINR,
	 *  EGUF and EGUL fields of the IN and OUT records.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setConversion(asynUser* pasynUser, epicsFloat64 *inArray,  size_t nelem);

    /** \brief Value conversion. [PV record getter]
     *
	 *  Asyn Reason: Conversion\n
	 *  PV Record suffix: - \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getConversion(asynUser* pasynUser, epicsFloat64 *outArray, size_t nelem,  size_t *nIn);

    /** \brief Set the filter for the analog input signal. [PV record setter]
     *
	 *  Asyn Reason: Filter\n
	 *  PV Record suffix: FILT\n
	 *
	 *  See section 4.4. of programmers guide.
	 *  If an empty array is given, the SMOO field of the IN record is used.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setFilter(asynUser* pasynUser, epicsFloat64 *inArray,  size_t nelem);

    /** \brief [PV record getter]
     *
	 *  Asyn Reason: Filter\n
	 *  PV Record suffix: -\n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getFilter(asynUser* pasynUser, epicsFloat64 *outArray, size_t nelem,  size_t *nIn);

    /** \brief  [PV record setter]
     *
	 *  Asyn Reason: ValueInt32\n
	 *  PV Record suffix: IN\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setValueInt32(asynUser* pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: ValueInt32\n
	 *  PV Record suffix: OUT\n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
	virtual ndsStatus getValueInt32(asynUser* pasynUser, epicsInt32 *value);

    /** \brief Set decimation factor [PV record setter]
     *
	 *  Asyn Reason: DecimationFactor\n
	 *  PV Record suffix: DECF\n
	 *
	 *  For input channels, only every DECF-th sample (or frame) will be sampled.
	 *  For output channel outputting a waveform, only every DECF-th sample will be output.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setDecimationFactor(asynUser* pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason:\n
	 *  PV Record suffix:\n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getDecimationFactor(asynUser* pasynUser, epicsInt32 *value);

    /** \brief  [PV record setter]
     *
	 *  Asyn Reason: DecimationOffset\n
	 *  PV Record suffix: DECO\n
	 *
	 *  Specify the index of the first sample that is not decimated.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setDecimationOffset(asynUser* pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: \n
	 *  PV Record suffix: \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getDecimationOffset(asynUser* pasynUser, epicsInt32 *value);

    /** \brief Set a value of the output channel. [PV record setter]
     *
	 *  Asyn Reason:\n
	 *  PV Record suffix: OUT\n
	 *
	 *  The value is subject to conversion from engineering units to raw units.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setValueFloat64(asynUser* pasynUser, epicsFloat64 value);

    /** \brief Read the current value of the channel. [PV record getter]
     *
	 *  Asyn Reason:\n
	 *  PV Record suffix: IN \n
	 *
	 *  The value is subject to filtering and conversion from raw units to engineering units.
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getValueFloat64(asynUser* pasynUser, epicsFloat64 *value);

    /** \brief Set the type of output signal that is generated by the output channel. [PV record setter]
     *
	 *  Asyn Reason:setSignalType\n
	 *  PV Record suffix: SGNT\n
	 *
	 *	It can be one of the following values:
	 *	0	WAVEFORM	Waveform as defined in WF record.
	 *	1	SPLINE	Spline (piecewise-cubic function) as defined in SPLN record
	 *	2	SIN	Sine curve (amplitude SGNA, offset SGNO, frequency SGNF, phase SGNP).
	 *	3	PULSE	Pulse train (amplitude SGNA, offset SGNO, frequency SGNF, phase SGNP, duty cycle SGNP).
	 *	4	SAWTOOTH	Sawtooth output (amplitude SGNA, offset SGNO, frequency SGNF, phase SGNP, rising for fraction SGNP, falling for fraction 1-SGNP).
	 *
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setSignalType(asynUser* pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getSignalType(asynUser* pasynUser, epicsInt32 *value);

    /** \brief Frequency of those types of signal that are periodic. [PV record setter]
     *
	 *  Asyn Reason: SignalFrequency\n
	 *  PV Record suffix: SGNF\n
	 *
	 *	See SGNT record for specification of the signal type.
	 *	Frequency is specified in Hz as a floating point number.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setSignalFrequency(asynUser* pasynUser, epicsFloat64 value);

    /** \brief  [PV record getter]
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getSignalFrequency(asynUser* pasynUser, epicsFloat64 *value);

    /** \brief Amplitude of the output signal. [PV record setter]
     *
	 *  Asyn Reason: SignalAmplitude\n
	 *  PV Record suffix: SGNA\n
	 *
	 *	E.g., the sine signal without offset would range from –SGNA to +SGNA.
	 *	Amplitude is specified in terms of the output’s engineering units.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setSignalAmplitude(asynUser* pasynUser, epicsFloat64 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getSignalAmplitude(asynUser* pasynUser, epicsFloat64 *value);

    /** \brief Offset to add to the output signal. [PV record setter]
     *
	 *  Asyn Reason: \n
	 *  PV Record suffix: SGNO\n
	 *
	 *  E.g., the sine signal would range from SGNO-SGNA to SGNO+SGNA.
	 *  Offset is specified in terms of the output’s engineering units.
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setSignalOffset(asynUser* pasynUser, epicsFloat64 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getSignalOffset(asynUser* pasynUser, epicsFloat64 *value);

    /** \brief Phase of the generated output signal. [PV record setter]
     *
	 *  Asyn Reason: SignalPhase\n
	 *  PV Record suffix: SGNP \n
	 *
	 *  Phase is relative to the epoch of the time base (e.g., the TCN).
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setSignalPhase(asynUser* pasynUser, epicsFloat64 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getSignalPhase(asynUser* pasynUser, epicsFloat64 *value);

    /** \brief Set the duty cycle of the output signal. [PV record setter]
     *
	 *  Asyn Reason: SignalDutyCycle\n
	 *  PV Record suffix: SGND\n
	 *
	 *  Duty cycle is expressed as a fraction of the whole cycle when the output is in the high state (pulse train) or rising (sawtooth).
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setSignalDutyCycle(asynUser* pasynUser, epicsFloat64 value);

    /** \brief  [PV record getter]
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getSignalDutyCycle(asynUser* pasynUser, epicsFloat64 *value);

    // FFT functions

    /** \brief Configure size of the frame for the Fourier transform. [PV record setter]
     *
	 *  Asyn Reason: FFTSize\n
	 *  PV Record suffix: FFTN\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setFFTSize(asynUser* pasynUser, epicsFloat64 value);

    /** \brief  [PV record getter]
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getFFTSize(asynUser* pasynUser, epicsFloat64 *value);

    /** \brief Configure the amount of overlap between consecutive frames for the Fourier transform. [PV record setter]
     *
	 *  Asyn Reason:\n
	 *  PV Record suffix: FFTO \n
	 *
	 *  This function will be called when EPICS record's is read.
     */
    virtual ndsStatus setFFTOverlap(asynUser* pasynUser, epicsFloat64 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: \n
	 *  PV Record suffix: \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getFFTOverlap(asynUser* pasynUser, epicsFloat64 *value);

    /** \brief Configure the smoothing factor for averaging the Fourier transform. [PV record setter]
     *
	 *  Asyn Reason: FFTSmoothing\n
	 *  PV Record suffix: FFTS\n
	 *
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setFFTSmoothing(asynUser* pasynUser, epicsFloat64 value);

    /** \brief  [PV record getter]
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getFFTSmoothing(asynUser* pasynUser, epicsFloat64 *value);

    /** \brief The Fourier transform of the acquired signal. [PV record setter]
     *
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *
	 *  This function will be called when EPICS record's value is set.
     */
    virtual ndsStatus setFFTBuffer(asynUser* pasynUser, epicsFloat64 *inArray,  size_t nelem);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: FFTBuffer\n
	 *  PV Record suffix: FFTB\n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getFFTBuffer(asynUser* pasynUser, epicsFloat64 *outArray, size_t nelem,  size_t *nIn);

    /** \brief  [PV record setter]
	 *  Asyn Reason:
	 *  PV Record suffix:
	 *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setFFT(asynUser* pasynUser, epicsInt32 value);

    /** \brief Select the windowing function for the Fourier transform. [PV record setter]
     *
	 *  Asyn Reason: \n
	 *  PV Record suffix: FFTW\n
	 *
	 *  Possible values are:
	 *  0	NONE	No window (i.e., window[i] = 1).
	 *  1	BARLETT
	 *  2	BLACKMAN
	 *  3	FLATTOP
	 *  4	HANN
	 *  5	HAMMING
	 *  6	TUKEY
	 *  7	WELCH
	 *  	 *
	 *  This function will be called when EPICS record's value is set.
     */
    virtual ndsStatus setFFTWindow(asynUser* pasynUser, epicsInt32 value);

    /** \brief  [PV record getter]
     *
	 *  Asyn Reason: \n
	 *  PV Record suffix: \n
	 *
	 *  This function will be called when EPICS record's is read.
	 */
    virtual ndsStatus getFFTWindow(asynUser* pasynUser, epicsInt32 *value);

    /** \brief  [PV record setter]
     *  Asyn Reason: StreaminURL
     *  PV Record suffix:
     *  This function will be called when EPICS record value is set.
     */
    virtual ndsStatus setStreamingURL(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered);

    /** \brief  [PV record getter]
     *
     *  Asyn Reason: FFTBuffer\n
     *  PV Record suffix: FFTB\n
     *
     *  This function will be called when EPICS record's is read.
     */
    virtual ndsStatus getStreamingURL(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason);

    /** \brief  Notification that channel was registered.
     *
     *  If some operations requires channel number or other registration information they could be done
     *  immediately after channel is registerd buy overriding this method.
     */
    virtual void onRegistered();

    /**
     * Private function to register handlers for each node.
     */
    virtual ndsStatus propagateRegisterHandlers(PVContainers* pvContainers);

    virtual void printStructure(const std::string& prefix);

    virtual ndsStatus getFFT(asynUser* pasynUser, epicsInt32 *value);
};

}

#endif /* NDSCHANNEL_H_ */
