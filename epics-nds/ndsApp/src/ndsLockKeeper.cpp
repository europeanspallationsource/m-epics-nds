/*
 * ndsLockKeeper.cpp
 *
 *  Created on: 13. dec. 2012
 *      Author: Viacheslav Isaev, (C) Cosylab 2013, Slovenia
 */

#define UTS_MODULE_NAME "NDS:LockKeeper"

#include "ndsDebug.h"
#include "ndsLockKeeper.h"

namespace nds
{

MutexLocker::MutexLocker(epicsMutex* mutex):
		_mutex(mutex)
,_file("")
,_line(0)

{
    NDS_LOG(NDS_LVL_TRACE, _file.c_str(), _line, "", "Locking mutex");
	_mutex->lock();
}

MutexLocker::MutexLocker(epicsMutex* mutex, const std::string file, int line):
		_mutex(mutex)
	,_file(file)
	,_line(line)
{
    NDS_LOG(NDS_LVL_TRACE, _file.c_str(), _line, "", "Locking mutex");
	_mutex->lock();
}


MutexLocker::~MutexLocker()
{
    NDS_LOG(NDS_LVL_TRACE, _file.c_str(), _line, "", "Unlocking mutex");
	_mutex->unlock();
}


MutexUnlocker::MutexUnlocker(epicsMutex* mutex):
        _mutex(mutex)
,_file("")
,_line(0)

{
    NDS_LOG(NDS_LVL_TRACE, _file.c_str(), _line, "", "Locking mutex");
    _mutex->unlock();
}

MutexUnlocker::MutexUnlocker(epicsMutex* mutex, const std::string file, int line):
        _mutex(mutex)
    ,_file(file)
    ,_line(line)
{
    NDS_LOG(NDS_LVL_TRACE, _file.c_str(), _line, "", "Unlocking mutex");
    _mutex->unlock();
}


MutexUnlocker::~MutexUnlocker()
{
    NDS_LOG(NDS_LVL_TRACE, _file.c_str(), _line, "", "Locking mutex");
    _mutex->lock();
}

AsynPortLocker::AsynPortLocker( asynPortDriver *obj ):
		_obj(obj)
		,_file("")
		,_line(0)

{
    NDS_LOG(NDS_LVL_TRACE, _file.c_str(), _line, "", "Locking asynPortDriver");
	_obj->lock();
}

AsynPortLocker::AsynPortLocker( asynPortDriver *obj, const std::string file, int line):
_obj(obj)
,_file(file)
,_line(line)
{
    NDS_LOG(NDS_LVL_TRACE, _file.c_str(), _line, "", "Locking asynPortDriver");
	_obj->lock();
}

AsynPortLocker::~AsynPortLocker()
{
    NDS_LOG(NDS_LVL_TRACE, _file.c_str(), _line, "", "Unlocking asynPortDriver");
	_obj->unlock();
}

AsynPortUnLocker::AsynPortUnLocker( asynPortDriver *obj ):
		_obj(obj)
		,_file("")
		,_line(0)
{
    NDS_LOG(NDS_LVL_TRACE, _file.c_str(), _line, "", "Unlocking asynPortDriver");
	_obj->unlock();
}

AsynPortUnLocker::AsynPortUnLocker( asynPortDriver *obj, const std::string file, int line):
_obj(obj)
,_file(file)
,_line(line)
{
    NDS_LOG(NDS_LVL_TRACE, _file.c_str(), _line, "", "Unlocking asynPortDriver");
	_obj->unlock();
}

AsynPortUnLocker::~AsynPortUnLocker()
{
    NDS_LOG(NDS_LVL_TRACE, _file.c_str(), _line, "", "Locking asynPortDriver");
	_obj->lock();
}


} /* namespace ad */
