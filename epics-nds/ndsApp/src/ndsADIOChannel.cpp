/*
 * ndsADIOChannel.cpp
 *
 *  Created on: 22. avg. 2012
 *      Author: Slava Isaev (C) Cosylab. Slovenia.
 */

#include "ndsADIOChannel.h"

using namespace nds;

#define NDS_CHANNEL_CLASS ADIOChannel

ndsStatus ADIOChannel::registerHandlers(PVContainers* pvContainers)
{
	Channel::registerHandlers(pvContainers);

#define NDS_INSTANTIATE NDS_REG_SIMPL_FUNCTIONS
#define NDS_INSTANTIATE_OCTET NDS_REG_OCTET_FUNCTIONS
#define NDS_INSTANTIATE_ARRAY NDS_REG_ARRAY_FUNCTIONS
	NDS_INSTANTIATE_FOR_EACH_ADIO_CHANNEL_FUNCTION
#undef NDS_INSTANTIATE
#undef NDS_INSTANTIATE_OCTET
#undef NDS_INSTANTIATE_ARRAY

	return ndsSuccess;
}

ndsStatus ADIOChannel::bufferProcessed()
{
	return bufferFloat64Processed();
}


#define NDS_INSTANTIATE NDS_IMPLEMENT_FUNCTIONS
#define NDS_INSTANTIATE_OCTET NDS_IMPLEMENT_OCTET_FUNCTIONS
#define NDS_INSTANTIATE_ARRAY NDS_IMPLEMENT_ARRAY_FUNCTIONS
	NDS_INSTANTIATE_FOR_EACH_ADIO_CHANNEL_FUNCTION
#undef NDS_INSTANTIATE
#undef NDS_INSTANTIATE_OCTET
#undef NDS_INSTANTIATE_ARRAY

