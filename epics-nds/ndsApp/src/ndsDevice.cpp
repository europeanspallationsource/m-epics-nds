/*
 * ndsDevice.cpp

 *
 *  Created on: 22.07.2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <algorithm>
#include <macLib.h>

#include <boost/filesystem.hpp>

#include "ndsDebug.h"
#include "ndsChannelGroup.h"
#include "ndsDevice.h"
#include "ndsAsynDriver.h"
#include "ndsDeviceStateMachine.h"

using namespace nds;

DeviceStateUnknown Device::stateUnknown;
DeviceStateIOCInit Device::stateIOCInit;
DeviceStateOn      Device::stateOn;
DeviceStateOff     Device::stateOff;
DeviceStateInit    Device::stateInit;
DeviceStateDefunct Device::stateDefunct;
DeviceStateError   Device::stateError;
DeviceStateReset   Device::stateReset;

Device::Device(const std::string& name):
_firmwareVersion("N/A")
,_hardwareRevision("N/A")
,_model("N/A")
,_softwareVersion("N/A")
,_serial("N/A")
{
    _name = name;
    _portAddr = 0;
    setMachineState(DEVICE_STATE_UNKNOWN);

    // onEnter state Handlers
    registerStateNotificationHandlers();

    // onEnter state Handlers
    registerOnEnterStateHandler(nds::DEVICE_STATE_IOC_INIT,
            boost::bind(&Device::onIOCInit, this, _1, _2));

    // onEnter state Handlers
    registerOnEnterStateHandler(nds::DEVICE_STATE_OFF,
            boost::bind(&Device::onEnterOff, this, _1, _2));

    pushbackOnEnterStateHandler(nds::DEVICE_STATE_OFF,
            boost::bind(&Device::checkAutoEnable, this, _1, _2));

    pushbackOnEnterStateHandler(nds::DEVICE_STATE_ON,
            boost::bind(&nds::Device::startChannelGroups, this));

    // Registration of the handlers for the messaging mechanism
    registerMessageWriteHandler("FWUP", /// Name of the message type which will be handled by this handler
            boost::bind(&Device::handleFirmwareUpdateMsg, /// Address of the handler
                    this,                      /// Object which owns the handler
                    _1, _2) /// Stub functors (see boost::bind documentation for details.)
                    );
}

Device::~Device()
{
    NDS_STK("Device::Dtr");

	for_each(_nodes.begin(),
			 _nodes.end(),
			 &deleteMapObj<ChannelGroupContainer::value_type>
			);

	_nodes.clear();
}

void Device::destroyPrivate()
{
    NDS_TRC("Device::destroyPrivate");

	// call user destroy function
	for(ChannelGroupContainer::iterator itr =  _nodes.begin();
			 itr != _nodes.end();
			 ++itr)
	{
        NDS_TRC("Call destroy() on %s ChannelGroup.", itr->first.c_str());
	    try
	    {
	        (itr->second)->destroyPrivate();
	    }catch(...)
	    {
	        NDS_ERR("Attempt to call destroy() on %s ChannelGroup throws an error.", itr->first.c_str());
	    }
	}
	destroy();
}

ndsStatus Device::createStructurePrivate(const char* portName, const char* params)
{
    /* Parse initialization parameters. */

    MAC_HANDLE *macHandle; ///< Handle for reading the parameters.
    char **macPairs; 		///< Parameters read from st.cmd file.
    long nParams; 			///< Number of read parameters.

    macHandle = NULL;

    if(macCreateHandle(&macHandle, NULL)) {
        cantProceed("macCreateHandle not successful.");
    }

    nParams = macParseDefns(
    		macHandle,
    		params,
    		&macPairs);

    for(int i = 2 * nParams - 2; i >= 0; i -= 2)
    {
    	_params.insert(
    			ParameterContainer::value_type(macPairs[i], macPairs[i + 1]));
    }

	if (createStructure(portName, params) != ndsSuccess)
		return ndsError;

	return ndsSuccess;
}

ndsStatus Device::createStructure(const char* portName, const char* params)
{
	return ndsSuccess;
}

const std::string& Device::getStrParam(
    const std::string& param,
    const std::string& def) const
{
	ParameterContainer::const_iterator itr =  _params.find(param);
	if ( _params.end() == itr )
		return def;
	return itr->second;
}

int Device::getIntParam(
    const std::string& param,
    int def) const
{
	ParameterContainer::const_iterator itr =  _params.find(param);
	if ( _params.end() == itr )
		return def;
    return atoi(itr->second.c_str());
}

ndsStatus Device::registerChannelGroup(ChannelGroup* channelGroup)
{
	char asynPortName[255];
	snprintf(asynPortName, 255,  "%s.%s",  _name.c_str(),  channelGroup->getName().c_str()  );

	AsynDriver* driver = new AsynDriver(channelGroup, asynPortName);

	std::string name = channelGroup->getName();

	_nodes.insert(
	        ChannelGroupContainer::value_type( name, driver )
			 );

	channelGroup->_portName = _name;
	channelGroup->_device   = this;

	// ChannelGroup will operate on device port driver
	channelGroup->setAsynDriver(_portDriver); // << registering ChannelGorup on device's asyn port
    channelGroup->_portAddr =_nodes.size();

//    channelGroup->setAsynDriver(driver);    // << registering ChannelGorup on separate asyn port
//    channelGroup->_portAddr = -1;

	// Channels will operate on ChannelGroup port driver
	channelGroup->setChannelAsynDriver(driver);

	channelGroup->registerOnRequestStateHandler(
			CHANNEL_STATE_OFF,
			CHANNEL_STATE_ON,
			boost::bind(&Device::onRequestChildSwitchOn,
					this, _1, _2) );

    channelGroup->registerOnRequestStateHandler(
            CHANNEL_STATE_OFF,
            CHANNEL_STATE_PROCESSING,
            boost::bind(&Device::onRequestChildProcessing,
                    this, _1, _2) );

	return ndsSuccess;
}

template<typename T>
void outMapKey(T val)
{
	NDS_TRC(val.first.c_str());
}

ndsStatus Device::getChannelGroup(const std::string& groupName, ChannelGroup** group)
{
    ChannelGroupContainer::const_iterator itr;
	std::string name=groupName;
	std::transform(name.begin(), name.end(), name.begin(), ::tolower);
	itr = _nodes.find( name );
	if ( itr != _nodes.end() )
	{
		try
		{
			*group = dynamic_cast<ChannelGroup*>(itr->second->getBase());
			NDS_TRC("ChannelGroup: %p", *group);
			return ndsSuccess;
		}
		catch (...)
		{
			NDS_DBG("Can't cast to ChannelGroup.");
		}
	}
	else
	{
		NDS_DBG("Can't find group: '%s' ", name.c_str());
	}

	return ndsError;
}

ndsStatus Device::getChannelByName(const std::string& channelName, Channel** channel)
{
	std::string groupName;
	int idx;
	std::string::size_type found = channelName.find_first_of("0123456789");
	if (std::string::npos == found)
	{
		NDS_WRN("Wrong channel name: %s", channelName.c_str() );
		return ndsError;
	}else
	{
		NDS_TRC("Channel name: %s, idx: %ld", channelName.c_str(), found );
	}

	std::string number, group;

	number = channelName.substr(found, 3);
	group  = channelName.substr(0, found);

	NDS_TRC("Looking for: '%s' '%s'", group.c_str(), number.c_str());
	std::transform(group.begin(), group.end(), group.begin(), ::tolower);
	idx = atoi(number.c_str());

	NDS_TRC("Looking for: '%s' %d", group.c_str(), idx);
	return getChannel(group, idx, channel);
}


ndsStatus Device::getChannel(const std::string& groupName, int idx, Channel** channel)
{
	ChannelGroup* group = 0;
	if ( ndsSuccess == getChannelGroup(groupName, &group) )
	{
		NDS_TRC("ChannelGroup: %p", group);

		if ( ndsSuccess == group->getChannel(idx, channel) )
			return ndsSuccess;
		else
			NDS_DBG("Channel %d is not found in %s", idx, groupName.c_str() );
	}else
	{
		NDS_DBG("Group name is not found: %s", groupName.c_str() );
	}

	return ndsError;
}

ndsStatus Device::getFirmwareVersion(asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason)
{
    *pcount = snprintf(buf, size, "%s", _firmwareVersion.c_str());
	*eomReason = ASYN_EOM_END;
	if (*pcount > 0)
		return ndsSuccess;
	return ndsError;
}

ndsStatus Device::getHardwareRevision(asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason)
{
	*pcount = snprintf(buf, size, "%s", _hardwareRevision.c_str());
	*eomReason = ASYN_EOM_END;
	if (*pcount > 0)
		return ndsSuccess;
	return ndsError;
}

ndsStatus Device::getModel(asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason)
{
	*pcount = snprintf(buf, size, "%s", _model.c_str());
	*eomReason = ASYN_EOM_END;
	if (*pcount > 0)
		return ndsSuccess;
	return ndsError;
}

ndsStatus Device::getSerial(asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason)
{
	*pcount = snprintf(buf, size, "%s", _serial.c_str());
	*eomReason = ASYN_EOM_END;
	if (*pcount > 0)
		return ndsSuccess;
	return ndsError;
}

ndsStatus Device::getSoftwareVersion(asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason)
{
	*pcount = snprintf(buf, size, "%s", _softwareVersion.c_str());
	*eomReason = ASYN_EOM_END;
	if (*pcount > 0)
		return ndsSuccess;
	return ndsError;
}

ndsStatus Device::propagateRegisterHandlers(PVContainers* pvContainers)
{
	NDS_TRC("Device::propagateRegisterHandlers");
	this->registerHandlers(pvContainers);

	RegisterHandlerCallerMap<ChannelGroupContainer::value_type> caller(pvContainers);
	std::for_each(
			_nodes.begin(),
			_nodes.end(),
			caller );
	return ndsSuccess;
}

ndsStatus Device::registerHandlers(PVContainers* pvContainers)
{
	NDS_TRC("Device::registerHandlers");

	NDS_PV_REGISTER_OCTET("FirmwareVersion",  	&Device::setOctet, &Device::getFirmwareVersion, &_interruptIdFirmware);
	NDS_PV_REGISTER_OCTET("HardwareRevision", 	&Device::setOctet, &Device::getHardwareRevision, &_interruptIdHardware);
	NDS_PV_REGISTER_OCTET("Model",    			&Device::setOctet, &Device::getModel,    &_interruptIdModel);
	NDS_PV_REGISTER_OCTET("Serial",   			&Device::setOctet, &Device::getSerial,   &_interruptIdSerial);
	NDS_PV_REGISTER_OCTET("SoftwareVersion", 	&Device::setOctet, &Device::getSoftwareVersion, &_interruptIdSoftware);

	NDS_PV_REGISTER_OCTET("UpdateFirmware", 	&Device::setFirmware, &Device::getOctet,  &_interruptIdUpdateSoftware);

	NDS_PV_REGISTER_INT32("Power", 				&Device::setPower, &Device::getPower,     &_interruptIdPower);
	NDS_PV_REGISTER_INT32("Reset", 				&Device::setReset, &Device::getInt32,     &_interruptIdReset);
    NDS_PV_REGISTER_INT32("ErrorCode",          &Device::setInt32, &Device::getErrorCode, &_interruptIdErrorCode);

	NDS_PV_REGISTER_FLOAT64("PowerValue",  		&Device::setInt32, 	&Device::getPowerDevice, &_interruptIdPowerDevice);
	NDS_PV_REGISTER_FLOAT64("Temperature", 		&Device::setInt32,  &Device::getTemperature, &_interruptIdTemperature);

	Base::registerHandlers(pvContainers);
	return ndsSuccess;
}

ndsStatus Device::setIOCInitialization()
{
	NDS_DBG("Device::setIOCInitialization");
	_state->IOCinit(this);
//	std::for_each(
//			_nodes.begin(),
//			_nodes.end(),
//			boost::bind( &AsynDriver::setIOCInitialization,
//					boost::bind<AsynDriver*>(&ChannelGroupMap::value_type::second, _1 )
//			)
//	);
	return ndsSuccess;
}

ndsStatus Device::setIOCInitialized()
{
	NDS_DBG("Device::setIOCInitialized");
    _state->off(this);
	return ndsSuccess;
}

ndsStatus Device::defunct()
{
	return _state->fault(this);
}

DeviceBaseState* Device::getStateObject(DeviceStates requestedState)
{
	switch (requestedState)
	{
	case DEVICE_STATE_IOC_INIT:
		return &stateIOCInit;
	case DEVICE_STATE_INIT:
		return &stateInit;
	case DEVICE_STATE_OFF:
		return &stateOff;
	case DEVICE_STATE_ON:
		return &stateOn;
	case DEVICE_STATE_ERROR:
		return &stateError;
	case DEVICE_STATE_DEFUNCT:
		return &stateDefunct;
    case DEVICE_STATE_RESETTING:
        return &stateReset;
	case DEVICE_STATE_UNKNOWN:
	default:
		return &stateUnknown;
	}
}

void Device::setMachineState(DeviceStates requestedState)
{
	_state = getStateObject(requestedState);
}

ndsStatus Device::reset()
{
    return _state->reset(this);
}

ndsStatus Device::on()
{
	return _state->on(this);
}

ndsStatus Device::off()
{
	return _state->off(this);
}

ndsStatus Device::error()
{
	return _state->error(this);
}

ndsStatus Device::writeState (asynUser *pasynUser, epicsInt32 value)
{
	NDS_TRC("Device::writeState");
	return ndsSuccess;
}

ndsStatus Device::readState (asynUser *pasynUser, epicsInt32 *value)
{
	NDS_TRC("Device::readState");
	*value = _state->getState();
	return ndsSuccess;
}

ndsStatus Device::stateChangeHandler(nds::DeviceStates, nds::DeviceStates)
{
	NDS_STK("Device::stateChangeHandler");
	doCallbacksInt32(_state->getState(), _interruptIdState, _portAddr);
	return ndsSuccess;
}

ndsStatus Device::onRequestChildProcessing(nds::ChannelStates, nds::ChannelStates requestedState)
{
    NDS_TRC("Device::onRequestChildProcessing");
    if ( requestedState == CHANNEL_STATE_PROCESSING )
    {
        if ( _state->getState() != DEVICE_STATE_ON )
        {
            NDS_WRN("Device should be switched ON before Channels or ChannelGroups.");
            return ndsBlockTransition;
        }
    }
    return ndsSuccess;
}

ndsStatus Device::onRequestChildSwitchOn(nds::ChannelStates, nds::ChannelStates requestedState)
{
	NDS_TRC("Device::onChildSwitchOn");
	if ( requestedState == CHANNEL_STATE_ON )
	{
		if ( _state->getState() != DEVICE_STATE_ON )
		{
			NDS_WRN("Device should be switched ON before Channels or ChannelGroups.");
			return ndsBlockTransition;
		}
	}
	return ndsSuccess;
}

ndsStatus Device::getPowerDevice(asynUser *pasynUser, epicsFloat64* value)
{
    if ( getCurrentState() == nds::DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus Device::getTemperature(asynUser *pasynUser, epicsFloat64* value)
{
    if ( getCurrentState() == nds::DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus Device::getPower(asynUser *pasynUser, epicsInt32* value)
{
    if ( getCurrentState() == nds::DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
	return ndsSuccess;
}

ndsStatus Device::setPower(asynUser  *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus Device::setReset(asynUser  *pasynUser, epicsInt32 value)
{
	return ndsSuccess;
}

ndsStatus Device::setFirmware(asynUser *pasynUser, const char *data, size_t numchars, size_t *nbytesTransfered)
{
	NDS_DBG("setFirmware");
	std::string metaFile(data);
	*nbytesTransfered = numchars;
	try
	{
		parseFirmwareMetafile(metaFile);
	}catch(...)
	{
		NDS_CRT("Firmware update fails with exception.");
		updateMessage("FWUP", 1,"Firmware update fails.");
	}
	return ndsSuccess;
}

ndsStatus Device::checkFirmwareCompatibility(const Target& target)
{
	if ( !_model.compare(target.device) )
	{
		if ( std::find(target.hardware.begin(), target.hardware.end(), _hardwareRevision) != target.hardware.end() )
		{
			if ( std::find(target.firmware.begin(), target.firmware.end(), _firmwareVersion) != target.firmware.end() )
				return ndsSuccess;
			else
				NDS_DBG("Firmware revision doesn't match. %s", _softwareVersion.c_str() );
		}
		else
		{
			NDS_DBG("Device revision doesn't match. %s", _hardwareRevision.c_str() );
		}
	}else
	{
		NDS_INF("Device model doesn't match. Device: %s Requested: %s", _model.c_str(), target.device.c_str() );
	}
	return ndsError;
}

ndsStatus Device::updateFirmware(const std::string& module, const std::string& image, const std::string& method)
{
	NDS_DBG("updateFirmware: %s %s %s", module.c_str(), image.c_str(), method.c_str());
	return ndsSuccess;
}

int Device::parseFirmware(xmlDocPtr doc, xmlNodePtr cur)
{
	NDS_DBG("Parsing firmware: %s", cur->name);
	Firmware firmware;
	firmware.loadXML(doc, cur);
	ndsStatus updateStatus=ndsError;
	bool firmwareFound=false;
	bool errorReported=false;
	int result = -1;

    cur = cur->children;
    while (NULL != cur)
    {
    	NDS_DBG("Children: %s", (char*)cur->name);
    	if ( xmlStrEqual(cur->name, (const xmlChar *)"target") )
    	{
    		Target target;
    		target.loadXML(doc, cur);
    		if ( ndsSuccess == checkFirmwareCompatibility(target) )
    		{
    			firmwareFound=true;
    			NDS_DBG("Firmware compatible");

    			if ( ndsSuccess == firmware.downloadImage())
    			{
					if ( boost::filesystem::exists(firmware.imageAbsPath) )
					{
						NDS_DBG("Image downloaded");
						if ( ndsSuccess == firmware.checkSHA() )
						{
							NDS_DBG("Firmware updating: %s %s %s", target.module.c_str(), firmware.imageAbsPath.c_str(), target.update_method.c_str());
							try
							{
								updateStatus = updateFirmware(target.module, firmware.imageAbsPath, target.update_method );
							}catch(...)
							{
								NDS_ERR("Firmware update fails.");
								updateStatus = ndsError;
							}
						}else
						{
							NDS_ERR("SHA-1 doesn't match.");
							errorReported=true;
							result = 6;
							updateMessage("FWUP", "SHA-1 doesn't match.", 6);
						}
					}else
					{
						NDS_ERR("Firmware image-file '%s' not found.", firmware.imageAbsPath.c_str());
						errorReported=true;
						result = 5;
						updateMessage("FWUP", 5, "Firmware image-file '%s' not found.", firmware.imageAbsPath.c_str());
					}
    			}else
    			{
    				NDS_ERR("Firmware image '%s' couldn't be downloaded.", firmware.image.c_str());
					errorReported=true;
					result = 4;
					updateMessage("FWUP", 4, "Firmware image '%s' couldn't be downloaded.", firmware.image.c_str());
    			}

    			if (firmware.localFileCreated)
    				boost::filesystem::remove(firmware.imageAbsPath);

    		}else
    		{
				NDS_DBG("Firmware isn't compatible.");
    		}
    	}
    	cur = cur->next;
    }

    if ( ndsSuccess == updateStatus )
    {
		result = 0;
    	updateMessage("FWUP", "Firmware update completed.", 0);
    }
    else if ( !firmwareFound )
    {
		result = 0;
    	updateMessage("FWUP", "No matching firmware found. ", 2);
    }
    else if ( !errorReported )
    {
		result = 0;
    	updateMessage("FWUP", "Firmware update failed.", 3);
    }

    return result;
}

ndsStatus Device::parseFirmwareMetafile(const std::string& fileName)
{
	NDS_DBG("parseFirmwareMetafile %s", fileName.c_str());
    xmlDocPtr doc;
    xmlNodePtr cur;
    nds::Firmware firmware;

	updateMessage("FWUP", "Firmware update in progress.", 1);

    if ( fileName.empty() )
    {
    	NDS_ERR("Firmware meta-file '%s' not found.", fileName.c_str());
    	updateMessage("FWUP", 5, "Firmware meta-file '%s' not found.", fileName.c_str() );
    	return ndsError;
    }

    if ( !boost::filesystem::exists(fileName) )
    {
    	NDS_ERR("Firmware meta-file '%s' not found.", fileName.c_str());
    	updateMessage("FWUP", 5, "Firmware meta-file '%s' not found.", fileName.c_str() );
    	return ndsError;
    }

    doc = xmlParseFile(fileName.c_str());
    if (doc == NULL )
    {
    	NDS_ERR("Meta-file couldn't be parsed.");
    	updateMessage("FWUP", 3, "Firmware update failed.");
    	return ndsError;
    }

    cur = xmlDocGetRootElement(doc);

    if (cur == NULL)
    {
    	NDS_ERR("Meta-file is empty.");
    	updateMessage("FWUP", 3, "Firmware update failed.");
    	xmlFreeDoc(doc);
		return ndsError;
	}

	NDS_DBG("Meta file root: %s", cur->name);

	if ( !xmlStrEqual(cur->name, (const xmlChar *) "firmwares"))
	{
		NDS_ERR("Meta-file has wrong format, root node != firmwares");
    	updateMessage("FWUP", 3, "Firmware update failed.");
		xmlFreeDoc(doc);
		return ndsError;
	}

	NDS_DBG("Meta file is ready to be parsed.");

    cur = cur->children;
    while ( cur != NULL )
    {
    	NDS_DBG("Firmwares children: %s", cur->name);
    	if ( xmlStrEqual(cur->name, (const xmlChar *)"firmware") )
    		parseFirmware(doc, cur);
    	cur = cur->next;
    }

    xmlFreeDoc(doc);
    return ndsSuccess;
}

ndsStatus Device::handleFirmwareUpdateMsg(asynUser *pasynUser, const Message& value)
{
	Message response;
	response.messageType = "FWUP";

	// File name is passed as argument 'URL' of the message type.
	ndsStatus res = parseFirmwareMetafile( value.getStrParam("URL", "") );

	if (ndsError == res)
	{
		response.insert("CODE", "-1");
	}else
	{
		response.insert("CODE", "0");
	}
	doCallbacksMessage(response);
	return res;
}

DeviceStates Device::getCurrentState()
{
    return _state->getState();
}

ndsStatus Device::getErrorCode(asynUser  *pasynUser, epicsInt32* value)
{
    *value = 0;
    return ndsSuccess;
}

void Device::registerStateNotificationHandlers()
{
    // onEnter state Handlers
    registerOnEnterStateHandler(nds::DEVICE_STATE_UNKNOWN,
            boost::bind(&Device::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(nds::DEVICE_STATE_IOC_INIT,
            boost::bind(&Device::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(nds::DEVICE_STATE_INIT,
            boost::bind(&Device::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(nds::DEVICE_STATE_OFF,
            boost::bind(&Device::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(nds::DEVICE_STATE_ON,
            boost::bind(&Device::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(nds::DEVICE_STATE_ERROR,
            boost::bind(&Device::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(nds::DEVICE_STATE_DEFUNCT,
            boost::bind(&Device::stateChangeHandler, this, _1, _2));
    registerOnEnterStateHandler(nds::DEVICE_STATE_RESETTING,
            boost::bind(&Device::stateChangeHandler, this, _1, _2));
}

ndsStatus Device::getInt32(asynUser* pasynUser, epicsInt32* value)
{
    if ( getCurrentState() == DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getInt32(pasynUser, value);
}

ndsStatus Device::getFloat64(asynUser* pasynUser, epicsFloat64* value)
{
    if ( getCurrentState() == DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getFloat64(pasynUser, value);
}

ndsStatus Device::getOctet(asynUser *pasynUser, char *data, size_t maxchars, size_t *nbytesTransfered, int *eomReason)
{
    if ( getCurrentState() == DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getOctet(pasynUser, data, maxchars, nbytesTransfered, eomReason);
}

ndsStatus Device::getInt8Array(asynUser* pasynUser, epicsInt8 *outArray, size_t nelem,  size_t *nIn)
{
    if ( getCurrentState() == DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getInt8Array(pasynUser, outArray, nelem,  nIn) ;
}

ndsStatus Device::getInt16Array(asynUser* pasynUser, epicsInt16 *outArray, size_t nelem,  size_t *nIn)
{
    if ( getCurrentState() == DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getInt16Array(pasynUser, outArray, nelem,  nIn) ;
}

ndsStatus Device::getInt32Array(asynUser* pasynUser, epicsInt32 *outArray, size_t nelem,  size_t *nIn)
{
    if ( getCurrentState() == DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base:: getInt32Array(pasynUser, outArray, nelem, nIn);
}

ndsStatus Device::getFloat32Array(asynUser* pasynUser, epicsFloat32 *outArray, size_t nelem,  size_t *nIn)
{
    if ( getCurrentState() == DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getFloat32Array(pasynUser, outArray, nelem, nIn) ;
}

ndsStatus Device::getFloat64Array(asynUser* pasynUser, epicsFloat64 *outArray, size_t nelem,  size_t *nIn)
{
    if ( getCurrentState() == DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getFloat64Array(pasynUser, outArray, nelem,  nIn);
}

ndsStatus Device::getUInt32Digital(asynUser *pasynUser, epicsUInt32 *value, epicsUInt32 mask)
{
    if ( getCurrentState() == DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getUInt32Digital(pasynUser, value, mask);
}

DeviceBaseState* Device::getCurrentStateObj()
{
    return _state;
}

ndsStatus Device::onIOCInit(nds::DeviceStates, nds::DeviceStates)
{
    for( ChannelGroupContainer::iterator itr = _nodes.begin();
            itr != _nodes.end();
            ++itr )
    {
        itr->second->getBase()->setIOCInitialization();
    }

    return ndsSuccess;
}

ndsStatus Device::onEnterOff(nds::DeviceStates prev, nds::DeviceStates)
{
    for( ChannelGroupContainer::iterator itr = _nodes.begin();
            itr != _nodes.end();
            ++itr )
    {
        itr->second->getBase()->off();
    }
    return ndsSuccess;
}

ndsStatus Device::checkAutoEnable(nds::DeviceStates prev, nds::DeviceStates)
{
    if ( this->isEnabled() && DEVICE_STATE_IOC_INIT == prev)
        on();
}

ndsStatus Device::switchChannelGroupsOn()
{
    for( ChannelGroupContainer::iterator itr = _nodes.begin();
            itr != _nodes.end();
            ++itr )
    {
            itr->second->getBase()->on();
    }
    return ndsSuccess;
}

ndsStatus Device::startChannelGroups()
{
    for( ChannelGroupContainer::iterator itr = _nodes.begin();
            itr != _nodes.end();
            ++itr )
    {
        if (itr->second->getBase()->isEnabled())
        try
        {
            nds::ChannelGroup *gr = dynamic_cast<nds::ChannelGroup *>(itr->second->getBase());
            gr->start();
        }catch(...)
        {

        }
    }
    return ndsSuccess;
}

ndsStatus Device::switchChannelGroupsOff()
{
    for( ChannelGroupContainer::iterator itr = _nodes.begin();
            itr != _nodes.end();
            ++itr)
    {
        itr->second->getBase()->off();
    }
    return ndsSuccess;
}

ndsStatus Device::registerOnRequestStateHandlerForAll(nds::DeviceStates requestedState,
        StateHandler handler)
{
    for(int i=DEVICE_STATE_OFF; i<DEVICE_STATE_last; ++i)
    {
        registerOnRequestStateHandler( (nds::DeviceStates) i,  requestedState,
                handler);
    }
    return ndsSuccess;
}

ndsStatus Device::setEnabled(asynUser* pasynUser, epicsInt32 value)
{
    return Base::setEnabled(pasynUser, value);
}

ndsStatus Device::getEnabled(asynUser* pasynUser, epicsInt32 *value)
{
    if ( getCurrentState() == DEVICE_STATE_IOC_INIT )
    {
        // Prevent overwriting of VAL value on initialization stage
        return ndsError;
    }
    return Base::getEnabled(pasynUser, value);
}

void Device::printStructure(const std::string& prefix)
{
    PRINT("Device: %s State: %s\n", _name.c_str(), DeviceBaseState::toString(this->getCurrentState()).c_str() );
    for( ChannelGroupContainer::iterator itr = _nodes.begin();
            itr != _nodes.end();
            ++itr)
    {
        itr->second->getBase()->printStructure(prefix+"\t");
    }
}

void  Device::enableFastInit()
{
    pushbackOnEnterStateHandler(nds::DEVICE_STATE_INIT,
            boost::bind(&Device::on, this ) );
}
