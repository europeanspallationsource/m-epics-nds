/*
 * ndsThreadManager.cpp
 *
 *  Created on: 23.08.2013
 *      Author: Slava
 */

#include <sstream>
#include <boost/bind.hpp>

#include "ndsDebug.h"

#include "map_functors.h"
#include "ndsTaskManager.h"

namespace nds
{

template<typename T>
void printTaskInfo(const T& obj)
{
    printf("%s: %s\n", obj.first.c_str(), obj.second->getStateStr().c_str() );
};

template<typename T>
class MapSecondCompare
{
public:
    TaskPtr _sample;
    MapSecondCompare (TaskPtr sample):
        _sample(sample)
    {
    }

    bool operator ()(T obj)
    {
        return  obj.second == _sample;
    }
};

TaskManagerBase::TaskManagerBase():
    _queue(epicsTimerQueueActive::allocate(true))
{
}

TaskManagerBase::~TaskManagerBase()
{
    std::for_each(
       _nodes.begin(),
       _nodes.end(),
       &deleteMapObj<TasksContainer::value_type>
    );

    _nodes.clear();

    _queue.release();
}

void TaskManagerBase::stopAll()
{
    std::for_each(
       _nodes.begin(),
       _nodes.end(),
       boost::bind(&BaseTask::cancel,
               boost::bind<TaskPtr>(&TasksContainer::value_type::second, _1)  )
    );
}

void TaskManagerBase::registerTask(const std::string& name, TaskPtr ptr)
{
    if (isTaskExists( name ) )
        NDS_CRT("Task '%s' exists.", name.c_str());
    _nodes.insert(
            TasksContainer::value_type(name, ptr));
}

ndsStatus TaskManagerBase::findTask(const std::string& name, TaskPtr* ptr)
{
    TasksContainer::iterator itr = _nodes.find(name);
    if (  _nodes.end() != itr )
    {
        *ptr = itr->second;
        return ndsSuccess;
    }
    return ndsError;
}

bool TaskManagerBase::isTaskExists(const std::string& name)
{
    TaskPtr ptr;
    return findTask(name, &ptr) == ndsSuccess;
}

epicsTimerQueue& TaskManagerBase::getQueue()
{
    return _queue;
}

void TaskManagerBase::listTasks()
{
    std::for_each(
       _nodes.begin(),
       _nodes.end(),
       &printTaskInfo<TasksContainer::value_type>
    );
}

void TaskManagerBase::startTask(const std::string& name)
{
    NDS_WRN("Manual task '%s' starting!", name.c_str());
    TasksContainer::iterator itr = _nodes.find(name);
    if (  _nodes.end() != itr )
    {
        itr->second->start();
        NDS_INF("Task '%s' started!", name.c_str());
        return;
    }
    NDS_ERR("Task '%s' is not found!", name.c_str());
}

void TaskManagerBase::cancelTask(const std::string& name)
{
    NDS_WRN("Manual task '%s' cancelation!", name.c_str());
    TasksContainer::iterator itr = _nodes.find(name);
    if (  _nodes.end() != itr )
    {
        itr->second->cancel();
        NDS_INF("Task '%s' cancelled!", name.c_str());
        return;
    }
    NDS_ERR("Task '%s' is not found!", name.c_str());
}

std::string TaskManagerBase::generateName()
{
    return TaskManager::getInstance().generateNamePrv();
}

std::string TaskManagerBase::generateName(const std::string& str)
{
    return TaskManager::getInstance().generateNamePrv(str);
}

std::string TaskManagerBase::generateNamePrv()
{
    std::stringstream ss (std::stringstream::in | std::stringstream::out);
    ss << _nodes.size();
    return ss.str();
}

std::string TaskManagerBase::generateNamePrv(const std::string& str)
{
    std::stringstream ss (std::stringstream::in | std::stringstream::out);
    ss << str << "_"<< _nodes.size();
    return ss.str();
}

void TaskManagerBase::deleteTask(const std::string& name)
{
    TasksContainer::iterator itr = _nodes.find(name);
    if (  _nodes.end() != itr )
    {
        _nodes.erase(itr);
    }
}

void TaskManagerBase::deleteTask(TaskPtr task)
{
    MapSecondCompare<TasksContainer::value_type> comparator(task);

    TasksContainer::iterator itr = std::find_if (
            _nodes.begin(),
            _nodes.end(),
            comparator);

    if (  _nodes.end() != itr )
    {
        _nodes.erase(itr);
    }
}

} /* namespace nds */
