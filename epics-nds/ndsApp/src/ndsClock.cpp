/*
 * ndsClock.cpp
 *
 *  Created on: 18. apr. 2013
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include "ndsClock.h"

namespace nds
{

Clock::Clock()
{
    // TODO Auto-generated constructor stub

}

Clock::~Clock()
{
    // TODO Auto-generated destructor stub
}

ndsStatus Clock::registerHandlers(nds::PVContainers* pvContainers)
{
    nds::Pulse::registerHandlers(pvContainers);

    NDS_PV_REGISTER_FLOAT64("DutyCycle", &Clock::setDutyCycle, &Clock::getDutyCycle, &_interruptIdDutyCycle);
    NDS_PV_REGISTER_FLOAT64("TimeEnd", &Clock::setTimeEnd, &Clock::getTimeEnd, &_interruptIdTimeEnd);

    return ndsSuccess;
}

ndsStatus Clock::setDutyCycle(asynUser* pasynUser, epicsFloat64 value)
{
    _DutyCycle = value;
    return ndsSuccess;
}

ndsStatus Clock::getDutyCycle(asynUser* pasynUser, epicsFloat64* value)
{
    *value = _DutyCycle;
    return ndsSuccess;
}

ndsStatus Clock::setTimeEnd(asynUser* pasynUser, epicsFloat64 value)
{
    _TimeEnd=value;
    return ndsSuccess;
}

ndsStatus Clock::getTimeEnd(asynUser* pasynUser, epicsFloat64* value)
{
    *value = _TimeEnd;
    return ndsSuccess;
}


} /* namespace nds */
