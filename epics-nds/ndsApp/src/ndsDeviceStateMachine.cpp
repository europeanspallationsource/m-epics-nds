/*
 * ndsDeviceStateMachine.cpp
 *
 *  Created on: 21. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#include <sstream>
#include <string>

#include "ndsDeviceStates.h"
#include "ndsDeviceStateMachine.h"

using namespace nds;

const std::string DeviceBaseState::toString(const DeviceStates& state)
{
	switch (state)
	{
		case DEVICE_STATE_IOC_INIT:
			return "IOC_INIT";
		case DEVICE_STATE_INIT:
			return "INIT";
		case DEVICE_STATE_OFF:
			return "OFF";
		case DEVICE_STATE_ON:
			return "ON";
		case DEVICE_STATE_ERROR:
			return "ERROR";
		case DEVICE_STATE_DEFUNCT:
			return "FAULT";
        case DEVICE_STATE_RESETTING:
            return "RESETTING";
		case DEVICE_STATE_UNKNOWN:
		default:
			return "UNKNOWN";
	}
}

