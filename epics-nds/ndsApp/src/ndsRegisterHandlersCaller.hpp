/*
 * ndsRegisterHandlersCaller.hpp
 *
 *  Created on: 2. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSREGISTERHANDLERSCALLER_HPP_
#define NDSREGISTERHANDLERSCALLER_HPP_

namespace nds
{

template<typename T>
struct RegisterHandlerCaller
{
	PVContainers* _pvContainers;
	RegisterHandlerCaller(PVContainers* pvContainers):_pvContainers(pvContainers){}
	void operator ()(T obj)
	{
		obj.second->registerHandlers(_pvContainers);
	}
};

}


#endif /* NDSREGISTERHANDLERSCALLER_HPP_ */
