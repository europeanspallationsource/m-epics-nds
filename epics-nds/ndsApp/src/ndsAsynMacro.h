/*
 * ndsAsynMacro.h
 *
 *  Created on: 22.07.2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef NDSASYNMACRO_H_
#define NDSASYNMACRO_H_

/// @cond NDS_INTERNALS

#define NDS_DEFINE_ASYN_SIMPLE_HANDLERS(asynType, epicsType) \
virtual asynStatus asynType##Write(void *drvPvt, asynUser *pasynUser, epicsType value);\
virtual asynStatus asynType##Read(void *drvPvt, asynUser *pasynUser, epicsType *value);

#define NDS_DEFINE_ASYN_ARRAY_HANDLERS(asynType, epicsType) \
virtual asynStatus asynType##Write(void *drvPvt, asynUser *pasynUser, epicsType *value, size_t nelements);\
virtual asynStatus asynType##Read(void *drvPvt, asynUser *pasynUser, epicsType *value, size_t nelements, size_t *nIn);

#define NDS_IMPL_ASYN_SIMPLE_HANDLERS(Type)\
static asynStatus asyn##Type##Write(void *drvPvt, asynUser *pasynUser, epics##Type value)\
{\
  return nds::Manager::getInstance().asyn##Type##Write(drvPvt, pasynUser, value);\
}\
static asynStatus asyn##Type##Read(void *drvPvt, asynUser *pasynUser, epics##Type *value)\
{\
  return nds::Manager::getInstance().asyn##Type##Read(drvPvt, pasynUser, value);\
}

#define NDS_IMPL_ASYN_ARRAY_HANDLERS(Type)\
static asynStatus asyn##Type##ArrayWrite(void *drvPvt, asynUser *pasynUser, epics##Type *value, size_t nelements)\
{\
  return nds::Manager::getInstance().asyn##Type##ArrayWrite(drvPvt, pasynUser, value, nelements);\
}\
asynStatus asyn##Type##ArrayRead(void *drvPvt, asynUser *pasynUser, epics##Type *value, size_t nelements, size_t *nIn)\
{\
  return nds::Manager::getInstance().asyn##Type##ArrayRead(drvPvt, pasynUser, value, nelements, nIn);\
}

#define NDS_IMPL_MNGR_SIMPLE_HANDLERS(Type)\
asynStatus ManagerBase::asyn##Type##Write(void *drvPvt, asynUser *pasynUser, epics##Type value)\
{\
  return asynSuccess;\
}\
asynStatus ManagerBase::asyn##Type##Read(void *drvPvt, asynUser *pasynUser, epics##Type *value)\
{\
  return asynSuccess;\
}

#define NDS_IMPL_MNGR_ARRAY_HANDLERS(type)\
asynStatus ManagerBase::asyn##type##ArrayWrite(void *drvPvt, asynUser *pasynUser, epics##type *value, size_t nelements)\
{\
  return asynSuccess;\
}\
asynStatus ManagerBase::asyn##type##ArrayRead(void *drvPvt, asynUser *pasynUser, epics##type *value, size_t nelements, size_t *nIn)\
{\
  return asynSuccess;\
}

#define NDS_IMPL_NDS_SIMPLE_HANDLERS(Type)\
static asynStatus asyn##Type##Write(void *drvPvt, asynUser *pasynUser, epics##Type value)\
{\
  return nds::Manager::getInstance().asyn##Type##Write(drvPvt, pasynUser, value);\
}\
static asynStatus asyn##Type##Read(void *drvPvt, asynUser *pasynUser, epics##Type *value)\
{\
  return nds::Manager::getInstance().asyn##Type##Read(drvPvt, pasynUser, value);\
}

#define NDS_IMPL_NDS_ARRAY_HANDLERS(Type)\
static asynStatus asyn##Type##ArrayWrite(void *drvPvt, asynUser *pasynUser, epics##Type *value, size_t nelements)\
{\
  return nds::Manager::getInstance().asyn##Type##ArrayWrite(drvPvt, pasynUser, value, nelements);\
}\
asynStatus asyn##Type##ArrayRead(void *drvPvt, asynUser *pasynUser, epics##Type *value, size_t nelements, size_t *nIn)\
{\
  return nds::Manager::getInstance().asyn##Type##ArrayRead(drvPvt, pasynUser, value, nelements, nIn);\
}

#define FILL_ASYN_INTERFACE(asynType)\
static asynType asynType##Functions = { asynType##Write, asynType##Read };

#define NDS_IMPL_REGISTRATION_FUNCTION(type) \
template<class Owner>\
ndsStatus register##type(int portAddr, const std::string& reason, Owner* obj,\
typename PV##type::template Handlers<Owner>::owner_writer_type writer,\
typename PV##type::template Handlers<Owner>::owner_reader_type reader,\
const std::string& writerName,\
const std::string& reasonName,\
int* id )\
{\
	PV##type* pv = PV##type::create<Owner>(portAddr, reason, obj,\
			writer, \
			reader, \
			writerName,\
			reasonName,\
			id);\
	ndsStatus result = Loki::Field<asyn##type>(this->_handlerKeeper).reg( pv );\
	if (ndsSuccess ==  result)\
	{\
			NDS_TRC("register"#type" '%s' - Success, code: %d", reason.c_str(), pv->getReasonCode());\
		return registerReasonOnOwner(pv->getPortAddr(), pv->getReasonStr(), pv->getReasonCode());\
	}else\
	{\
			NDS_TRC("register"#type" '%s' - Fails", reason.c_str());\
	}\
	return ndsError;\
}

#define NDS_IMPL_CLEAR_FUNCTION(type) \
    Loki::Field<asyn##type>(this->_handlerKeeper).clear();

#define NDS_INSTANTIATE_FOR_EACH_TYPE \
    NDS_INSTANTIATE(Octet) \
    NDS_INSTANTIATE(Int32) \
    NDS_INSTANTIATE(Float64) \
    NDS_INSTANTIATE(Int8Array) \
    NDS_INSTANTIATE(Int16Array) \
    NDS_INSTANTIATE(Int32Array) \
    NDS_INSTANTIATE(Float32Array) \
    NDS_INSTANTIATE(Float64Array) \
    NDS_INSTANTIATE(UInt32Digital) \
    NDS_INSTANTIATE(GenericPointer)


#define DEFINE_PORT_SIMPLE_CALLBACKS(Type) \
virtual asynStatus doCallbacks##Type(epics##Type value, int reason, int addr, ndsStatus status); \
virtual asynStatus doCallbacks##Type(epics##Type value, int reason, int addr, ndsStatus status, epicsTimeStamp timestamp); \

/// @endcond


#endif /* NDSASYNMACRO_H_ */
