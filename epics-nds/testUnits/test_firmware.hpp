/*
 * test_adiochannel.hpp
 *
 *  Created on: 5. okt. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef TEST_ADIOCHANNEL_HPP_
#define TEST_ADIOCHANNEL_HPP_

#include <boost/filesystem.hpp>
#include "ndsDevice.h"
#include "ndsFirmware.h"

using namespace nds;

class TestDevice : public nds::Device
{
	int _code;
	std::vector<int> _states;
public:
	friend class FirmwareUnitTestSuite;

	TestDevice(const std::string& name):nds::Device(name)
	,_code(-1)
	{
		_firmwareVersion = "2.2.2";
		_hardwareRevision = "rev0";
		_model = "NI6529";
		_states.push_back(_code);
	}

	virtual ndsStatus updateFirmware(const std::string& module, const std::string& image, const std::string& method)
	{
		NDS_INF("Updating hardware. Returning Success.");
		return ndsSuccess;
	}

	virtual ndsStatus updateMessage(const std::string& type, const std::string& text, int code )
	{
		_code = code;
		_states.push_back(code);
		return ndsSuccess;
	}
};

class TestDeviceUpdateError : public nds::Device
{
	int _code;
	std::vector<int> _states;
public:
	friend class FirmwareUnitTestSuite;

	TestDeviceUpdateError(const std::string& name):nds::Device(name)
	,_code(-1)
	{
		_firmwareVersion = "2.2.2";
		_hardwareRevision = "rev0";
		_model = "NI6529";
		_states.push_back(_code);
	}

	virtual ndsStatus updateFirmware(const std::string& module, const std::string& image, const std::string& method)
	{
		NDS_INF("Updating hardware. Returning Error");
		return ndsError;
	}

	virtual ndsStatus updateMessage(const std::string& type, const std::string& text, int code )
	{
		_code = code;
		_states.push_back(code);
		return ndsSuccess;
	}
};

class TestDeviceChain : public nds::Device
{
	int _counter;
	std::string _firmwares[4];
	int _code;

	std::vector<int> _states;
public:
	friend class FirmwareUnitTestSuite;

	TestDeviceChain(const std::string& name):
		nds::Device(name)
		,_counter(0)
		,_code(-1)
	{
		_firmwares[0]="2.2.0";
		_firmwares[1]="2.2.1";
		_firmwares[2]="2.2.2";
		_firmwares[3]="2.3.1";

		_hardwareRevision = "rev0";
		_model = "NI6529";

		_firmwareVersion = _firmwares[_counter];

		_states.push_back(_code);
	}

	virtual ndsStatus updateFirmware(const std::string& module, const std::string& image, const std::string& method)
	{
		TS_TRACE( "Updating firmware." );
		TS_TRACE( _firmwareVersion.c_str() );
		TS_TRACE( _counter );

		_counter ++;
		_firmwareVersion = _firmwares[_counter];

		if ( !boost::filesystem::exists(image) )
			return ndsError;

		return ndsSuccess;
	}

	virtual ndsStatus updateMessage(const std::string& type, const std::string& text, int code )
	{
		_code = code;
		_states.push_back(_code);
		return ndsSuccess;
	}

};

class FirmwareUnitTestSuite : public CxxTest::TestSuite
{
  public:

	void testOne()
	{

	}

	/* TEST CASES */
	void testCheckCompatibilityTrue()
	{
		TestDevice device("");
		Target target;

		target.device   = "NI6529";
		target.hardware.push_back("rev0");
		target.firmware.push_back("2.2.2");

		TSM_ASSERT_EQUALS("", device.checkFirmwareCompatibility(target), ndsSuccess);
	}

	void testCheckCompatibilityWrongDevice()
	{
		TestDevice device("");
		Target target;

		target.device   = "pxi6682";
		target.hardware.push_back("rev0");
		target.firmware.push_back("2.2.2");

		TSM_ASSERT_EQUALS("", device.checkFirmwareCompatibility(target), ndsError);
	}

	void testCheckCompatibilityFalse()
	{
		TestDevice device("");
		Target target;

		target.device   = "NI6529";
		target.hardware.push_back("rev0");
		target.firmware.push_back("2.1.1");

		TSM_ASSERT_EQUALS("", device.checkFirmwareCompatibility(target), ndsError);
	}

	void testLocalfile()
	{
		TestDevice device("");
		TSM_ASSERT_EQUALS("Can't parse firmware meta file", device.parseFirmwareMetafile("local.xml"), ndsSuccess );
	}

//	void testBadHttpURL()
//	{
//		TestDevice device("");
//		TSM_ASSERT_EQUALS("Can't parse firmware meta file", device.parseFirmwareMetafile("error.xml"), ndsSuccess );
//
//		TSM_ASSERT_EQUALS("Progress code doesn't match.", device._code, 4);
//	}

	void testParseFirmwareSimple()
	{
		TestDevice device("");
		TSM_ASSERT_EQUALS("Can't parse firmware meta file", device.parseFirmwareMetafile("simple.xml"), ndsSuccess );

		int states[]={-1, 1, 0};
		TSM_ASSERT_EQUALS("Update state sequence is wrong", std::equal(device._states.begin(), device._states.end(), states), true);
		TSM_ASSERT_EQUALS("Progress code doesn't match.", device._code, 0);
	}

	void testParseFirmwareChainUpdate()
	{
		TestDeviceChain device("");
		TSM_ASSERT_EQUALS("Can't parse firmware meta file", device.parseFirmwareMetafile("chain.xml"), ndsSuccess );
		TS_TRACE(device._firmwareVersion.c_str());
		TSM_ASSERT_EQUALS("Expected version is not loaded", device._firmwareVersion.compare("2.3.1"), 0 );

		int states[]={-1, 1, 0, 0, 0};
		TSM_ASSERT_EQUALS("Update state sequence is wrong", std::equal(device._states.begin(), device._states.end(), states), true);
		if (!std::equal(device._states.begin(), device._states.end(), states) )
			for( std::vector<int>::const_iterator i = device._states.begin(); i != device._states.end(); ++i)
				TS_TRACE( *i );

		TSM_ASSERT_EQUALS("Progress code doesn't match.", device._code, 0);
	}
};



#endif /* TEST_ADIOCHANNEL_HPP_ */
