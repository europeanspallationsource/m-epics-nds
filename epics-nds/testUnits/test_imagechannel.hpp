/*
 * test_imagechannel.hpp
 *
 *  Created on: 5. okt. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef TEST_IMAGECHANNEL_HPP_
#define TEST_IMAGECHANNEL_HPP_

#include "ndsImageChannel.h"
#include "test_tools.h"

class ImageChannelUnitTest: public nds::ImageChannel
{
	friend class ImageChannelUnitTestSuite;
public:
	ImageChannelUnitTest() : nds::ImageChannel()
	{}

	virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers)
	{
		ImageChannel::registerHandlers(pvContainers);
		NDS_TRC("ImageChannelUnitTest::registerHandlers");

		return ndsSuccess;
	}
};

class ImageChannelUnitTestSuite : public CxxTest::TestSuite
{
  public:

	void testCreateImageChannel()
	{
		ImageChannelUnitTest channel;
	}

	void testHandlerRegistration()
	{
		nds::PVContainers containers;
		ndsStatus result;
		reason_type reason;
		int portAddress=1;

		ImageChannelUnitTest channel;
  	    channel._portAddr = portAddress;
  	    result = channel.registerHandlers( &containers );
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		CHECK_REASON_REGISTRATION( "Command" );
		CHECK_REASON_REGISTRATION( "State" );
		CHECK_REASON_REGISTRATION( "Resolution" );
		CHECK_REASON_REGISTRATION( "Width" );
		CHECK_REASON_REGISTRATION( "Height" );
		CHECK_REASON_REGISTRATION( "SamplesPerPixel" );
		CHECK_REASON_REGISTRATION( "OriginX" );
		CHECK_REASON_REGISTRATION( "OriginY" );
	}

	void idea()
	{
		nds::PVContainers containers;
		ndsStatus result;
//		reason_type reason;
		int portAddress=1;

		ImageChannelUnitTest channel;
  	    channel._portAddr = portAddress;
  	    result = channel.registerHandlers( &containers );


//		#define NDS_INSTANTIATE NDS_TEST_CHECK_REASON_REGISTRATION
//		#define NDS_INSTANTIATE_OCTET NDS_TEST_CHECK_REASON_REGISTRATION_OCTET
//		#define NDS_INSTANTIATE_ARRAY NDS_TEST_CHECK_REASON_REGISTRATION_ARRAY
//			NDS_INSTANTIATE_FOR_EACH_IMAGE_CHANNEL_FUNCTION
//		#undef NDS_INSTANTIATE
//		#undef NDS_INSTANTIATE_OCTET
//		#undef NDS_INSTANTIATE_ARRAY
	}

	void idea2()
	{
		nds::PVContainers containers;
		ndsStatus result;
		int portAddress=1;

		ImageChannelUnitTest obj;
  	    obj._portAddr = portAddress;
  	    result = obj.registerHandlers( &containers );


		#define NDS_INSTANTIATE NDS_TEST_CHECK_REASON_VALUE
		#define NDS_INSTANTIATE_OCTET NDS_TEST_CHECK_REASON_VALUE_OCTET
		#define NDS_INSTANTIATE_ARRAY NDS_TEST_CHECK_REASON_VALUE_ARRAY
			NDS_INSTANTIATE_FOR_EACH_IMAGE_CHANNEL_FUNCTION
		#undef NDS_INSTANTIATE
		#undef NDS_INSTANTIATE_OCTET
		#undef NDS_INSTANTIATE_ARRAY
	}

};


#endif /* TEST_IMAGECHANNEL_HPP_ */
