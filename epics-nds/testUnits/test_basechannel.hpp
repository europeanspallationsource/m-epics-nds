/*
 * test_adiochannel.hpp
 *
 *  Created on: 5. okt. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef TEST_ADIOCHANNEL_HPP_
#define TEST_ADIOCHANNEL_HPP_

#include "ndsBaseChannel.h"

class BaseChannelUnitTest: public nds::BaseChannel
{
	friend class BaseChannelUnitTestSuite;
public:
	BaseChannelUnitTest() : nds::BaseChannel()
	{}

	virtual void printStructure(const std::string&){}
};

class BaseChannelUnitTestSuite : public CxxTest::TestSuite
{
  public:
	/* TEST CASES */
	void testCreateADIOChannel()
	{
		BaseChannelUnitTest channel;
	}

	void testHandlerRegistration()
	{
		nds::PVContainers containers;
		ndsStatus result;
		reason_type reason;
		int portAddress=1;

		BaseChannelUnitTest channel;
  	    channel._portAddr = portAddress;

  	    result = channel.registerHandlers( &containers );
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "Command", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "State", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);


		result = containers.getReason(portAddress, "Trigger", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "TriggerChannel", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "TriggerRepeat", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "TriggerDelay", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "TriggerRouting", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "PauseTrigger", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "PauseTriggerChannel", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "PauseTriggerRepeat", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "PauseTriggerDelay", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "PauseTriggerRouting", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "ReferenceTrigger", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "ReferenceTriggerChannel", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "ReferenceTriggerRepeat", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "ReferenceTriggerDelay", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "ReferenceTriggerRouting", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "ConvertClockBase", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "ConvertClockTarget", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "ConvertClockPolarity", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "ConvertClockDelay", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "ConvertClockRouting", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "ClockSource", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "ClockFrequency", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "ClockMultiplier", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "ClockTarget", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "ClockRouting", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "ClockPolarity", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(portAddress, "ProcessingMode", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		result = containers.getReason(portAddress, "SamplesCount", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
	}



};



#endif /* TEST_ADIOCHANNEL_HPP_ */
