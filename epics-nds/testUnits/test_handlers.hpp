/*
 * test_handlers.hpp
 *
 *  Created on: 30. avg. 2012
 *      Author: Slava Isaev
 *
 *      (C) Cosylab. Slovenia 2012
 */

#ifndef TEST_HANDLERS_HPP_
#define TEST_HANDLERS_HPP_

#include <cstdio>
#include <cstring>
#include <iostream>

#include "ndsPV.h"
#include "ndsPVContainer.h"
#include "ndsDevice.h"
#include "ndsDebug.h"

using namespace std;

#define NDS_LOG_OUT_TO(LEVEL, args...) TS_TRACE(args)

#define OCTET_TEST_VAL "ValueTestOctet"

class TestDevice : public nds::Device
{
public:
	TestDevice(const std::string& name):nds::Device(name)
	{}

	int getInterruptCommand ()
	{
		return _interruptIdCommand;
	}

	int getInterruptFirmware ()
	{
		return _interruptIdFirmware;
	}
	int getInterruptHardware ()
	{
		return _interruptIdHardware;
	}
	int getInterruptModel ()
	{
		return _interruptIdModel;
	}
	int getInterruptSerial ()
	{
		return _interruptIdSerial;
	}
	int getInterruptSoftware ()
	{
		return _interruptIdSoftware;
	}

	int getInterruptOctet ()
	{
		return _interruptIdOctet;
	}
	int getInterruptInt32 ()
	{
		return _interruptIdInt32;
	}
	int getInterruptFloat64 ()
	{
		return _interruptIdFloat64;
	}
	int getInterruptInt8Array ()
	{
		return _interruptIdInt8Array;
	}
	int getInterruptInt16Array ()
	{
		return _interruptIdInt16Array;
	}
	int getInterruptInt32Array ()
	{
		return _interruptIdInt32Array;
	}
	int getInterruptFloat32Array ()
	{
		return _interruptIdFloat32Array;
	}
	int getInterruptFloat64Array ()
	{
		return _interruptIdFloat64Array;
	}

	virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers)
	{
		NDS_TRC("TestDevice::registerHandlers");

		NDS_PV_REGISTER_OCTET("Octet", &TestDevice::setOctet, &TestDevice::getOctet, &_interruptIdOctet);
		NDS_PV_REGISTER_INT32("Int32", &TestDevice::setInt32, &TestDevice::getInt32, &_interruptIdInt32);
		NDS_PV_REGISTER_FLOAT64("Float64", &TestDevice::setFloat64, &TestDevice::getFloat64, &_interruptIdFloat64);
		NDS_PV_REGISTER_INT8ARRAY("Int8Array", &TestDevice::setInt8Array, &TestDevice::getInt8Array, &_interruptIdInt8Array);
		NDS_PV_REGISTER_INT16ARRAY("Int16Array", &TestDevice::setInt16Array, &TestDevice::getInt16Array, &_interruptIdInt16Array);
		NDS_PV_REGISTER_INT32ARRAY("Int32Array", &TestDevice::setInt32Array, &TestDevice::getInt32Array, &_interruptIdInt32Array);
		NDS_PV_REGISTER_FLOAT32ARRAY("Float32Array", &TestDevice::setFloat32Array, &TestDevice::getFloat32Array, &_interruptIdFloat32Array);
		NDS_PV_REGISTER_FLOAT64ARRAY("Float64Array", &TestDevice::setFloat64Array, &TestDevice::getFloat64Array, &_interruptIdFloat64Array);

		Device::registerHandlers(pvContainers);

		return ndsSuccess;
	}

    virtual ndsStatus getFirmware(asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason)
    {
    	return ndsSuccess;
    }

    virtual ndsStatus getHardware(asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason)
    {
    	return ndsSuccess;
    }

    virtual ndsStatus getModel   (asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason)
    {
    	return ndsSuccess;
    }

    virtual ndsStatus getSerial  (asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason)
    {
    	return ndsError;
    }

    virtual ndsStatus getSoftware(asynUser *pasynUser, char *buf, size_t size, size_t *pcount, int *eomReason)
    {
    	snprintf(buf, size, OCTET_TEST_VAL);
    	*pcount = strlen(buf);
    	*eomReason = ASYN_EOM_END;
    	return ndsSuccess;
    }

};


class HandlersTestSuite : public CxxTest::TestSuite
{
  public:

	/* TEST CASES */
	void testCreatePVContainers()
	{
		nds::PVContainers containers;
    }

	void testCreateDevice()
	{
		nds::PVContainers containers;
		TestDevice device("testDevice");
		ndsStatus   result;
		result = device.propagateRegisterHandlers(&containers);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		reason_type reason;
		result = containers.getReason(device.getPortAddr(), "Octet", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptOctet() );

		result = containers.getReason(device.getPortAddr(), "Int32", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptInt32() );

		result = containers.getReason(device.getPortAddr(), "Float64", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptFloat64() );

		result = containers.getReason(device.getPortAddr(), "Int8Array", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptInt8Array() );
		result = containers.getReason(device.getPortAddr(), "Int16Array", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptInt16Array() );
		result = containers.getReason(device.getPortAddr(), "Int32Array", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptInt32Array() );

		result = containers.getReason(device.getPortAddr(), "Float32Array", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptFloat32Array() );
		result = containers.getReason(device.getPortAddr(), "Float64Array", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptFloat64Array() );

		result = containers.getReason(device.getPortAddr(), "Command", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptCommand() );

		result = containers.getReason(device.getPortAddr(), "FirmwareVersion", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptFirmware() );

		result = containers.getReason(device.getPortAddr(), "HardwareRevision", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptHardware() );

		result = containers.getReason(device.getPortAddr(), "Model", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptModel() );

		result = containers.getReason(device.getPortAddr(), "Serial", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptSerial() );

		result = containers.getReason(device.getPortAddr(), "SoftwareVersion", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
		TSM_ASSERT_EQUALS("", reason, device.getInterruptSoftware() );

    }

	void testCallOctetFunctions()
	{
		TS_TRACE("CallOctetFunctions");
		nds::PVContainers containers;
		TestDevice device("testDevice");
		ndsStatus   result;
		int 		reason;
		result = device.propagateRegisterHandlers(&containers);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(device.getPortAddr(), "SoftwareVersion", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		nds::PVOctet* pv = containers.getHandlers(Loki::Type2Type<asynOctet>(), reason);
		if (pv)
		{
			size_t nActual;
			result = pv->write( (asynUser*)0, "Test val", 20, &nActual);
			TSM_ASSERT_EQUALS("", result, ndsSuccess);

		}else
			TS_FAIL("PV is not found");
	}

	void testCallInt32Functions()
	{
		TS_TRACE("Int32Functions");
		nds::PVContainers containers;
		TestDevice device("testDevice");
		ndsStatus   result;
		int 		reason;
		result = device.propagateRegisterHandlers(&containers);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);

		result = containers.getReason(device.getPortAddr(), "SoftwareVersion", &reason);
		TSM_ASSERT_EQUALS("", result, ndsSuccess);
	}

};

#endif /* TEST_HANDLERS_HPP_ */
